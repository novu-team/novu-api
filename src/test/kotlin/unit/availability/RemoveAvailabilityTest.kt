package unit.availability

import fr.novu.api.availability.domain.entity.*
import fr.novu.api.availability.domain.repository.ParticipantRepository
import fr.novu.api.availability.usecase.RemoveAvailability
import fr.novu.api.availability.usecase.exceptions.AvailabilityNotFoundException
import fr.novu.api.availability.usecase.exceptions.UnableToRemoveException
import fr.novu.api.availability.usecase.exceptions.UnknownParticipantException
import fr.novu.api.availability.usecase.input.RemoveAvailabilityInput
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.NullAndEmptySource
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.Mockito.*
import org.mockito.kotlin.argumentCaptor
import java.time.LocalDate

@DisplayName("Remove availability")
internal class RemoveAvailabilityTest {

    private lateinit var removeAvailability: RemoveAvailability
    private lateinit var mockParticipantRepository: ParticipantRepository

    private val validGroupId = "33"
    private val validUserId = "53"

    @BeforeEach
    fun setup() {
        mockParticipantRepository = mock(ParticipantRepository::class.java)
        removeAvailability = RemoveAvailability(mockParticipantRepository)
        val userId = UserId(validUserId)

        val groupId = GroupId(validGroupId)
        val participantId = ParticipantId(userId, groupId)
        val participant =
            Participant(
                participantId, false,
                mutableListOf(
                    Availability(
                        participantId, LocalDate.now(),
                        LocalDate.now().plusMonths(6)
                    )
                )

            )

        `when`(mockParticipantRepository.findParticipant(userId, groupId)).thenReturn(participant)
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "55"])
    @DisplayName("Participant should exists with a valid user")
    fun should_throw_participant_not_found_exception_when_invalid_user(userId: String?) {
        Assertions.assertThrows(UnknownParticipantException::class.java) {
            removeAvailability.execute(
                RemoveAvailabilityInput(
                    userId,
                    userId,
                    validGroupId,
                    LocalDate.now(),
                    LocalDate.now().plusMonths(6)
                )
            )
        }
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "55"])
    @DisplayName("Participant should exists with a valid group")
    fun should_throw_participant_not_found_exception_when_invalid_group(groupId: String?) {
        Assertions.assertThrows(UnknownParticipantException::class.java) {
            removeAvailability.execute(
                RemoveAvailabilityInput(
                    validUserId,
                    validUserId,
                    groupId,
                    LocalDate.now(),
                    LocalDate.now().plusMonths(6)
                )
            )
        }
    }

    @Test
    @DisplayName("Suppressor should possess the availability to remove")
    fun should_throw_unable_to_remove_exception() {
        Assertions.assertThrows(UnableToRemoveException::class.java) {
            removeAvailability.execute(
                RemoveAvailabilityInput(
                    "22",
                    "53",
                    validGroupId,
                    LocalDate.now(),
                    LocalDate.now().plusMonths(6)
                )
            )
        }
    }

    @Test
    @DisplayName("Availability should exists")
    fun should_throw_availability_not_found() {
        Assertions.assertThrows(AvailabilityNotFoundException::class.java) {
            removeAvailability.execute(
                RemoveAvailabilityInput(
                    validUserId,
                    validUserId,
                    validGroupId,
                    LocalDate.now().plusMonths(3),
                    LocalDate.now().plusMonths(6)
                )
            )
        }
    }

    @Test
    @DisplayName("Should update participant availabilities")
    fun should_throw_not_enough_availability_exception() {
        val input = RemoveAvailabilityInput(
            validUserId,
            validUserId,
            validGroupId,
            LocalDate.now(),
            LocalDate.now().plusMonths(6)
        )

        val participantId = ParticipantId(UserId(validUserId), GroupId(validGroupId))
        val participant = Participant(participantId, false, mutableListOf())

        removeAvailability.execute(input)

        val participantArgument = argumentCaptor<Participant>()

        verify(mockParticipantRepository, times(1)).updateParticipant(participantArgument.capture())

        val capturedParticipant = participantArgument.firstValue
        assertThat(capturedParticipant)
            .usingRecursiveComparison()
            .isEqualTo(participant)
    }


}