package unit.availability

import fr.novu.api.availability.domain.entity.*
import fr.novu.api.availability.domain.repository.ParticipantRepository
import fr.novu.api.availability.usecase.IndicateAvailability
import fr.novu.api.availability.usecase.exceptions.*
import fr.novu.api.availability.usecase.input.IndicateAvailabilityInput
import fr.novu.api.availability.usecase.output.AvailabilityOutput
import fr.novu.api.availability.usecase.output.IndicateAvailabilityOutput
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.NullAndEmptySource
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.kotlin.argumentCaptor
import java.time.LocalDate

@DisplayName("Quit a group")
internal class IndicateAvailabilityTest {

    private lateinit var indicateAvailability: IndicateAvailability
    private lateinit var mockParticipantRepository: ParticipantRepository

    @BeforeEach
    fun setup() {
        mockParticipantRepository = mock(ParticipantRepository::class.java)
        indicateAvailability = IndicateAvailability(mockParticipantRepository)

        val userId = UserId("53")
        val groupId = GroupId("33")

        val participantId = ParticipantId(userId, groupId)
        val participant = Participant(participantId, false)

        val participantId2 = ParticipantId(UserId("44"), groupId)
        val participant2 = Participant(participantId2, true)

        val userId2 = UserId("1")
        val groupId2 = GroupId("55")
        val participantId3 = ParticipantId(userId2, groupId2)
        val participant3 = Participant(participantId3, false)


        `when`(mockParticipantRepository.findParticipant(userId, groupId)).thenReturn(participant)
        `when`(mockParticipantRepository.findParticipant(userId2, groupId2)).thenReturn(participant3)
        `when`(mockParticipantRepository.findParticipantsOfGroup(groupId2)).thenReturn(listOf(participant3))
        `when`(mockParticipantRepository.findParticipantsOfGroup(groupId)).thenReturn(listOf(participant, participant2))
    }


    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "55"])
    @DisplayName("Participant should exists with a valid user")
    fun should_throw_participant_not_found_exception_when_invalid_user(userId: String?) {
        Assertions.assertThrows(UnknownParticipantException::class.java) {
            indicateAvailability.execute(
                IndicateAvailabilityInput(
                    userId,
                    "33",
                    LocalDate.now(),
                    LocalDate.now().plusMonths(6)
                )
            )
        }
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "55"])
    @DisplayName("Participant should exists with a valid group")
    fun should_throw_participant_not_found_exception_when_invalid_group(groupId: String?) {
        Assertions.assertThrows(UnknownParticipantException::class.java) {
            indicateAvailability.execute(
                IndicateAvailabilityInput(
                    "53",
                    groupId,
                    LocalDate.now(),
                    LocalDate.now().plusMonths(6)
                )
            )
        }
    }

    @Test
    @DisplayName("Group should have an admin")
    fun should_throw_start_no_admin_exception() {
        Assertions.assertThrows(NoAdminException::class.java) {
            indicateAvailability.execute(
                IndicateAvailabilityInput(
                    "1",
                    "55",
                    LocalDate.now().plusDays(1),
                    LocalDate.now().plusMonths(6)
                )
            )
        }
    }

    @Test
    @DisplayName("Start date should not be in the past")
    fun should_throw_start_date_in_the_past_exception() {
        Assertions.assertThrows(InvalidStartDateException::class.java) {
            indicateAvailability.execute(
                IndicateAvailabilityInput(
                    "53",
                    "33",
                    LocalDate.now().minusYears(1),
                    LocalDate.now().plusMonths(6)
                )
            )
        }
    }

    @Test
    @DisplayName("Start date should be provided")
    fun should_throw_start_date_in_the_past_exception_when_start_date_null() {
        Assertions.assertThrows(InvalidStartDateException::class.java) {
            indicateAvailability.execute(IndicateAvailabilityInput("53", "33", null, LocalDate.now().plusMonths(6)))
        }
    }

    @Test
    @DisplayName("End date should not be anterior to the start date")
    fun should_throw_invalid_end_date_exception_if_before_start_date() {
        Assertions.assertThrows(InvalidEndDateException::class.java) {
            indicateAvailability.execute(
                IndicateAvailabilityInput(
                    "53",
                    "33",
                    LocalDate.now().plusDays(2),
                    LocalDate.now().plusDays(1)
                )
            )
        }
    }

    @Test
    @DisplayName("End date should not be null")
    fun should_throw_invalid_end_date_exception_when_null() {
        Assertions.assertThrows(InvalidEndDateException::class.java) {
            indicateAvailability.execute(IndicateAvailabilityInput("53", "33", LocalDate.now().plusDays(2), null))
        }
    }

    @Test
    @DisplayName("Availability should not already exists")
    fun should_throw_availability_already_exists_exception() {
        val userId = UserId("444")
        val groupId = GroupId("33")
        val participantId = ParticipantId(userId, groupId)
        val participant = Participant(
            participantId,
            true,
            mutableListOf(Availability(participantId, LocalDate.now(), LocalDate.now().plusDays(2)))
        )
        `when`(mockParticipantRepository.findParticipant(userId, groupId))
            .thenReturn(participant)
        Assertions.assertThrows(AvailabilityAlreadyExistsException::class.java) {
            indicateAvailability.execute(
                IndicateAvailabilityInput(
                    userId.value,
                    groupId.value,
                    LocalDate.now(),
                    LocalDate.now().plusDays(2)
                )
            )
        }
    }

    @Test
    @DisplayName("Should save participant availabilities")
    fun should_save_participant_availabilty() {
        val participantId = ParticipantId(UserId("53"), GroupId("33"))
        val expectedParticipant = Participant(
            participantId, false,
            mutableListOf(
                Availability(
                    participantId, LocalDate.now().plusDays(3),
                    LocalDate.now().plusDays(5)
                )
            )

        )

        indicateAvailability.execute(
            IndicateAvailabilityInput(
                "53", "33", LocalDate.now().plusDays(3),
                LocalDate.now().plusDays(5)
            )
        )

        val groupArgument = argumentCaptor<Participant>()
        Mockito.verify(mockParticipantRepository, Mockito.times(1)).updateParticipant(groupArgument.capture())
        val capturedGroup = groupArgument.firstValue

        assertThat(capturedGroup).usingRecursiveComparison().isEqualTo(expectedParticipant)
    }

    @Test
    @DisplayName("Should return new participant infos")
    fun should_return_new_participant_infos() {
        val expectedOutput = IndicateAvailabilityOutput(
            "53", "33",
            false, mutableListOf(
                AvailabilityOutput(
                    LocalDate.now().plusDays(3),
                    LocalDate.now().plusDays(5)
                )
            )
        )

        val result = indicateAvailability.execute(
            IndicateAvailabilityInput(
                "53", "33", LocalDate.now().plusDays(3),
                LocalDate.now().plusDays(5)
            )
        )


        assertThat(expectedOutput)
            .usingRecursiveComparison()
            .isEqualTo(result)
    }


}