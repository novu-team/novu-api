package unit.availability

import fr.novu.api.availability.domain.entity.*
import fr.novu.api.availability.domain.repository.GroupRepository
import fr.novu.api.availability.domain.repository.ParticipantRepository
import fr.novu.api.availability.usecase.LockGroupDates
import fr.novu.api.availability.usecase.exceptions.*
import fr.novu.api.availability.usecase.input.LockGroupDatesInput
import fr.novu.api.availability.usecase.output.LockGroupDatesOutput
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.NullAndEmptySource
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.kotlin.argumentCaptor
import java.time.LocalDate

@DisplayName("Lock group dates")
internal class LockGroupDatesTest {

    private lateinit var lockGroupDates: LockGroupDates
    private lateinit var mockParticipantRepository: ParticipantRepository
    private lateinit var mockGroupRepository: GroupRepository

    @BeforeEach
    fun setup() {
        mockParticipantRepository = mock(ParticipantRepository::class.java)
        mockGroupRepository = mock(GroupRepository::class.java)
        lockGroupDates = LockGroupDates(mockParticipantRepository, mockGroupRepository)

        val userId = UserId("53")
        val groupId = GroupId("33")

        val participantId = ParticipantId(userId, groupId)
        val participant = Participant(participantId, false)

        val userId2 = UserId("44")
        val participantId2 = ParticipantId(userId2, groupId)
        val participant2 = Participant(participantId2, true)

        val userId4 = UserId("4334")
        val pId4 = ParticipantId(userId4, groupId)
        val participant4 = Participant(
            pId4, false, mutableListOf(
                Availability(
                    pId4, LocalDate.now().plusDays(2),
                    LocalDate.now().plusDays(5)
                )
            )
        )

        `when`(mockParticipantRepository.findParticipant(userId, groupId)).thenReturn(participant)
        `when`(mockParticipantRepository.findParticipant(userId4, groupId)).thenReturn(participant4)
        `when`(mockParticipantRepository.findParticipant(userId2, groupId)).thenReturn(participant2)

        `when`(mockParticipantRepository.findParticipantsOfGroup(groupId)).thenReturn(
            listOf(
                participant,
                participant2,
                participant4
            )
        )

        val secondUserId = UserId("432")
        val secondGroupId = GroupId("222222")

        val secondPID = ParticipantId(secondUserId, secondGroupId)
        val secondP = Participant(
            secondPID, false, mutableListOf(
                Availability(
                    secondPID, LocalDate.now().plusDays(2),
                    LocalDate.now().plusDays(5)
                )
            )
        )

        val secondUserId2 = UserId("44")
        val secondParticipantId2 = ParticipantId(secondUserId2, secondGroupId)
        val secondParticipant2 = Participant(secondParticipantId2, true)

        val secondUserId4 = UserId("4334")
        val secondPID4 = ParticipantId(secondUserId4, secondGroupId)
        val secondParticipant4 = Participant(
            secondPID4, false, mutableListOf(
                Availability(
                    secondPID4, LocalDate.now().plusDays(2),
                    LocalDate.now().plusDays(5)
                )
            )
        )

        val secondGroup = Group(secondGroupId, "name", mutableListOf(secondP, secondParticipant2, secondParticipant4))

        `when`(mockParticipantRepository.findParticipant(secondUserId, secondGroupId)).thenReturn(secondP)
        `when`(mockParticipantRepository.findParticipant(secondUserId4, secondGroupId)).thenReturn(secondParticipant4)
        `when`(mockParticipantRepository.findParticipant(secondUserId2, secondGroupId)).thenReturn(secondParticipant2)
        `when`(mockGroupRepository.findGroup(secondGroupId)).thenReturn(secondGroup)


    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "55"])
    @DisplayName("Participant should exists with a valid user")
    fun should_throw_participant_not_found_exception_when_invalid_user(userId: String?) {
        Assertions.assertThrows(UnknownParticipantException::class.java) {
            lockGroupDates.execute(
                LockGroupDatesInput(
                    userId,
                    "33",
                    LocalDate.now(),
                    LocalDate.now().plusMonths(6)
                )
            )
        }
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "55"])
    @DisplayName("Participant should exists with a valid group")
    fun should_throw_participant_not_found_exception_when_invalid_group(groupId: String?) {
        Assertions.assertThrows(UnknownParticipantException::class.java) {
            lockGroupDates.execute(
                LockGroupDatesInput(
                    "53",
                    groupId,
                    LocalDate.now(),
                    LocalDate.now().plusMonths(6)
                )
            )
        }
    }

    @Test
    @DisplayName("Participant should be admin")
    fun should_throw_not_an_admin_exception() {
        Assertions.assertThrows(NotAnAdminException::class.java) {
            lockGroupDates.execute(
                LockGroupDatesInput(
                    "53",
                    "33",
                    LocalDate.now(),
                    LocalDate.now().plusMonths(6)
                )
            )
        }
    }

    @Test
    @DisplayName("Half of the group should have indicate his availabilities")
    fun should_throw_not_enough_availability_exception() {
        Assertions.assertThrows(NotEnoughAvailabilityException::class.java) {
            lockGroupDates.execute(
                LockGroupDatesInput(
                    "44",
                    "33",
                    LocalDate.now().plusDays(1),
                    LocalDate.now().plusMonths(6)
                )
            )
        }
    }

    @Test
    @DisplayName("Start date should not be in the past")
    fun should_throw_start_date_in_the_past_exception() {
        Assertions.assertThrows(InvalidStartDateException::class.java) {
            lockGroupDates.execute(
                LockGroupDatesInput(
                    "44",
                    "33",
                    LocalDate.now().minusYears(1),
                    LocalDate.now().plusMonths(6)
                )
            )
        }
    }

    @Test
    @DisplayName("Start date should be provided")
    fun should_throw_start_date_in_the_past_exception_when_start_date_null() {
        Assertions.assertThrows(InvalidStartDateException::class.java) {
            lockGroupDates.execute(LockGroupDatesInput("44", "33", null, LocalDate.now().plusMonths(6)))
        }
    }

    @Test
    @DisplayName("End date should not be anterior to the start date")
    fun should_throw_invalid_end_date_exception_if_before_start_date() {
        Assertions.assertThrows(InvalidEndDateException::class.java) {
            lockGroupDates.execute(
                LockGroupDatesInput(
                    "44",
                    "33",
                    LocalDate.now().plusDays(2),
                    LocalDate.now().plusDays(1)
                )
            )
        }
    }

    @Test
    @DisplayName("End date should not be null")
    fun should_throw_invalid_end_date_exception_when_null() {
        Assertions.assertThrows(InvalidEndDateException::class.java) {
            lockGroupDates.execute(LockGroupDatesInput("44", "33", LocalDate.now().plusDays(2), null))
        }
    }


    @Test
    @DisplayName("Should save the group dates")
    fun should_save_the_new_group_dates() {

        val userId = UserId("432")
        val groupId = GroupId("222222")

        val participantId = ParticipantId(userId, groupId)
        val participant = Participant(
            participantId, false, mutableListOf(
                Availability(
                    participantId, LocalDate.now().plusDays(2),
                    LocalDate.now().plusDays(5)
                )
            )
        )

        val userId2 = UserId("44")
        val participantId2 = ParticipantId(userId2, groupId)
        val participant2 = Participant(participantId2, true)

        val userId4 = UserId("4334")
        val pId4 = ParticipantId(userId4, groupId)
        val participant4 = Participant(
            pId4, false, mutableListOf(
                Availability(
                    pId4, LocalDate.now().plusDays(2),
                    LocalDate.now().plusDays(5)
                )
            )
        )

        val group = Group(groupId, "name", mutableListOf(participant, participant2, participant4))

        `when`(mockParticipantRepository.findParticipant(userId, groupId)).thenReturn(participant)
        `when`(mockParticipantRepository.findParticipant(userId4, groupId)).thenReturn(participant4)
        `when`(mockParticipantRepository.findParticipant(userId2, groupId)).thenReturn(participant2)
        `when`(mockGroupRepository.findGroup(groupId)).thenReturn(group)

        val input = LockGroupDatesInput(
            "44",
            "222222",
            LocalDate.now().plusDays(1),
            LocalDate.now().plusMonths(6)
        )

        lockGroupDates.execute(input)

        val expectedGroup = Group(
            GroupId("222222"), "name",
            mutableListOf(participant, participant2, participant4),
            LocalDate.now().plusDays(1),
            LocalDate.now().plusMonths(6)
        )

        val groupArgument = argumentCaptor<Group>()
        Mockito.verify(mockGroupRepository, Mockito.times(1)).updateGroup(groupArgument.capture())
        val capturedGroup = groupArgument.firstValue

        assertThat(capturedGroup)
            .usingRecursiveComparison()
            .isEqualTo(expectedGroup)
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "ddd"])
    @DisplayName("Should reject wrong group id")
    fun should_throw_wrong_group_exception(groupId: String?) {
        `when`(mockGroupRepository.findGroup(GroupId(groupId ?: ""))).thenReturn(null)
        `when`(mockParticipantRepository.findParticipant(UserId("44"), GroupId(groupId ?: ""))).thenReturn(
            Participant(ParticipantId(UserId("44"), GroupId(groupId ?: "")), true)
        )
        Assertions.assertThrows(UnknownGroupException::class.java) {
            lockGroupDates.execute(
                LockGroupDatesInput(
                    "44", groupId,
                    LocalDate.now().plusDays(1),
                    LocalDate.now().plusMonths(6)
                )
            )
        }
    }


    @Test
    @DisplayName("Should return new group infos")
    fun should_return_new_participant_infos() {
        val expectedOutput = LockGroupDatesOutput(
            "222222", LocalDate.now().plusDays(3),
            LocalDate.now().plusDays(5)
        )

        val result = lockGroupDates.execute(
            LockGroupDatesInput(
                "44", "222222", LocalDate.now().plusDays(3),
                LocalDate.now().plusDays(5)
            )
        )


        assertThat(expectedOutput)
            .usingRecursiveComparison()
            .isEqualTo(result)
    }


}