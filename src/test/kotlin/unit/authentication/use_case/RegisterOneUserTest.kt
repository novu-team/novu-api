package unit.authentication.use_case

import fr.novu.api.authentication.domain.entity.UserId
import fr.novu.api.authentication.domain.exception.*
import fr.novu.api.authentication.domain.repository.UserRepository
import fr.novu.api.authentication.usecase.RegisterOneUser
import fr.novu.api.authentication.usecase.ports.PasswordEncrypter
import fr.novu.api.authentication.usecase.ports.RegisterOneUserDataGateway
import fr.novu.api.authentication.usecase.request.RegisterOneUserRequest
import fr.novu.api.authentication.usecase.response.RegisterOneUserResponse
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import unit.authentication.infra.repository.FakeGateway
import unit.authentication.infra.repository.FakeUserRepository
import unit.authentication.infra.utils.FakePasswordEncrypter
import java.time.LocalDate

@DisplayName("Register One User")
internal class RegisterOneUserTest {

    private lateinit var fakeUserRepository: UserRepository
    private lateinit var fakeGateway: RegisterOneUserDataGateway
    private lateinit var fakePasswordEncrypter: PasswordEncrypter

    private lateinit var sut: RegisterOneUser

    @BeforeEach
    fun setup() {
        fakeUserRepository = FakeUserRepository()
        fakePasswordEncrypter = FakePasswordEncrypter()
        fakeGateway = FakeGateway(fakeUserRepository)
        sut = RegisterOneUser(fakeGateway, fakePasswordEncrypter)
    }

    @Nested
    @DisplayName("Name controls")
    inner class Name {
//        @Test
//        @DisplayName("Name Null Should Throw Missing Property Exception")
//        fun name_null_should_throw_missing_property_exception() {
//            assertThrows(MissingPropertyException::class.java) {
//                sut.execute(
//                    Register(
//                        email = "bonjour@bonjour.com",
//                        password = "123456fdgfhE**",
//                        firstname = "okkkkk",
//                        name = null,
//                        trigram = "ABC",
//                        trigramColor = "red",
//                        birthday = LocalDate.now().minusYears(20),
//                        phoneNumber = "+33670954332",
//                        sex = true,
//                        tagIds = emptyList()
//                    )
//                )
//            }
//        }

        @Test
        @DisplayName("Name Empty Should Throw Missing Property Exception")
        fun name_empty_should_throw_missing_property_exception() {
            assertThrows(MissingPropertyException::class.java) {
                sut.execute(
                    RegisterOneUserRequest(
                        email = "bonjour@bonjour.com",
                        password = "123456fdgfhE**",
                        firstname = "okkkkk",
                        name = "",
                        trigram = "ABC",
                        trigramColor = "red",
                        birthday = LocalDate.now().minusYears(20),
                        phoneNumber = "+33670954332",
                        sex = true,
                        tagIds = emptyList()
                    )
                )
            }
        }
    }

    @Nested
    @DisplayName("Firstname controls")
    inner class Firstname {
//        @Test
//        @DisplayName("Firstname Null Should Throw Missing Property Exception")
//        fun firstname_null_should_throw_missing_property_exception() {
//            assertThrows(MissingPropertyException::class.java) {
//                sut.execute(
//                    Register(
//                        email = "bonjour@bonjour.com",
//                        password = "123456fdgfhE**",
//                        firstname = null,
//                        name = "okjojuninio",
//                        trigram = "ABC",
//                        trigramColor = "red",
//                        birthday = LocalDate.now().minusYears(20),
//                        phoneNumber = "+33670954332",
//                        sex = true,
//                        tagIds = emptyList()
//                    )
//                )
//            }
//        }

        @Test
        @DisplayName("Firstname Empty Should Throw Missing Property Exception")
        fun firstname_empty_should_throw_missing_property_exception() {
            assertThrows(MissingPropertyException::class.java) {
                sut.execute(
                    RegisterOneUserRequest(
                        email = "bonjour@bonjour.com",
                        password = "123456fdgfhE**",
                        firstname = "",
                        name = "joi,,i,klk",
                        trigram = "ABC",
                        trigramColor = "red",
                        birthday = LocalDate.now().minusYears(20),
                        phoneNumber = "+33670954332",
                        sex = true,
                        tagIds = emptyList()
                    )
                )
            }
        }
    }

    @Nested
    @DisplayName("Email controls")
    inner class Email {
//        @Test
//        @DisplayName("Email Null Should Throw Missing Property Exception")
//        fun email_null_should_throw_missing_property_exception() {
//            assertThrows(WrongEmailException::class.java) {
//                sut.execute(
//                    Register(
//                        email = null,
//                        password = "123456fdgfhE**",
//                        firstname = ",ionmoinklk",
//                        name = "okjojuninio",
//                        trigram = "ABC",
//                        trigramColor = "red",
//                        birthday = LocalDate.now().minusYears(20),
//                        phoneNumber = "+33670954332",
//                        sex = true,
//                        tagIds = emptyList()
//                    )
//                )
//            }
//        }

        @Test
        @DisplayName("Email Empty Should Throw Missing Property Exception")
        fun email_empty_should_throw_missing_property_exception() {
            assertThrows(WrongEmailException::class.java) {
                sut.execute(
                    RegisterOneUserRequest(
                        email = " ",
                        password = "123456fdgfhE**",
                        firstname = ",ionmoinklk",
                        name = "okjojuninio",
                        trigram = "ABC",
                        trigramColor = "red",
                        birthday = LocalDate.now().minusYears(20),
                        phoneNumber = "+33670954332",
                        sex = true,
                        tagIds = emptyList()
                    )
                )
            }
        }

        @Test
        @DisplayName("Email Wrong Format Should Throw Wrong Email Exception")
        fun email_wrong_format_should_throw_wrong_email_exception() {
            assertThrows(WrongEmailException::class.java) {
                sut.execute(
                    RegisterOneUserRequest(
                        email = "terterter",
                        password = "123456fdgfhE**",
                        firstname = ",ionmoinklk",
                        name = "okjojuninio",
                        trigram = "ABC",
                        trigramColor = "red",
                        birthday = LocalDate.now().minusYears(20),
                        phoneNumber = "+33670954332",
                        sex = true,
                        tagIds = emptyList()
                    )
                )
            }
        }

    }

    @Nested
    @DisplayName("Password controls")
    inner class Password {

        @Test
        @DisplayName("Password Empty Should Throw Wrong Password Exception")
        fun password_empty_should_throw_wrong_password_exception() {
            assertThrows(WrongPasswordException::class.java) {
                sut.execute(
                    RegisterOneUserRequest(
                        email = "tern@gmail.com",
                        password = "          P",
                        firstname = ",ionmoinklk",
                        name = "okjojuninio",
                        trigram = "ABC",
                        trigramColor = "red",
                        birthday = LocalDate.now().minusYears(20),
                        phoneNumber = "+33670954332",
                        sex = true,
                        tagIds = emptyList()
                    )
                )
            }
        }

        @Test
        @DisplayName("Password should contain at least 8 characters")
        fun should_throw_wrong_password_if_below_8_chars() {
            assertThrows(WrongPasswordException::class.java) {
                sut.execute(
                    RegisterOneUserRequest(
                        email = "ter@gmail.com",
                        password = "2123Lm",
                        firstname = ",ionmoinklk",
                        name = "okjojuninio",
                        trigram = "ABC",
                        trigramColor = "red",
                        birthday = LocalDate.now().minusYears(20),
                        phoneNumber = "+33670954332",
                        sex = true,
                        tagIds = emptyList()
                    )
                )
            }
        }

        @Test
        @DisplayName("Password should contain at least one maj")
        fun should_throw_wrong_password_if_contains_only_mins() {
            assertThrows(WrongPasswordException::class.java) {
                sut.execute(
                    RegisterOneUserRequest(
                        email = "ter@gmail.com",
                        password = "2123mmmmmmmm",
                        firstname = ",ionmoinklk",
                        name = "okjojuninio",
                        trigram = "ABC",
                        trigramColor = "red",
                        birthday = LocalDate.now().minusYears(20),
                        phoneNumber = "+33670954332",
                        sex = true,
                        tagIds = emptyList()
                    )
                )
            }
        }


    }

    @Nested
    @DisplayName("Trigram controls")
    inner class Trigram {

        @Test
        @DisplayName("Trigram should not be empty")
        fun should_throw_invalid_trigram_when_empty() {
            assertThrows(InvalidTrigramException::class.java) {
                sut.execute(
                    RegisterOneUserRequest(
                        email = "tedsdsr@gmail.com",
                        password = "2123mmMmmmmm",
                        firstname = ",ionmoinklk",
                        name = "okjojuninio",
                        trigram = "AB ",
                        trigramColor = "red",
                        birthday = LocalDate.now().minusYears(20),
                        phoneNumber = "+33670954332",
                        sex = true,
                        tagIds = emptyList()
                    )
                )
            }
        }

        @Test
        @DisplayName("Trigram should contain 3 letters")
        fun should_throw_invalid_trigram() {
            assertThrows(InvalidTrigramException::class.java) {
                sut.execute(
                    RegisterOneUserRequest(
                        email = "terytvg@gmail.com",
                        password = "2123mmMmmmmm",
                        firstname = ",ionmoinklk",
                        name = "okjojuninio",
                        trigram = "ABCd",
                        trigramColor = "red",
                        birthday = LocalDate.now().minusYears(20),
                        phoneNumber = "+33670954332",
                        sex = true,
                        tagIds = emptyList()
                    )
                )
            }
        }
    }


    @Nested
    @DisplayName("Trigram Color controls")
    inner class TrigramColor {

//        @Test
//        @DisplayName("Trigram color should not be null")
//        fun should_throw_missing_property_when_trigram_color_null() {
//            assertThrows(MissingPropertyException::class.java) {
//                sut.execute(
//                    Register(
//                        email = "ter@gmail.com",
//                        password = "2123mmMmmmmm",
//                        firstname = ",ionmoinklk",
//                        name = "okjojuninio",
//                        trigram = "ABC",
//                        trigramColor = null,
//                        birthday = LocalDate.now().minusYears(20),
//                        phoneNumber = "+33670954332",
//                        sex = true,
//                        tagIds = emptyList()
//                    )
//                )
//            }
//        }

        @Test
        @DisplayName("Trigram color should not be empty")
        fun should_throw_missing_property_when_trigram_color_empty() {
            assertThrows(MissingPropertyException::class.java) {
                sut.execute(
                    RegisterOneUserRequest(
                        email = "terno@gmail.com",
                        password = "2123mmMmmmmm",
                        firstname = ",ionmoinklk",
                        name = "okjojuninio",
                        trigram = "ABC",
                        trigramColor = "  ",
                        birthday = LocalDate.now().minusYears(20),
                        phoneNumber = "+33670954332",
                        sex = true,
                        tagIds = emptyList()
                    )
                )
            }
        }
    }


    @Nested
    @DisplayName("Birthday controls")
    inner class Birthday {

//        @Test
//        @DisplayName("Birhtday should not be null")
//        fun should_throw_invalid_birthday_when_null() {
//            assertThrows(InvalidBirthdayException::class.java) {
//                sut.execute(
//                    Register(
//                        email = "ter@gmail.com",
//                        password = "2123mmMmmmmm",
//                        firstname = ",ionmoinklk",
//                        name = "okjojuninio",
//                        trigram = "ABC",
//                        trigramColor = "red",
//                        birthday = null,
//                        phoneNumber = "+33670954332",
//                        sex = true,
//                        tagIds = emptyList()
//                    )
//                )
//            }
//        }

        @Test
        @DisplayName("Birthday should be in situated in the past")
        fun should_throw_missing_property_when_trigram_color_null() {
            assertThrows(InvalidBirthdayException::class.java) {
                sut.execute(
                    RegisterOneUserRequest(
                        email = "terno@gmail.com",
                        password = "2123mmMmmmmm",
                        firstname = ",ionmoinklk",
                        name = "okjojuninio",
                        trigram = "ABC",
                        trigramColor = "red",
                        birthday = LocalDate.now().plusMonths(1),
                        phoneNumber = "+33670954332",
                        sex = true,
                        tagIds = emptyList()
                    )
                )
            }
        }
    }

    @Nested
    @DisplayName("Phone controls")
    inner class Phone {

        @Test
        @DisplayName("Phone should not be empty")
        fun should_throw_invalid_phone_when_empty() {
            assertThrows(InvalidPhoneException::class.java) {
                sut.execute(
                    RegisterOneUserRequest(
                        email = "ternor@gmail.com",
                        password = "2123mmMmmmmm",
                        firstname = ",ionmoinklk",
                        name = "okjojuninio",
                        trigram = "ABC",
                        trigramColor = "red",
                        birthday = LocalDate.now().minusYears(20),
                        phoneNumber = "            ",
                        sex = true,
                        tagIds = emptyList()
                    )
                )
            }
        }

        @Test
        @DisplayName("Phone should contains 12 characters")
        fun should_throw_invalid_phone_when_not_12_chars() {
            assertThrows(InvalidPhoneException::class.java) {
                sut.execute(
                    RegisterOneUserRequest(
                        email = "terno@gmail.com",
                        password = "2123mmMmmmmm",
                        firstname = ",ionmoinklk",
                        name = "okjojuninio",
                        trigram = "ABC",
                        trigramColor = "red",
                        birthday = LocalDate.now().minusYears(20),
                        phoneNumber = "+333333333",
                        sex = true,
                        tagIds = emptyList()
                    )
                )
            }
        }

    }

    @Nested
    @DisplayName("Sex controls")
    inner class Sex {
//        @Test
//        @DisplayName("Sex should not be null")
//        fun should_throw_missing_property_sex_when_null() {
//            assertThrows(MissingPropertyException::class.java) {
//                sut.execute(
//                    Register(
//                        email = "ter@gmail.com",
//                        password = "2123mmMmmmmm",
//                        firstname = ",ionmoinklk",
//                        name = "okjojuninio",
//                        trigram = "ABC",
//                        trigramColor = "red",
//                        birthday = LocalDate.now().minusYears(20),
//                        phoneNumber = "+33644444444",
//                        sex = null,
//                        tagIds = emptyList()
//                    )
//                )
//            }
//        }

    }

    @Nested
    @DisplayName("Already exists controls")
    inner class AlreadyExist {
        @Test
        @DisplayName("Email should not already has been taken")
        fun should_throw_email_already_used_exception() {
            assertThrows(EmailAlreadyUsedException::class.java) {
                sut.execute(
                    RegisterOneUserRequest(
                        email = "ter@gmail.com",
                        password = "2123mmMmmmmm",
                        firstname = ",ionmoinklk",
                        name = "okjojuninio",
                        trigram = "ABC",
                        trigramColor = "red",
                        birthday = LocalDate.now().minusYears(20),
                        phoneNumber = "+33644444444",
                        sex = false,
                        tagIds = emptyList()
                    )
                )
            }
        }

        @Test
        @DisplayName("Phone should not already has been taken")
        fun should_throw_phone_already_used_exception() {
            assertThrows(PhoneAlreadyUsedException::class.java) {
                sut.execute(
                    RegisterOneUserRequest(
                        email = "terter@gmail.com",
                        password = "2123mmMmmmmm",
                        firstname = ",ionmoinklk",
                        name = "okjojuninio",
                        trigram = "ABC",
                        trigramColor = "red",
                        birthday = LocalDate.now().minusYears(20),
                        phoneNumber = "+33644444444",
                        sex = false,
                        tagIds = emptyList()
                    )
                )
            }
        }

    }

    @Nested
    @DisplayName("Register succeed")
    inner class RegisterSucceed {
        @Test
        @DisplayName("Register should save user with normal role")
        fun should_save_user() {
            val request = RegisterOneUserRequest(
                email = "terr,eio@gmail.com",
                password = "2123mmMmmmmm",
                firstname = ",ionmoinklk",
                name = "okjojuninio",
                trigram = "ABC",
                trigramColor = "red",
                birthday = LocalDate.now().minusYears(20),
                phoneNumber = "+33644444466",
                sex = false,
                tagIds = emptyList()
            )

            val expected = RegisterOneUserResponse(
                email = "terr,eio@gmail.com",
                userId = UserId("3L"),
                isAdmin = false
            )

            assertEquals(expected, sut.execute(request))
        }

    }


}