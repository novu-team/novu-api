package unit.authentication.infra.repository

import fr.novu.api.authentication.domain.entity.User
import fr.novu.api.authentication.domain.entity.UserId
import fr.novu.api.authentication.domain.repository.UserRepository
import fr.novu.api.authentication.usecase.ports.RegisterOneUserDataGateway

class FakeGateway(private val repository: UserRepository) : RegisterOneUserDataGateway {
    override fun nextIdentity(): UserId {
        return repository.nextIdentity()
    }

    override fun save(user: User) {
        repository.save(user)
    }

    override fun findUserByEmail(email: String): User? {
        return repository.findUserByEmail(email)
    }

    override fun findUserByPhone(phoneNumber: String): User? {
        return repository.findUserByPhone(phoneNumber)
    }
}