package unit.authentication.infra.repository

import fr.novu.api.authentication.domain.entity.User
import fr.novu.api.authentication.domain.entity.UserId
import fr.novu.api.authentication.domain.repository.UserRepository
import java.time.LocalDate

internal class FakeUserRepository : UserRepository {
    override fun findUserByEmail(email: String): User? {
        if (email.contentEquals("ter@gmail.com")) {
            return User(
                email = "ter@gmail.com",
                firstname = ",ionmoinklk",
                name = "okjojuninio",
                trigram = "ABC",
                birthday = LocalDate.now().minusYears(20),
                phoneNumber = "+33644444444",
                sex = false,
                id = UserId("1"),
                tags = emptyList()
            )
        }
        return null
    }

    override fun findUserByPhone(phoneNumber: String): User? {
        if (phoneNumber.contentEquals("+33644444444")) {
            return User(
                email = "terr,eio@gmail.com",
                firstname = ",ionmoinklk",
                name = "okjojuninio",
                trigram = "ABC",
                birthday = LocalDate.now().minusYears(20),
                phoneNumber = "+33644444444",
                sex = false,
                id = UserId("2"),
                tags = emptyList()
            )
        }
        return null
    }


    override fun nextIdentity(): UserId {
        return UserId("3L")
    }

    override fun count(): Number {
        return 2
    }

    override fun save(user: User): User? {
        val expected = User(
            id = UserId("3L"),
            email = "terr,eio@gmail.com",
            password = "dskdislbsn+***43",
            firstname = ",ionmoinklk",
            name = "okjojuninio",
            trigram = "ABC",
            trigramColor = "red",
            birthday = LocalDate.now().minusYears(20),
            phoneNumber = "+33644444466",
            sex = false
        )
        if (user == expected) {
            return User(
                email = "terr,eio@gmail.com",
                firstname = ",ionmoinklk",
                name = "okjojuninio",
                trigram = "ABC",
                birthday = LocalDate.now().minusYears(20),
                phoneNumber = "+33644444466",
                sex = false,
                id = UserId("3L")
            )
        }
        return null
    }


}