package unit.authentication.infra.utils

import fr.novu.api.authentication.usecase.ports.PasswordEncrypter

class FakePasswordEncrypter : PasswordEncrypter {
    override fun encryptPassword(password: String): String {
        return "dskdislbsn+***43"
    }
}