package unit.rental.infra.repository

import fr.novu.api.rental.domain.entity.group.Group
import fr.novu.api.rental.domain.entity.group.GroupId
import fr.novu.api.rental.domain.entity.group.Participant
import fr.novu.api.rental.domain.entity.group.ParticipantId
import fr.novu.api.rental.domain.entity.housing.HousingId
import fr.novu.api.rental.domain.repository.GroupRepository

internal class FakeGroupRepository : GroupRepository {
    private fun findUsersInGroup(groupId: GroupId): MutableList<Participant> {
        val participant1 = Participant(ParticipantId("1"))
        val participant2 = Participant(ParticipantId("2"))
        val participant3 = Participant(ParticipantId("3"))
        val participant4 = Participant(ParticipantId("4"))
        val participant5 = Participant(ParticipantId("5"))
        val participant6 = Participant(ParticipantId("6"))
        val participant7 = Participant(ParticipantId("7"))
        if (groupId.value == "10") {
            return mutableListOf(
                participant1,
                participant2,
                participant3,
                participant4,
                participant5,
                participant6,
                participant7
            )
        }
        if (groupId.value == "11") {
            return mutableListOf(participant1, participant2, participant3, participant4)
        }
        if (groupId.value == "36") {
            return mutableListOf()
        }
        return mutableListOf(participant5)
    }

    override fun findGroupById(groupId: GroupId): Group? {
        if (groupId.value == "25") {
            return null
        }
        if (groupId.value == "46") {
            return Group(
                GroupId(groupId.value),
                findUsersInGroup(groupId),
                HousingId("5")
            )
        }
        if (groupId.value == "8") {
            return Group(
                GroupId(groupId.value),
                findUsersInGroup(groupId),
                HousingId("5")
            )
        }
        return Group(GroupId(groupId.value), findUsersInGroup(groupId))
    }


    override fun update(group: Group) {
        TODO("Not yet implemented")
    }
}