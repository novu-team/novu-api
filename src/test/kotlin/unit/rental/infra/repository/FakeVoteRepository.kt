package unit.rental.infra.repository

import fr.novu.api.rental.domain.entity.group.GroupId
import fr.novu.api.rental.domain.entity.group.ParticipantId
import fr.novu.api.rental.domain.entity.housing.HousingId
import fr.novu.api.rental.domain.entity.vote.Vote
import fr.novu.api.rental.domain.entity.vote.VoteId
import fr.novu.api.rental.domain.repository.VoteRepository

internal class FakeVoteRepository : VoteRepository {
    override fun findHousingVotesInGroup(groupId: GroupId): List<Vote> {
        val vote1 = Vote(VoteId("1", HousingId("1")))
        val vote2 = Vote(VoteId("4", HousingId("1")))
        val vote3 = Vote(VoteId("3", HousingId("5")))
        val vote4 = Vote(VoteId("4", HousingId("1")))
        val vote5 = Vote(VoteId("2", HousingId("1")))
        return when (groupId.value) {
            "10" -> {
                listOf(vote1, vote2, vote3)
            }
            "2" -> {
                listOf(vote4)
            }
            "11" -> {
                listOf(vote1, vote2, vote3, vote5)
            }
            else -> emptyList()
        }
    }

    override fun findAllVoteByHousingId(housingId: HousingId): List<Vote> {
        val vote1 = Vote(VoteId("1", HousingId("3")))
        val vote2 = Vote(VoteId("5", HousingId("3")))
        val vote3 = Vote(VoteId("3", HousingId("3")))
        val vote5 = Vote(VoteId("2", HousingId("3")))
        return listOf(vote1, vote2, vote3, vote5)
    }

    override fun save(vote: Vote) {
        return
    }

    override fun findVote(participantId: ParticipantId, housingId: HousingId): Vote? {
        TODO("Not yet implemented")
    }

    override fun delete(vote: Vote) {
        TODO("Not yet implemented")
    }
}