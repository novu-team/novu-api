package unit.rental.infra.repository

import fr.novu.api.rental.domain.entity.group.GroupId
import fr.novu.api.rental.domain.entity.group.ParticipantId
import fr.novu.api.rental.domain.entity.housing.Housing
import fr.novu.api.rental.domain.entity.housing.HousingId
import fr.novu.api.rental.domain.repository.HousingRepository

internal class FakeHousingRepository : HousingRepository {
    override fun findByHousingIdAndGroupId(housingId: HousingId, groupId: GroupId): Housing? {
        if (housingId.value == "1" && groupId.value == "10") {
            return Housing(housingId, GroupId("10"), ParticipantId("33"), "name", available = true)
        }
        if (groupId.value == "11" && housingId.value == "1") {
            return Housing(housingId, GroupId("11"), ParticipantId("33"), "name", available = true)
        }

        if (groupId.value == "36" && housingId.value == "1") {
            return Housing(housingId, GroupId("11"), ParticipantId("33"), "name", available = true)
        }
        if (groupId.value == "46" && housingId.value == "1") {
            return Housing(housingId, GroupId("11"), ParticipantId("33"), "name", available = true)
        }

        return null
    }

    override fun findHousing(housingId: HousingId): Housing? {

        if (housingId.value == "44") {
            return null
        }
        if (housingId.value == "123") {
            return Housing(housingId, GroupId("25"), ParticipantId("33"), "name", available = true)
        }
        if (housingId.value == "46") {
            return Housing(housingId, GroupId("11"), ParticipantId("33"), "name", available = true)
        }
        if (housingId.value == "2") {
            return Housing(housingId, GroupId("11"), ParticipantId("33"), "name", available = false)
        }
        if (housingId.value == "5") {
            return Housing(housingId, GroupId("8"), ParticipantId("5"), "name", available = false)
        }

        return Housing(housingId, GroupId("11"), ParticipantId("33"), "name", available = true)
    }

    override fun addHousing(housing: Housing) {
        TODO("Not yet implemented")
    }

    override fun nextIdentity(): HousingId {
        TODO("Not yet implemented")
    }

    override fun findByCreatorAndName(participantId: ParticipantId, name: String): Housing? {
        TODO("Not yet implemented")
    }

    override fun updateHousing(housing: Housing) {
        TODO("Not yet implemented")
    }

    override fun deleteHousing(housing: Housing) {
        TODO("Not yet implemented")
    }
}