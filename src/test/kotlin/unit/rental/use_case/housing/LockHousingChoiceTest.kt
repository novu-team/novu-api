package unit.rental.use_case.housing

import fr.novu.api.rental.domain.entity.housing.exceptions.ParticipantNotInGroupException
import fr.novu.api.rental.domain.repository.GroupRepository
import fr.novu.api.rental.domain.repository.HousingRepository
import fr.novu.api.rental.domain.repository.VoteRepository
import fr.novu.api.rental.usecase.housing.LockHousingChoice
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import unit.rental.infra.repository.FakeGroupRepository
import unit.rental.infra.repository.FakeHousingRepository
import unit.rental.infra.repository.FakeVoteRepository

internal class LockHousingChoiceTest {

    private lateinit var fakeHousingRepository: HousingRepository
    private lateinit var fakeGroupRepository: GroupRepository
    private lateinit var fakeVoteRepository: VoteRepository

    private lateinit var sut: LockHousingChoice

    @BeforeEach
    fun setup() {
        fakeHousingRepository = FakeHousingRepository()
        fakeGroupRepository = FakeGroupRepository()
        fakeVoteRepository = FakeVoteRepository()
        sut = LockHousingChoice(
            fakeGroupRepository, fakeHousingRepository, fakeVoteRepository
        )
    }
//
//    @Test
//    fun should_return_housing_not_exist_when_housing_is_not_in_group_proposal() {
//        Assertions.assertThrows(HousingNotInGroupException::class.java) {
//            sut.lock("1", "5")
//        }
//    }
//
//    @Test
//    fun should_throw_not_enough_votes_when_less_of_half_users_have_voted() {
//        Assertions.assertThrows(NotEnoughVoteException::class.java) {
//            sut.lock("1", "1")
//        }
//    }

    @Test
    fun should_throw_user_not_in_group_when_user_is_not_in_group() {
        Assertions.assertThrows(ParticipantNotInGroupException::class.java) {
            sut.lock("1", "40")
        }
    }

//    @Test
//    fun should_lock_housing_when_all_parameters_are_good() {
//        org.assertj.core.api.Assertions.assertThatCode {
//            sut.lock("1", "1")
//        }.doesNotThrowAnyException()
//    }
//
//    @Test
//    fun should_throw_group_not_exist_when_group_does_not_exist() {
//        Assertions.assertThrows(GroupNotFoundException::class.java) {
//            sut.lock("1", "40")
//        }
//    }
//
//    @Test
//    fun should_assign_choose_housing_to_group_when_housing_locked() {
//        val participant1 = Participant(ParticipantId("1"))
//        val participant2 = Participant(ParticipantId("2"))
//        val participant3 = Participant(ParticipantId("3"))
//        val participant4 = Participant(ParticipantId("4"))
//        val participants = mutableListOf(
//            participant1, participant2, participant3, participant4
//        )
//        val expectedResult = Group(GroupId("11"), participants, HousingId("1"))
//        val group = sut.lock("1", "1")
//        Assertions.assertEquals(expectedResult, group)
//    }
//
//    @Test
//    fun should_throw_empty_group_when_group_have_no_participants() {
//        Assertions.assertThrows(EmptyGroupException::class.java) {
//            sut.lock("1", "1")
//        }
//    }
//
//    @Test
//    fun should_throw_housing_already_assign_when_group_already_has_housing() {
//        Assertions.assertThrows(HousingAlreadyAssignException::class.java) {
//            sut.lock("1", "5")
//        }
//    }

}