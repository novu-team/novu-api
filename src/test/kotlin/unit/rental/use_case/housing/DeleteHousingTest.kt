package unit.rental.use_case.housing


import fr.novu.api.rental.domain.entity.group.GroupId
import fr.novu.api.rental.domain.entity.group.ParticipantId
import fr.novu.api.rental.domain.entity.housing.Housing
import fr.novu.api.rental.domain.entity.housing.HousingId
import fr.novu.api.rental.domain.entity.housing.exceptions.NotAllowedToDeleteHousingException
import fr.novu.api.rental.domain.repository.HousingRepository
import fr.novu.api.rental.usecase.housing.DeleteHousing
import fr.novu.api.rental.usecase.housing.input.DeleteHousingInput
import fr.novu.api.rental.usecase.vote.exceptions.HousingNotFoundException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.NullAndEmptySource
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.kotlin.argumentCaptor
import org.mockito.kotlin.times
import org.mockito.kotlin.verify

@DisplayName("Delete Housing")
internal class DeleteHousingTest {

    //on doit checker que c bien le createur
    //et pis c tout jai envie de te dire
    //ça va etre assez rapide celle la je piense
    private lateinit var deleteHousing: DeleteHousing

    private lateinit var mockHousingRepository: HousingRepository

    @BeforeEach
    fun setup() {
        mockHousingRepository = mock(HousingRepository::class.java)
        deleteHousing = DeleteHousing(mockHousingRepository)
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "GB5TYHE56YT"])
    @DisplayName("Housing should exists")
    fun should_throw_invalid_housing_name(housingId: String?) {
        `when`(mockHousingRepository.findHousing(HousingId("GB5TYHE56YT"))).thenReturn(null)
        Assertions.assertThrows(HousingNotFoundException::class.java) {
            val input = DeleteHousingInput(housingId, "34")
            deleteHousing.execute(input)
        }
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "flipflop"])
    @DisplayName("Check if the suppressor is the creator")
    fun should_throw_not_allowed_exception(suppressorId: String?) {
        val groupId = GroupId("33")
        val participantId = ParticipantId("bonujour")

        val housingId = HousingId("dfcvnjko")
        val housing = Housing(
            housingId,
            groupId,
            participantId,
            "name",
            "url",
            "description",
            available = true
        )

        `when`(mockHousingRepository.findHousing(HousingId("dfcvnjko"))).thenReturn(housing)

        Assertions.assertThrows(NotAllowedToDeleteHousingException::class.java) {
            val input = DeleteHousingInput("dfcvnjko", suppressorId)
            deleteHousing.execute(input)
        }
    }


    @Test
    @DisplayName("Should delete housing")
    fun should_delete_housing() {
        val groupId = GroupId("33")
        val participantId = ParticipantId("bonujour")

        val housingId = HousingId("dfcvnjko")
        val housing = Housing(
            housingId,
            groupId,
            participantId,
            "name",
            "url",
            "description",
            available = true
        )

        val expectedHousing = Housing(
            housingId,
            groupId,
            participantId,
            "name",
            "url",
            "description",
            available = true
        )

        `when`(mockHousingRepository.findHousing(HousingId("dfcvnjko"))).thenReturn(housing)

        val input = DeleteHousingInput("dfcvnjko", "bonujour")
        deleteHousing.execute(input)

        val housingArgument = argumentCaptor<Housing>()

        verify(mockHousingRepository, times(1)).deleteHousing(housingArgument.capture())


        val capturedHousing = housingArgument.firstValue
        assertThat(capturedHousing)
            .usingRecursiveComparison()
            .isEqualTo(expectedHousing)

    }


}