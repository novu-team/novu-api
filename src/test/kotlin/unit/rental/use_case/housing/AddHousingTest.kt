package unit.rental.use_case.housing


import fr.novu.api.rental.domain.entity.group.Group
import fr.novu.api.rental.domain.entity.group.GroupId
import fr.novu.api.rental.domain.entity.group.Participant
import fr.novu.api.rental.domain.entity.group.ParticipantId
import fr.novu.api.rental.domain.entity.housing.Housing
import fr.novu.api.rental.domain.entity.housing.HousingId
import fr.novu.api.rental.domain.entity.housing.exceptions.HousingAlreadyExistsException
import fr.novu.api.rental.domain.entity.housing.exceptions.InvalidHousingNameException
import fr.novu.api.rental.domain.entity.housing.exceptions.ParticipantNotInGroupException
import fr.novu.api.rental.domain.repository.GroupRepository
import fr.novu.api.rental.domain.repository.HousingRepository
import fr.novu.api.rental.usecase.exceptions.GroupNotFoundException
import fr.novu.api.rental.usecase.housing.AddHousing
import fr.novu.api.rental.usecase.housing.input.AddHousingInput
import fr.novu.api.rental.usecase.housing.output.AddHousingOutput
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.NullAndEmptySource
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.kotlin.argumentCaptor
import org.mockito.kotlin.times
import org.mockito.kotlin.verify

@DisplayName("Add Housing")
internal class AddHousingTest {

    private lateinit var addHousing: AddHousing
    private lateinit var mockGroupRepository: GroupRepository
    private lateinit var mockHousingRepository: HousingRepository

    @BeforeEach
    fun setup() {
        mockGroupRepository = mock(GroupRepository::class.java)
        mockHousingRepository = mock(HousingRepository::class.java)
        addHousing = AddHousing(mockGroupRepository, mockHousingRepository)
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n"])
    @DisplayName("Housing should have a name")
    fun should_throw_invalid_housing_name(name: String?) {
        Assertions.assertThrows(InvalidHousingNameException::class.java) {
            val input = AddHousingInput("ERZ", "33", name, "url", "description")
            addHousing.execute(input)
        }
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "3434"])
    @DisplayName("Group should exists")
    fun should_throw_group_not_found_exception(groupId: String?) {
        `when`(mockGroupRepository.findGroupById(GroupId("3434"))).thenReturn(null)
        Assertions.assertThrows(GroupNotFoundException::class.java) {
            val input = AddHousingInput("ERZ", groupId, "name", "url", "description")
            addHousing.execute(input)
        }
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "ZERFCZ3E4A"])
    @DisplayName("Participant should exists")
    fun should_throw_participant_not_in_group_exception(participantId: String?) {
        val groupId = GroupId("33")
        val participant = Participant(ParticipantId("bonujour"))
        val group = Group(groupId, mutableListOf(participant))
        `when`(mockGroupRepository.findGroupById(groupId)).thenReturn(group)

        Assertions.assertThrows(ParticipantNotInGroupException::class.java) {
            val input = AddHousingInput(participantId, "33", "name", "url", "description")
            addHousing.execute(input)
        }
    }

    @Test
    @DisplayName("Housing should not have same creator and name")
    fun should_throw_already_exists_exception() {
        val groupId = GroupId("33")
        val participantId = ParticipantId("bonujour")
        val participant = Participant(participantId)
        val group = Group(groupId, mutableListOf(participant))

        val housingId = HousingId("ERTGRTG")
        val housing = Housing(
            housingId,
            groupId,
            participantId,
            "name",
            "url",
            "description",
            available = true
        )

        `when`(mockGroupRepository.findGroupById(groupId)).thenReturn(group)
        `when`(mockHousingRepository.findByCreatorAndName(participantId, "name")).thenReturn(housing)

        Assertions.assertThrows(HousingAlreadyExistsException::class.java) {
            val input = AddHousingInput(participantId.value, "33", "name", "url", "description")
            addHousing.execute(input)
        }
    }

    @Test
    @DisplayName("Should save housing")
    fun should_save_new_housing() {
        val groupId = GroupId("33")
        val participantId = ParticipantId("bonujour")
        val participant = Participant(participantId)

        val housingId = HousingId("ERTGRTG")
        val expectedHousing = Housing(
            housingId,
            groupId,
            participantId,
            "name",
            "url",
            "description",
            available = true
        )
        val group = Group(groupId, mutableListOf(participant))

        val input = AddHousingInput(participantId.value, groupId.value, "name", "url", "description")

        `when`(mockGroupRepository.findGroupById(groupId)).thenReturn(group)
        `when`(mockHousingRepository.nextIdentity()).thenReturn(housingId)
        addHousing.execute(input)

        val housingArgument = argumentCaptor<Housing>()

        verify(mockHousingRepository, times(1)).addHousing(housingArgument.capture())


        val capturedHousing = housingArgument.firstValue
        assertThat(capturedHousing)
            .usingRecursiveComparison()
            .isEqualTo(expectedHousing)

    }

    @Test
    @DisplayName("Should return add housing output")
    fun should_return_output() {
        val groupId = GroupId("33")
        val participantId = ParticipantId("bonujour")
        val participant = Participant(participantId)
        val housingId = HousingId("ERTGRTG")

        val group = Group(groupId, mutableListOf(participant))

        val input = AddHousingInput(participantId.value, groupId.value, "name", "url", "description")

        `when`(mockGroupRepository.findGroupById(groupId)).thenReturn(group)
        `when`(mockHousingRepository.nextIdentity()).thenReturn(housingId)


        val expectedOutput = AddHousingOutput(
            housingId.value, "33", "bonujour", "name", "url", "description",
            true
        )

        assertThat(addHousing.execute(input))
            .usingRecursiveComparison()
            .isEqualTo(expectedOutput)

    }


}