package unit.rental.use_case.housing


import fr.novu.api.rental.domain.entity.group.GroupId
import fr.novu.api.rental.domain.entity.group.ParticipantId
import fr.novu.api.rental.domain.entity.housing.Housing
import fr.novu.api.rental.domain.entity.housing.HousingId
import fr.novu.api.rental.domain.entity.housing.exceptions.NotAllowedToChangeInfoException
import fr.novu.api.rental.domain.repository.HousingRepository
import fr.novu.api.rental.usecase.housing.ChangeHousingInfo
import fr.novu.api.rental.usecase.housing.input.ChangeHousingInfoInput
import fr.novu.api.rental.usecase.housing.output.ChangeHousingOutput
import fr.novu.api.rental.usecase.vote.exceptions.HousingNotFoundException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.NullAndEmptySource
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.kotlin.argumentCaptor
import org.mockito.kotlin.times
import org.mockito.kotlin.verify

@DisplayName("Change Housing Info")
internal class ChangeHousingInformationTest {

    private lateinit var changeHousingInfo: ChangeHousingInfo

    //    private lateinit var mockGroupRepository: GroupRepository
    private lateinit var mockHousingRepository: HousingRepository

    @BeforeEach
    fun setup() {
//        mockGroupRepository = mock(GroupRepository::class.java)
        mockHousingRepository = mock(HousingRepository::class.java)
        changeHousingInfo = ChangeHousingInfo(mockHousingRepository)
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "GB5TYHE56YT"])
    @DisplayName("Housing should exists")
    fun should_throw_invalid_housing_name(housingId: String?) {
        `when`(mockHousingRepository.findHousing(HousingId("GB5TYHE56YT"))).thenReturn(null)
        Assertions.assertThrows(HousingNotFoundException::class.java) {
            val input = ChangeHousingInfoInput(housingId, "33", "newurl", "newDes")
            changeHousingInfo.execute(input)
        }
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "flipflop"])
    @DisplayName("Check if the changer is the creator")
    fun should_throw_not_allowed_exception(changerId: String?) {
        val groupId = GroupId("33")
        val participantId = ParticipantId("bonujour")

        val housingId = HousingId("dfcvnjko")
        val housing = Housing(
            housingId,
            groupId,
            participantId,
            "name",
            "url",
            "description",
            available = true
        )

        `when`(mockHousingRepository.findHousing(HousingId("dfcvnjko"))).thenReturn(housing)

        Assertions.assertThrows(NotAllowedToChangeInfoException::class.java) {
            val input = ChangeHousingInfoInput("dfcvnjko", changerId, "newurl", "newDes")
            changeHousingInfo.execute(input)
        }
    }


    @Test
    @DisplayName("Should update housing")
    fun should_update_housing() {
        val groupId = GroupId("33")
        val participantId = ParticipantId("bonujour")

        val housingId = HousingId("dfcvnjko")
        val housing = Housing(
            housingId,
            groupId,
            participantId,
            "name",
            "url",
            "description",
            available = true
        )

        val expectedHousing = Housing(
            housingId,
            groupId,
            participantId,
            "name",
            "newurl",
            "newDes",
            available = true
        )

        `when`(mockHousingRepository.findHousing(HousingId("dfcvnjko"))).thenReturn(housing)

        val input = ChangeHousingInfoInput("dfcvnjko", "bonujour", "newurl", "newDes")
        changeHousingInfo.execute(input)

        val housingArgument = argumentCaptor<Housing>()

        verify(mockHousingRepository, times(1)).updateHousing(housingArgument.capture())


        val capturedHousing = housingArgument.firstValue
        assertThat(capturedHousing)
            .usingRecursiveComparison()
            .isEqualTo(expectedHousing)

    }

    @Test
    @DisplayName("Should return add housing output")
    fun should_return_output() {
        val groupId = GroupId("33")
        val participantId = ParticipantId("bonujour")

        val housingId = HousingId("dfcvnjko")
        val housing = Housing(
            housingId,
            groupId,
            participantId,
            "name",
            "url",
            "description",
            available = true
        )

        `when`(mockHousingRepository.findHousing(HousingId("dfcvnjko"))).thenReturn(housing)

        val input = ChangeHousingInfoInput("dfcvnjko", "bonujour", "newurl", "newDes")


        val expectedOutput = ChangeHousingOutput(
            housingId.value, "33", "bonujour", "name", "newurl", "newDes",
            true
        )

        assertThat(changeHousingInfo.execute(input))
            .usingRecursiveComparison()
            .isEqualTo(expectedOutput)

    }


}