package unit.rental.use_case.vote

import fr.novu.api.rental.domain.entity.group.exceptions.HousingAlreadyAssignException
import fr.novu.api.rental.domain.entity.housing.exceptions.HousingUnavailableException
import fr.novu.api.rental.domain.entity.housing.exceptions.ParticipantNotInGroupException
import fr.novu.api.rental.domain.entity.vote.exceptions.ParticipantAlreadyVotedException
import fr.novu.api.rental.domain.repository.GroupRepository
import fr.novu.api.rental.domain.repository.HousingRepository
import fr.novu.api.rental.domain.repository.VoteRepository
import fr.novu.api.rental.usecase.exceptions.GroupNotFoundException
import fr.novu.api.rental.usecase.vote.VoteForOneHousing
import fr.novu.api.rental.usecase.vote.exceptions.HousingNotFoundException
import fr.novu.api.rental.usecase.vote.output.VoteOutput
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import unit.rental.infra.repository.FakeGroupRepository
import unit.rental.infra.repository.FakeHousingRepository
import unit.rental.infra.repository.FakeVoteRepository


internal class VoteForOneHousingTest {

    lateinit var sut: VoteForOneHousing
    private lateinit var fakeGroupRepository: GroupRepository

    private lateinit var fakeHousingRepository: HousingRepository

    private lateinit var fakeVoteRepository: VoteRepository

    @BeforeEach
    fun setUp() {
        fakeGroupRepository = FakeGroupRepository()
        fakeHousingRepository = FakeHousingRepository()
        fakeVoteRepository = FakeVoteRepository()
        sut = VoteForOneHousing(
            fakeGroupRepository,
            fakeHousingRepository,
            fakeVoteRepository
        )
    }

    //vérif groupe existe
    @Test
    fun should_throw_group_not_exist_when_group_does_not_exist() {
        Assertions.assertThrows(GroupNotFoundException::class.java) {
            sut.vote("33", "123")
        }
    }

    //vérifier si le participant appartient au groupe
    @Test
    fun should_throw_user_not_in_group_when_user_is_not_in_group() {
        Assertions.assertThrows(ParticipantNotInGroupException::class.java) {
            sut.vote("5", "1")
        }
    }

    //vérifier si lhousing existe
    @Test
    fun should_throw_housing_not_found() {
        Assertions.assertThrows(HousingNotFoundException::class.java) {
            sut.vote("1", "44")
        }
    }

    //vérifier si le groupe a déjà verouiller l'hebergement
    @Test
    fun should_throw_housing_already_assign_group_when_group_already_has_housing_locked() {
        Assertions.assertThrows(HousingAlreadyAssignException::class.java) {
            sut.vote("5", "5")
        }
    }

    //vérifier que la maison est toujours disponible
    @Test
    fun should_throw_housing_unavailable_when_housing_not_available() {
        Assertions.assertThrows(HousingUnavailableException::class.java) {
            sut.vote("1", "2")
        }
    }

    //vérifier si le participant a déjà voté pour la même house
    @Test
    fun should_throw_participant_already_voted_for_this_house() {
        Assertions.assertThrows(ParticipantAlreadyVotedException::class.java) {
            sut.vote("1", "3")
        }
    }

    //vérifier que le vote a été enregistré
    @Test
    fun should_return_vote_with_participant_and_house() {
        val expectedVote = VoteOutput("4", "3")
        val result = sut.vote("4", "3")
        Assertions.assertEquals(expectedVote, result)
    }
}