package unit.management.use_case

import fr.novu.api.management.domain.entity.*
import fr.novu.api.management.domain.repository.GroupRepository
import fr.novu.api.management.usecase.DeleteGroupAdmin
import fr.novu.api.management.usecase.exceptions.NotEnoughAdminException
import fr.novu.api.management.usecase.exceptions.ParticipantNotAdminException
import fr.novu.api.management.usecase.exceptions.ParticipantNotInGroupException
import fr.novu.api.management.usecase.exceptions.UnknownGroupException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.NullAndEmptySource
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.Mockito.*
import org.mockito.kotlin.argumentCaptor

@DisplayName("Remove admin of a group")
internal class DeleteGroupAdminTest {

    private lateinit var deleteGroupAdmin: DeleteGroupAdmin
    private lateinit var mockGroupRepository: GroupRepository

    @BeforeEach
    fun setup() {
        mockGroupRepository = mock(GroupRepository::class.java)
        deleteGroupAdmin = DeleteGroupAdmin(mockGroupRepository)

        val participantId = ParticipantId(UserId("53"), GroupId("33"))
        val participant = Participant(participantId, false)

        val participantId2 = ParticipantId(UserId("434334"), GroupId("33"))
        val participant2 = Participant(participantId2, true)


        val group = Group(GroupId("33"), "name", mutableListOf(participant, participant2))


        `when`(mockGroupRepository.findGroup(GroupId("33"))).thenReturn(group)
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "ddd"])
    @DisplayName("Should reject wrong group id")
    fun should_throw_wrong_group_exception(groupId: String?) {
        `when`(mockGroupRepository.findGroup(GroupId("ddd"))).thenReturn(null)
        Assertions.assertThrows(UnknownGroupException::class.java) {
            deleteGroupAdmin.execute("53", groupId, "44")
        }
    }


    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "55"])
    @DisplayName("Participant should be in the group")
    fun should_throw_participant_not_in_group_exception(userId: String?) {
        Assertions.assertThrows(ParticipantNotInGroupException::class.java) {
            deleteGroupAdmin.execute(userId, "33", "44")
        }
    }

    @Test
    @DisplayName("Should not be able to add admin if participant is not an admin")
    fun should_throw_not_admin_exception() {
        Assertions.assertThrows(ParticipantNotAdminException::class.java) {
            deleteGroupAdmin.execute("53", "33", "44")
        }
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "55"])
    @DisplayName("Admin should be in the group")
    fun should_throw_admin_not_in_group_exception(userId: String?) {
        Assertions.assertThrows(ParticipantNotInGroupException::class.java) {
            deleteGroupAdmin.execute("434334", "33", userId)
        }
    }

    @Test
    @DisplayName("Should not be able to remove admin if no admin left")
    fun should_throw_not_enough_admin_exception() {
        Assertions.assertThrows(NotEnoughAdminException::class.java) {
            deleteGroupAdmin.execute("434334", "33", "434334")
        }
    }


    @Test
    @DisplayName("Should remove admin of the group")
    fun should_remove_admin_group() {
        val participantId = ParticipantId(UserId("53"), GroupId("234"))
        val participant = Participant(participantId, false)

        val participantId2 = ParticipantId(UserId("434334"), GroupId("234"))
        val participant2 = Participant(participantId2, true)

        val participantId3 = ParticipantId(UserId("EFGDRTE"), GroupId("234"))
        val participant3 = Participant(participantId3, true)

        val group = Group(GroupId("234"), "name", mutableListOf(participant, participant2, participant3))

        val expectedGroup = Group(GroupId("234"), "name", mutableListOf(participant, participant2))

        `when`(mockGroupRepository.findGroup(GroupId("234"))).thenReturn(group)

        deleteGroupAdmin.execute("434334", "234", "EFGDRTE")

        val groupArgument = argumentCaptor<Group>()
        verify(mockGroupRepository, times(1)).updateGroup(groupArgument.capture())
        val capturedGroup = groupArgument.firstValue

        assertThat(capturedGroup)
            .usingRecursiveComparison()
            .isEqualTo(expectedGroup)
    }


}