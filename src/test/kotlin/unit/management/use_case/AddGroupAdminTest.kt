package unit.management.use_case

import fr.novu.api.management.domain.entity.*
import fr.novu.api.management.domain.repository.GroupRepository
import fr.novu.api.management.usecase.AddGroupAdmin
import fr.novu.api.management.usecase.exceptions.ParticipantNotAdminException
import fr.novu.api.management.usecase.exceptions.ParticipantNotInGroupException
import fr.novu.api.management.usecase.exceptions.UnknownGroupException
import fr.novu.api.management.usecase.output.AddGroupAdminOutput
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.NullAndEmptySource
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.kotlin.argumentCaptor

@DisplayName("Quit a group")
internal class AddGroupAdminTest {

    private lateinit var addGroupAdmin: AddGroupAdmin
    private lateinit var mockGroupRepository: GroupRepository

    @BeforeEach
    fun setup() {
        mockGroupRepository = mock(GroupRepository::class.java)
        addGroupAdmin = AddGroupAdmin(mockGroupRepository)

        val participantId = ParticipantId(UserId("53"), GroupId("33"))
        val participant = Participant(participantId, false)

        val participantId2 = ParticipantId(UserId("434334"), GroupId("33"))
        val participant2 = Participant(participantId2, true)
        val group = Group(GroupId("33"), "name", mutableListOf(participant, participant2))



        `when`(mockGroupRepository.findGroup(GroupId("33"))).thenReturn(group)
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "ddd"])
    @DisplayName("Should reject wrong group id")
    fun should_throw_wrong_group_exception(groupId: String?) {
        `when`(mockGroupRepository.findGroup(GroupId("ddd"))).thenReturn(null)
        Assertions.assertThrows(UnknownGroupException::class.java) {
            addGroupAdmin.execute("53", groupId, "44")
        }
    }


    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "55"])
    @DisplayName("Participant should be in the group")
    fun should_throw_participant_not_in_group_exception(userId: String?) {
        Assertions.assertThrows(ParticipantNotInGroupException::class.java) {
            addGroupAdmin.execute(userId, "33", "44")
        }
    }

    @Test
    @DisplayName("Should not be able to add admin if participant is not an admin")
    fun should_throw_not_admin_exception() {
        Assertions.assertThrows(ParticipantNotAdminException::class.java) {
            addGroupAdmin.execute("53", "33", "44")
        }
    }

    @Test
    @DisplayName("Should set participant to admin if already in group")
    fun should_set_participant_to_admin() {

        val participantId = ParticipantId(UserId("53"), GroupId("33"))
        val participant = Participant(participantId, true)

        val participantId2 = ParticipantId(UserId("434334"), GroupId("33"))
        val participant2 = Participant(participantId2, true)

        val expectedGroup = Group(GroupId("33"), "name", mutableListOf(participant, participant2))

        addGroupAdmin.execute("434334", "33", "53")

        val groupArgument = argumentCaptor<Group>()

        Mockito.verify(mockGroupRepository, Mockito.times(1)).updateGroup(groupArgument.capture())

        val capturedGroup = groupArgument.firstValue
        assertThat(capturedGroup)
            .usingRecursiveComparison()
            .isEqualTo(expectedGroup)
    }

    @Test
    @DisplayName("Should  not add admin participant to admin if not already in group")
    fun should_add_group_admin() {
        Assertions.assertThrows(ParticipantNotInGroupException::class.java) {
            addGroupAdmin.execute("434334", "33", "1234")
        }

    }

    @Test
    @DisplayName("Should return admin output")
    fun should_return_new_group() {

        val expectedOutput = AddGroupAdminOutput("53", "33", true)


        val output = addGroupAdmin.execute("434334", "33", "53")

        Assertions.assertEquals(expectedOutput, output)
    }


}