package unit.management.use_case

import fr.novu.api.management.domain.entity.*
import fr.novu.api.management.domain.repository.GroupRepository
import fr.novu.api.management.usecase.CreateGroup
import fr.novu.api.management.usecase.exceptions.GroupCreatorNotProvidedException
import fr.novu.api.management.usecase.exceptions.GroupNameNotProvidedException
import fr.novu.api.management.usecase.output.CreateGroupOutput
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.NullAndEmptySource
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.Mockito.*
import org.mockito.kotlin.argumentCaptor

@DisplayName("Create a group")
internal class CreateGroupTest {

    private lateinit var createGroup: CreateGroup
    private lateinit var mockGroupRepository: GroupRepository

    @BeforeEach
    fun setup() {
        mockGroupRepository = mock(GroupRepository::class.java)
        createGroup = CreateGroup(mockGroupRepository)
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n"])
    @DisplayName("A group name should be provided")
    fun should_throw_empty_name_exception(groupName: String?) {
        assertThrows(GroupNameNotProvidedException::class.java) {
            createGroup.execute("3333", groupName)
        }
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n"])
    @DisplayName("A group creator should be provided")
    fun should_throw_no_group_creator_provided(userId: String?) {
        assertThrows(GroupCreatorNotProvidedException::class.java) {
            createGroup.execute(userId, "example")
        }
    }

    @Test
    @DisplayName("Should save the new group with the creator as a member")
    fun should_save_new_group() {
        val nextGroupId = GroupId("22342")
        val joinCode = "novvpdnfiumkfovmlk,vsdùpfml"
        `when`(mockGroupRepository.nextGroupIdentity()).thenReturn(nextGroupId)
        `when`(mockGroupRepository.generateJoinCode()).thenReturn(joinCode)


        val participants = mutableListOf(Participant(ParticipantId(UserId("44"), nextGroupId), true))
        val expectedGroup = Group(nextGroupId, "example", participants, joinCode)

        createGroup.execute("44", "example")

        val groupArgument = argumentCaptor<Group>()

        verify(mockGroupRepository, times(1)).saveGroup(groupArgument.capture())

        val capturedGroup = groupArgument.firstValue
        assertThat(capturedGroup).usingRecursiveComparison().isEqualTo(expectedGroup)
    }

    @Test
    @DisplayName("Should return Create Group Response")
    fun should_return_create_group_response() {
        val nextGroupId = GroupId("22342")
        val joinCode = "novvpdnfiumkfovmlk,vsdùpfml"
        `when`(mockGroupRepository.generateJoinCode()).thenReturn(joinCode)
        `when`(mockGroupRepository.nextGroupIdentity()).thenReturn(nextGroupId)

        val expectedResponse = CreateGroupOutput(nextGroupId.value, joinCode)
        assertThat(createGroup.execute("44", "example")).isEqualTo(expectedResponse)
    }


}