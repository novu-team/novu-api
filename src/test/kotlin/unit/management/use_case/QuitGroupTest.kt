package unit.management.use_case

import fr.novu.api.management.domain.entity.*
import fr.novu.api.management.domain.repository.GroupRepository
import fr.novu.api.management.usecase.QuitGroup
import fr.novu.api.management.usecase.exceptions.NotEnoughAdminException
import fr.novu.api.management.usecase.exceptions.ParticipantNotInGroupException
import fr.novu.api.management.usecase.exceptions.UnknownGroupException
import fr.novu.api.management.usecase.output.QuitGroupOutput
import fr.novu.api.management.usecase.output.QuitGroupParticipantOutput
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.NullAndEmptySource
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.Mockito.*
import org.mockito.kotlin.argumentCaptor

@DisplayName("Quit a group")
internal class QuitGroupTest {

    private lateinit var quitGroup: QuitGroup
    private lateinit var mockGroupRepository: GroupRepository

    @BeforeEach
    fun setup() {
        mockGroupRepository = mock(GroupRepository::class.java)
        quitGroup = QuitGroup(mockGroupRepository)

        val participantId = ParticipantId(UserId("53"), GroupId("33"))
        val participant = Participant(participantId, false)

        val participantId2 = ParticipantId(UserId("434334"), GroupId("33"))
        val participant2 = Participant(participantId2, true)
        val group = Group(GroupId("33"), "name", mutableListOf(participant, participant2))


        `when`(mockGroupRepository.findGroup(GroupId("33"))).thenReturn(group)
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "ddd"])
    @DisplayName("Should reject wrong group id")
    fun should_throw_wrong_group_exception(groupId: String?) {
        `when`(mockGroupRepository.findGroup(GroupId("ddd"))).thenReturn(null)
        Assertions.assertThrows(UnknownGroupException::class.java) {
            quitGroup.execute("53", groupId)
        }
    }


    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "55"])
    @DisplayName("Participant should be in the group")
    fun should_throw_participant_not_in_group_exception(userId: String?) {
        Assertions.assertThrows(ParticipantNotInGroupException::class.java) {
            quitGroup.execute(userId, "33")
        }
    }

    @Test
    @DisplayName("Should not be able to quit group if no admin left")
    fun should_throw_not_enough_admin_exception() {
        Assertions.assertThrows(NotEnoughAdminException::class.java) {
            quitGroup.execute("434334", "33")
        }
    }

    @Test
    @DisplayName("Should remove participant of the group")
    fun should_add_participant_in_group() {
        val participantId2 = ParticipantId(UserId("434334"), GroupId("33"))
        val participant2 = Participant(participantId2, true)

        val expectedGroup = Group(GroupId("33"), "name", mutableListOf(participant2))

        quitGroup.execute("53", "33")

        val groupArgument = argumentCaptor<Group>()
        verify(mockGroupRepository, times(1)).updateGroup(groupArgument.capture())
        val capturedGroup = groupArgument.firstValue

        assertThat(capturedGroup)
            .usingRecursiveComparison()
            .isEqualTo(expectedGroup)
    }

    @Test
    @DisplayName("Should return new group output")
    fun should_return_new_group() {
        val response = quitGroup.execute("53", "33")

        val participantOutput1 = QuitGroupParticipantOutput("434334", "33", true)
        val expectedOutput = QuitGroupOutput("33", "name", listOf(participantOutput1))

        Assertions.assertEquals(expectedOutput, response)
    }


}