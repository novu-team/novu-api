package unit.management.use_case

import fr.novu.api.management.domain.entity.*
import fr.novu.api.management.domain.repository.GroupRepository
import fr.novu.api.management.domain.repository.UserRepository
import fr.novu.api.management.usecase.JoinGroup
import fr.novu.api.management.usecase.exceptions.AlreadyInGroupException
import fr.novu.api.management.usecase.exceptions.UnknownGroupException
import fr.novu.api.management.usecase.exceptions.UnknownUserException
import fr.novu.api.management.usecase.output.JoinGroupOutput
import fr.novu.api.management.usecase.output.JoinGroupParticipantOutput
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.NullAndEmptySource
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.Mockito.*
import org.mockito.kotlin.argumentCaptor

@DisplayName("Join a group")
internal class JoinGroupTest {

    private lateinit var joinGroup: JoinGroup
    private lateinit var mockGroupRepository: GroupRepository
    private lateinit var mockUserRepository: UserRepository

    @BeforeEach
    fun setup() {
        mockGroupRepository = mock(GroupRepository::class.java)
        mockUserRepository = mock(UserRepository::class.java)
        joinGroup = JoinGroup(mockGroupRepository, mockUserRepository)

        val participantId = ParticipantId(UserId("53"), GroupId("33"))
        val participant = Participant(participantId, false)
        val group = Group(GroupId("33"), "name", mutableListOf(participant), "hueipsc")
        val user = User(UserId("53"))
        val user2 = User(UserId("SDFdsv"))

        `when`(mockGroupRepository.findGroupToJoin("hueipsc")).thenReturn(group)
        `when`(mockUserRepository.findUser(UserId("53"))).thenReturn(user)
        `when`(mockUserRepository.findUser(UserId("SDFdsv"))).thenReturn(user2)
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "ddd"])
    @DisplayName("Should reject wrong join code")
    fun should_throw_wrong_group_exception(joinCode: String?) {
        `when`(mockGroupRepository.findGroupToJoin("ddd")).thenReturn(null)
        Assertions.assertThrows(UnknownGroupException::class.java) {
            joinGroup.execute("53", joinCode)
        }
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = ["  ", "\t", "\n", "ddsdf"])
    @DisplayName("Should reject wrong user id")
    fun should_throw_wrong_user_exception(userId: String?) {
        `when`(mockUserRepository.findUser(UserId("ddsdf"))).thenReturn(null)
        Assertions.assertThrows(UnknownUserException::class.java) {
            joinGroup.execute(userId, "hueipsc")
        }
    }

    @Test
    @DisplayName("Should reject if already in group")
    fun should_throw_already_in_group_exception() {
        Assertions.assertThrows(AlreadyInGroupException::class.java) {
            joinGroup.execute("53", "hueipsc")
        }
    }

    @Test
    @DisplayName("Should add participant in group")
    fun should_add_participant_in_group() {
        val participantId = ParticipantId(UserId("53"), GroupId("33"))
        val participant = Participant(participantId, false)
        val newParticipant = Participant(
            ParticipantId(
                UserId("SDFdsv"),
                GroupId("33")
            ), false
        )

        val expectedGroup = Group(GroupId("33"), "name", mutableListOf(participant, newParticipant), "hueipsc")

        joinGroup.execute("SDFdsv", "hueipsc")

        val groupArgument = argumentCaptor<Group>()
        verify(mockGroupRepository, times(1)).updateGroup(groupArgument.capture())
        val capturedGroup = groupArgument.firstValue

        assertThat(capturedGroup)
            .usingRecursiveComparison()
            .isEqualTo(expectedGroup)
    }

    @Test
    @DisplayName("Should return response")
    fun should_return_new_group() {
        val response = joinGroup.execute("SDFdsv", "hueipsc")

        val joinGroupParticipantOutput1 = JoinGroupParticipantOutput("53", "33", false)
        val joinGroupParticipantOutput2 = JoinGroupParticipantOutput("SDFdsv", "33", false)
        val expectedOutput =
            JoinGroupOutput("33", "name", listOf(joinGroupParticipantOutput1, joinGroupParticipantOutput2))


        Assertions.assertEquals(expectedOutput, response)
    }


}