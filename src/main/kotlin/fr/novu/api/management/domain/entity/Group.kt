package fr.novu.api.management.domain.entity

import java.time.LocalDate

class Group(
    val groupId: GroupId,
    var name: String,
    var participants: MutableList<Participant>,
    var codeToJoin: String? = null,
    var startDate: LocalDate? = null,
    var endDate: LocalDate? = null,
    var location: Location? = null
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Group

        if (groupId != other.groupId) return false

        return true
    }

    override fun hashCode(): Int {
        return groupId.hashCode()
    }

    override fun toString(): String {
        return "Group(groupId=$groupId, name='$name')"
    }

    fun addParticipant(
        user: User
    ) {
        participants += Participant(ParticipantId(user.userId, groupId), false)
    }

    fun doesNotContainOtherAdmin(
        participant: Participant
    ): Boolean = participants.none { otherParticipant ->
        otherParticipant != participant && otherParticipant.isAdmin
    }

    fun getParticipant(userId: String): Participant? =
        participants.find { participant -> participant.id.userId.value == userId }

    fun removeParticipant(participant: Participant) {
        participants.remove(participant)
    }


}