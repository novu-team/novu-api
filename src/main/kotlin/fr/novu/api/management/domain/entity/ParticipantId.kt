package fr.novu.api.management.domain.entity

data class ParticipantId(val userId: UserId, val groupId: GroupId)
