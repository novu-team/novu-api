package fr.novu.api.management.domain.entity

data class UserId(val value: String)
