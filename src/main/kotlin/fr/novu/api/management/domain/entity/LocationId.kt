package fr.novu.api.management.domain.entity

data class LocationId(val value: String)
