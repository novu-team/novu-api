package fr.novu.api.management.domain.repository

import fr.novu.api.management.domain.entity.Tag
import fr.novu.api.management.domain.entity.TagId
import org.springframework.stereotype.Repository

interface TagRepository {
    fun saveTag(tag: Tag)

    fun nextTagIdentity(): TagId
}