package fr.novu.api.management.domain.entity

class Participant(val id: ParticipantId, var isAdmin: Boolean, var participate: Boolean = false) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Participant

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun toString(): String {
        return "Member(id=$id, isAdmin=$isAdmin)"
    }


}