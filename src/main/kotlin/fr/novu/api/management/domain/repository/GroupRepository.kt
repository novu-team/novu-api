package fr.novu.api.management.domain.repository

import fr.novu.api.management.domain.entity.Group
import fr.novu.api.management.domain.entity.GroupId
import fr.novu.api.management.domain.entity.UserId

interface GroupRepository {
    fun saveGroup(group: Group)
    fun nextGroupIdentity(): GroupId
    fun findGroup(groupId: GroupId): Group?
    fun findGroupToJoin(joinCode: String): Group?
    fun updateGroup(group: Group)
    fun findAllByUser(userId: UserId): List<Group>
    fun generateJoinCode(): String
}