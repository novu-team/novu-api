package fr.novu.api.management.domain.exceptions

abstract class InvalidException(message: String) : Exception(message)