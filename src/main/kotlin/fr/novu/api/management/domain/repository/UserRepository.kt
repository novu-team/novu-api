package fr.novu.api.management.domain.repository

import fr.novu.api.management.domain.entity.User
import fr.novu.api.management.domain.entity.UserId

interface UserRepository {
    fun findUser(userId: UserId): User?
    fun deleteById(id: String)
}