package fr.novu.api.management.domain.entity

data class GroupId(val value: String)
