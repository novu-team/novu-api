package fr.novu.api.management.domain.exceptions

abstract class ConflictException(message: String) : Exception(message)