package fr.novu.api.management.domain.exceptions

class TagTypeNotProvidedException : InvalidException("A type should be provided") {
}