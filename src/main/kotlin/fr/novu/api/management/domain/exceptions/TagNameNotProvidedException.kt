package fr.novu.api.management.domain.exceptions

class TagNameNotProvidedException : InvalidException("A name should be provided") {

}