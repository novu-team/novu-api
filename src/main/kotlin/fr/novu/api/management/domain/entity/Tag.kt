package fr.novu.api.management.domain.entity

data class Tag(
    val id: String,
    val name: String,
    val type: String
)