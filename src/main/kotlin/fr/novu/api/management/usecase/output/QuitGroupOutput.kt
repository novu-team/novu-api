package fr.novu.api.management.usecase.output

data class QuitGroupOutput(
    val groupId: String,
    val name: String,
    val participants: List<QuitGroupParticipantOutput>
) {


    override fun toString(): String {
        return "JoinGroupOutput(groupId='$groupId', name='$name', participants=$participants)"
    }
}