package fr.novu.api.management.usecase.exceptions

import fr.novu.api.management.domain.exceptions.InvalidException

class ParticipantNotAdminException : InvalidException("The participant is not an admin")