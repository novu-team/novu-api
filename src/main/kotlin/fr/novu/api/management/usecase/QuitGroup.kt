package fr.novu.api.management.usecase

import fr.novu.api.management.domain.entity.Group
import fr.novu.api.management.domain.entity.GroupId
import fr.novu.api.management.domain.repository.GroupRepository
import fr.novu.api.management.usecase.exceptions.NotEnoughAdminException
import fr.novu.api.management.usecase.exceptions.ParticipantNotInGroupException
import fr.novu.api.management.usecase.exceptions.UnknownGroupException
import fr.novu.api.management.usecase.output.QuitGroupOutput
import fr.novu.api.management.usecase.output.QuitGroupParticipantOutput
import org.springframework.stereotype.Service

@Service
class QuitGroup(
    private val groupRepository: GroupRepository
) {
    fun execute(userId: String?, groupId: String?): QuitGroupOutput {
        val group = findGroup(groupId)

        val participant = group.getParticipant(userId ?: "") ?: throw ParticipantNotInGroupException()

        if (group.doesNotContainOtherAdmin(participant)) throw NotEnoughAdminException()

        group.removeParticipant(participant)

        groupRepository.updateGroup(group)

        return mapToOutput(group)
    }

    private fun mapToOutput(group: Group) =
        QuitGroupOutput(group.groupId.value, group.name, group.participants.map { participant ->
            QuitGroupParticipantOutput(
                participant.id.userId.value, participant.id.groupId.value, participant.isAdmin
            )
        })


    private fun findGroup(groupId: String?): Group {
        groupId ?: throw UnknownGroupException()
        return groupRepository.findGroup(GroupId(groupId)) ?: throw UnknownGroupException()
    }
}