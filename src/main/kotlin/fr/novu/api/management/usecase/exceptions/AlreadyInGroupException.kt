package fr.novu.api.management.usecase.exceptions

import fr.novu.api.management.domain.exceptions.ConflictException

class AlreadyInGroupException : ConflictException("Participant already in group !")