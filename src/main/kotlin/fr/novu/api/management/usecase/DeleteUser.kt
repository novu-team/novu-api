package fr.novu.api.management.usecase

import fr.novu.api.management.domain.entity.User
import fr.novu.api.management.domain.entity.UserId
import fr.novu.api.management.domain.repository.UserRepository
import fr.novu.api.management.usecase.exceptions.UnknownUserException
import org.springframework.stereotype.Service

@Service
class DeleteUser (private val userRepository: UserRepository) {
    fun execute(userId: String) {
        val user = findUser(userId)

        if (user != null) {
           userRepository.deleteById(user.userId.value)
        }
    }

    private fun findUser(userId: String?): User? {
        userId ?: throw UnknownUserException()
        return userRepository.findUser(UserId(userId))
    }
}