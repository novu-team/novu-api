package fr.novu.api.management.usecase

import fr.novu.api.authentication.domain.repository.UserRepository
import fr.novu.api.authentication.domain.entity.User
import fr.novu.api.management.usecase.output.GetCountOutput
import fr.novu.api.management.usecase.output.GetUsersOutput
import fr.novu.api.management.usecase.output.UserOutput
import org.springframework.stereotype.Service
import kotlin.math.ceil


@Service
class GetUsersCount(
    private val userRepository: UserRepository
) {
    fun execute(): GetCountOutput {
        val count = userRepository.count()
        val maxPage = ceil(count.toDouble().div(20))
        return mapToOutput(count, maxPage)
    }

    private fun mapToOutput(
        count: Number, maxPage: Double
    ) = GetCountOutput(
        count = count,
        maxPage = maxPage + 1
    )
}