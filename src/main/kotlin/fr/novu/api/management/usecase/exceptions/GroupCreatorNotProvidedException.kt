package fr.novu.api.management.usecase.exceptions

import fr.novu.api.management.domain.exceptions.InvalidException

class GroupCreatorNotProvidedException : InvalidException("A group creator should be provided")