package fr.novu.api.management.usecase.exceptions

import fr.novu.api.management.domain.exceptions.InvalidException

class NotEnoughAdminException : InvalidException("A group should always have an admin")