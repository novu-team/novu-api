package fr.novu.api.management.usecase

import fr.novu.api.management.domain.entity.Group
import fr.novu.api.management.domain.entity.GroupId
import fr.novu.api.management.domain.entity.Participant
import fr.novu.api.management.domain.repository.GroupRepository
import fr.novu.api.management.usecase.exceptions.ParticipantNotAdminException
import fr.novu.api.management.usecase.exceptions.ParticipantNotInGroupException
import fr.novu.api.management.usecase.exceptions.UnknownGroupException
import fr.novu.api.management.usecase.output.AddGroupAdminOutput
import org.springframework.stereotype.Service

@Service
class AddGroupAdmin(
    private val groupRepository: GroupRepository
) {
    fun execute(userId: String?, groupId: String?, futureAdminId: String?): AddGroupAdminOutput {
        val group = findGroup(groupId)

        val participant = group.getParticipant(userId ?: "") ?: throw ParticipantNotInGroupException()
        assertParticipantIsAdmin(participant)

        val futureAdmin = group.getParticipant(futureAdminId ?: "")

        if (futureAdmin != null) {
            futureAdmin.isAdmin = true
        } else {
            throw ParticipantNotInGroupException()
        }

        groupRepository.updateGroup(group)
        return AddGroupAdminOutput(futureAdminId ?: "", groupId ?: "", true)
    }

    private fun assertParticipantIsAdmin(participant: Participant) {
        if (!participant.isAdmin) throw ParticipantNotAdminException()
    }

    private fun findGroup(groupId: String?): Group {
        groupId ?: throw UnknownGroupException()
        return groupRepository.findGroup(GroupId(groupId)) ?: throw UnknownGroupException()
    }
}