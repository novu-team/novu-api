package fr.novu.api.management.usecase.output


data class QuitGroupParticipantOutput(
    val userId: String,
    val groupId: String,
    val isAdmin: Boolean
) {


    override fun toString(): String {
        return "ParticipantResponse(userId='$userId', groupId='$groupId', isAdmin=$isAdmin)"
    }


}