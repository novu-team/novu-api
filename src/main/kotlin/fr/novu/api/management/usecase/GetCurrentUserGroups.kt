package fr.novu.api.management.usecase

import fr.novu.api.management.domain.entity.Group
import fr.novu.api.management.domain.entity.User
import fr.novu.api.management.domain.entity.UserId
import fr.novu.api.management.domain.repository.GroupRepository
import fr.novu.api.management.domain.repository.UserRepository
import fr.novu.api.management.usecase.exceptions.UnknownUserException
import fr.novu.api.management.usecase.output.GetUserGroupsOutput
import fr.novu.api.management.usecase.output.GroupOutput
import fr.novu.api.management.usecase.output.ParticipantOutput
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service
class GetCurrentUserGroups(
    private val userRepository: UserRepository,
    private val groupRepository: GroupRepository
) {
    fun execute(userId: String?): GetUserGroupsOutput {
        val user = findUser(userId)
        val groups = groupRepository.findAllByUser(user.userId)

        val currentGroups = groups.filter { group ->
            group.endDate?.isAfter(LocalDate.now()) == true
                    || group.endDate?.isEqual(LocalDate.now()) == true
        }

        return mapToOutput(user, currentGroups)
    }

    private fun mapToOutput(
        user: User,
        groups: List<Group>
    ) = GetUserGroupsOutput(user.userId.value,
        groups.map { group ->
            GroupOutput(
                group.groupId.value,
                group.name,
                group.participants.map { participant ->
                    ParticipantOutput(
                        participant.id.userId.value,
                        participant.id.groupId.value,
                        participant.participate,
                        participant.isAdmin
                    )
                }.toMutableList(),
                group.startDate, group.endDate,
                group.location?.id?.value
            )
        }
    )

    private fun findUser(userId: String?): User {
        userId ?: throw UnknownUserException()
        return userRepository.findUser(UserId(userId)) ?: throw UnknownUserException()
    }
}