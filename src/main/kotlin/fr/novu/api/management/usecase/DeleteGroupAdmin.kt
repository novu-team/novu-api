package fr.novu.api.management.usecase

import fr.novu.api.management.domain.entity.Group
import fr.novu.api.management.domain.entity.GroupId
import fr.novu.api.management.domain.entity.Participant
import fr.novu.api.management.domain.repository.GroupRepository
import fr.novu.api.management.usecase.exceptions.NotEnoughAdminException
import fr.novu.api.management.usecase.exceptions.ParticipantNotAdminException
import fr.novu.api.management.usecase.exceptions.ParticipantNotInGroupException
import fr.novu.api.management.usecase.exceptions.UnknownGroupException
import org.springframework.stereotype.Service

@Service
class DeleteGroupAdmin(
    private val groupRepository: GroupRepository
) {
    fun execute(userId: String?, groupId: String?, adminToDeleteId: String?) {
        val group = findGroup(groupId)

        val participant = group.getParticipant(userId ?: "") ?: throw ParticipantNotInGroupException()
        assertParticipantIsAdmin(participant)

        val adminToRemove = group.getParticipant(adminToDeleteId ?: "") ?: throw ParticipantNotInGroupException()

        if (group.doesNotContainOtherAdmin(adminToRemove)) throw NotEnoughAdminException()

        group.removeParticipant(adminToRemove)

        groupRepository.updateGroup(group)
    }

    private fun findGroup(groupId: String?): Group {
        groupId ?: throw UnknownGroupException()
        return groupRepository.findGroup(GroupId(groupId)) ?: throw UnknownGroupException()
    }

    private fun assertParticipantIsAdmin(participant: Participant) {
        if (!participant.isAdmin) throw ParticipantNotAdminException()
    }
}