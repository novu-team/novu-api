package fr.novu.api.management.usecase

import fr.novu.api.management.domain.entity.Group
import fr.novu.api.management.domain.entity.User
import fr.novu.api.management.domain.entity.UserId
import fr.novu.api.management.domain.repository.GroupRepository
import fr.novu.api.management.domain.repository.UserRepository
import fr.novu.api.management.usecase.exceptions.AlreadyInGroupException
import fr.novu.api.management.usecase.exceptions.UnknownGroupException
import fr.novu.api.management.usecase.exceptions.UnknownUserException
import fr.novu.api.management.usecase.output.JoinGroupOutput
import fr.novu.api.management.usecase.output.JoinGroupParticipantOutput
import org.springframework.stereotype.Service

@Service
class JoinGroup(
    private val groupRepository: GroupRepository,
    private val userRepository: UserRepository
) {
    fun execute(userId: String?, joinCode: String?): JoinGroupOutput {
        val group = findGroup(joinCode)
        val user = findUser(userId)

        assertThatParticipantNotInGroup(group, user)
        group.addParticipant(user)
        groupRepository.updateGroup(group)

        return mapToOutput(group)
    }

    private fun mapToOutput(group: Group) = JoinGroupOutput(
        group.groupId.value,
        group.name,
        group.participants.map { participant ->
            JoinGroupParticipantOutput(
                participant.id.userId.value, participant.id.groupId.value, participant.isAdmin
            )
        }
    )


    private fun assertThatParticipantNotInGroup(
        group: Group, user: User
    ) {
        val alreadyInGroup = group.participants.any { participant -> participant.id.userId == user.userId }
        if (alreadyInGroup) {
            throw AlreadyInGroupException()
        }
    }

    private fun findUser(userId: String?): User {
        userId ?: throw UnknownUserException()
        return userRepository.findUser(UserId(userId)) ?: throw UnknownUserException()
    }

    private fun findGroup(joinCode: String?): Group {
        joinCode ?: throw UnknownGroupException()
        return groupRepository.findGroupToJoin(joinCode) ?: throw UnknownGroupException()
    }
}