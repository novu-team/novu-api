package fr.novu.api.management.usecase.exceptions

import fr.novu.api.management.domain.exceptions.InvalidException

class ParticipantNotInGroupException : InvalidException("The participant does not belong the this group")