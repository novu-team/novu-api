package fr.novu.api.management.usecase

import fr.novu.api.authentication.domain.entity.User
import fr.novu.api.shared.infrastructure.database.repository.UserRepositoryPaging
import fr.novu.api.authentication.infra.db.mapper.UserMapper
import fr.novu.api.management.usecase.output.GetUsersOutput
import fr.novu.api.management.usecase.output.UserOutput
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service

@Service
class GetUsers(
    private val userRepositoryPaging: UserRepositoryPaging,
    private val userMapper: UserMapper
) {
    fun execute(page: String): GetUsersOutput {
        val paging = PageRequest.of(page.toInt(), 20, Sort.by("id"))
        val pageUsers = userRepositoryPaging.findAll(paging)
        val users = userMapper.mapToUsersEntity(pageUsers)

        return mapToOutput(users)
    }

    private fun mapToOutput(
        users: MutableList<User>
    ) = GetUsersOutput(
        users.map { user -> UserOutput(
            user.id.uuid,
            user.email,
            user.firstname,
            user.name,
            user.isAdmin,
            user.phoneNumber.toString()
        )})
}