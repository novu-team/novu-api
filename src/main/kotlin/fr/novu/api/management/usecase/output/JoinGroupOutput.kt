package fr.novu.api.management.usecase.output

data class JoinGroupOutput(
    val groupId: String,
    val name: String,
    val participants: List<JoinGroupParticipantOutput>
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as JoinGroupOutput

        if (groupId != other.groupId) return false
        if (name != other.name) return false
        if (participants != other.participants) return false

        return true
    }

    override fun hashCode(): Int {
        var result = groupId.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + participants.hashCode()
        return result
    }

    override fun toString(): String {
        return "JoinGroupOutput(groupId='$groupId', name='$name', participants=$participants)"
    }
}