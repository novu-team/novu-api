package fr.novu.api.management.usecase.output

data class GetUsersOutput(
    val users: List<UserOutput>
)