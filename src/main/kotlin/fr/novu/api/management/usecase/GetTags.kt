//package fr.novu.api.management.usecase
//
//import fr.novu.api.management.adapters.mapper.GetTagsMapper
//import fr.novu.api.management.adapters.response.GetTagsResponse
//import fr.novu.api.shared.infrastructure.database.repository.TagRepositoryPaging
//import org.springframework.data.domain.PageRequest
//import org.springframework.data.domain.Sort
//import org.springframework.stereotype.Service
//
//@Service
//class GetTags(
//    private val tagRepositoryPaging: TagRepositoryPaging,
//    private val getTagsMapper: GetTagsMapper
//) {
//    fun execute(page: String): GetTagsResponse {
//        val paging = PageRequest.of(page.toInt(), 20, Sort.by("id"))
//        val pageTags = tagRepositoryPaging.findAll(paging)
//        val tags = getTagsMapper.mapMySQLToEntity(pageTags)
//
//        return getTagsMapper.mapToResponse(tags)
//    }
//}