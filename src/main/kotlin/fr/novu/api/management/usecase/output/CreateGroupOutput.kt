package fr.novu.api.management.usecase.output

data class CreateGroupOutput(val groupId: String, val joinCode: String) {

    override fun toString(): String {
        return "CreateGroupResponse(groupId='$groupId')"
    }


}