package fr.novu.api.management.usecase.output

data class AddGroupAdminOutput(val userId: String, val groupId: String, val isAdmin: Boolean)