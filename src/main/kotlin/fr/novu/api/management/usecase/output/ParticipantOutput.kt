package fr.novu.api.management.usecase.output


data class ParticipantOutput(
    val userId: String,
    val groupId: String,
    val participate: Boolean,
    val isAdmin: Boolean
) {


    override fun toString(): String {
        return "ParticipantOutput(userId='$userId', groupId='$groupId', participate=$participate, isAdmin=$isAdmin)"
    }
}