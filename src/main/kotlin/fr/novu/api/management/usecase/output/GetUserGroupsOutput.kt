package fr.novu.api.management.usecase.output

data class GetUserGroupsOutput(
    val userId: String,
    val groups: List<GroupOutput>
)
