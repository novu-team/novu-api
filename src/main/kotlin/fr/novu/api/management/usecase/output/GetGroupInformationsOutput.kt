package fr.novu.api.management.usecase.output

import java.time.LocalDate

data class GetGroupInformationsOutput(
    val groupId: String,
    var name: String,
    var participants: MutableList<ParticipantOutput>,
    var startDate: LocalDate? = null,
    var endDate: LocalDate? = null,
    var locationId: String? = null
)
