package fr.novu.api.management.usecase

import fr.novu.api.management.domain.entity.Group
import fr.novu.api.management.domain.entity.Participant
import fr.novu.api.management.domain.entity.ParticipantId
import fr.novu.api.management.domain.entity.UserId
import fr.novu.api.management.domain.repository.GroupRepository
import fr.novu.api.management.usecase.exceptions.GroupCreatorNotProvidedException
import fr.novu.api.management.usecase.exceptions.GroupNameNotProvidedException
import fr.novu.api.management.usecase.output.CreateGroupOutput
import org.springframework.stereotype.Service

@Service
class CreateGroup(private val groupRepository: GroupRepository) {
    fun execute(userId: String?, groupName: String?): CreateGroupOutput {
        if (groupName.isNullOrBlank()) {
            throw GroupNameNotProvidedException()
        }
        if (userId.isNullOrBlank()) {
            throw GroupCreatorNotProvidedException()
        }

        val nextGroupId = groupRepository.nextGroupIdentity()
        val newParticipant = Participant(
            ParticipantId(
                UserId(userId), nextGroupId
            ), true
        )

        val joinCode = groupRepository.generateJoinCode()
        val newGroup = Group(nextGroupId, groupName, mutableListOf(newParticipant), joinCode)

        groupRepository.saveGroup(newGroup)

        return CreateGroupOutput(newGroup.groupId.value, joinCode)
    }
}