package fr.novu.api.management.usecase

import fr.novu.api.management.domain.entity.Group
import fr.novu.api.management.domain.entity.GroupId
import fr.novu.api.management.domain.repository.GroupRepository
import fr.novu.api.management.usecase.exceptions.UnknownGroupException
import fr.novu.api.management.usecase.output.GetGroupInformationsOutput
import fr.novu.api.management.usecase.output.ParticipantOutput
import org.springframework.stereotype.Service

@Service
class GetGroupInformations(
    private val groupRepository: GroupRepository
) {
    fun execute(groupId: String?): GetGroupInformationsOutput {

        val group = findGroup(groupId)
        return mapToOutput(group)
    }

    private fun findGroup(groupId: String?): Group {
        groupId ?: throw UnknownGroupException()
        return groupRepository.findGroup(GroupId(groupId)) ?: throw UnknownGroupException()
    }

    private fun mapToOutput(
        group: Group
    ) = GetGroupInformationsOutput(
        group.groupId.value,
        group.name,
        group.participants.map { participant ->
            ParticipantOutput(
                participant.id.userId.value,
                participant.id.groupId.value,
                participant.participate,
                participant.isAdmin
            )
        }.toMutableList(),
        group.startDate, group.endDate,
        group.location?.id?.value
    )
}

