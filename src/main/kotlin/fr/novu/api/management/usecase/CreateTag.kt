//package fr.novu.api.management.usecase
//
//import fr.novu.api.management.domain.entity.Tag
//import fr.novu.api.management.domain.exceptions.TagNameNotProvidedException
//import fr.novu.api.management.domain.exceptions.TagTypeNotProvidedException
//import fr.novu.api.management.domain.repository.TagRepository
//import fr.novu.api.management.usecase.output.CreateTagOutput
//import fr.novu.api.shared.infrastructure.database.repository.TagJpaRepository
//import org.springframework.stereotype.Service
//
//@Service
//class CreateTag(
//    private val tagRepository: TagRepository,
//) {
//
//    fun execute(name: String?, type: String?): CreateTagOutput {
//        if (name.isNullOrBlank()) {
//            throw TagNameNotProvidedException()
//        }
//
//        if (type.isNullOrBlank()) {
//            throw TagTypeNotProvidedException()
//        }
//
//        val nextTagId = tagRepository.nextTagIdentity()
//
//        val newTag = Tag(
//            id = nextTagId.value,
//            name = name,
//            type = type
//        )
//
//        tagRepository.saveTag(newTag)
//
//        return CreateTagOutput(newTag.id)
//    }
//}