package fr.novu.api.management.usecase.output

data class GetCountOutput (
    val count: Number,
    val maxPage: Number,
)