package fr.novu.api.management.usecase.exceptions

import fr.novu.api.management.domain.exceptions.InvalidException

class GroupNameNotProvidedException : InvalidException("A name should be provided")