package fr.novu.api.management.usecase.output

data class CreateTagOutput(val tagId: String) {
    override fun toString(): String {
        return "CreateTagResponse('$tagId')"
    }
}