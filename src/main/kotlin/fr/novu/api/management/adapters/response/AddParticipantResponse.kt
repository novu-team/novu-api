package fr.novu.api.management.adapters.response

data class AddParticipantResponse(
    val groupId: String,
    val participants: List<ParticipantResponse>
)
