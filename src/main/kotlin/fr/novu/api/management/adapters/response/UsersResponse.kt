package fr.novu.api.management.adapters.response

data class UsersResponse(
    val userId: String,
    val email: String,
    val first_name: String,
    val name: String,
    val admin: Boolean,
    val phone_number: String
)