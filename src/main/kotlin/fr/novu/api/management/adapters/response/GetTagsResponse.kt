package fr.novu.api.management.adapters.response

data class GetTagsResponse (
    val tags: List<TagResponse>
)