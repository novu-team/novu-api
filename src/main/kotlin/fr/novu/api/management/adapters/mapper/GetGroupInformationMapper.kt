package fr.novu.api.management.adapters.mapper

import fr.novu.api.management.adapters.response.GetGroupResponse
import fr.novu.api.management.adapters.response.GroupParticipantResponse
import fr.novu.api.management.usecase.output.GetGroupInformationsOutput
import org.springframework.stereotype.Component

@Component
class GetGroupInformationMapper {

    fun mapToResponse(output: GetGroupInformationsOutput, userId: String): GetGroupResponse {
        return GetGroupResponse(
            output.name,
            output.startDate,
            output.endDate,
            output.locationId != null,
            output.participants.any { p -> p.userId == userId && p.participate },
            output.participants.map { p -> GroupParticipantResponse(p.userId, p.participate) }
        )
    }

}

