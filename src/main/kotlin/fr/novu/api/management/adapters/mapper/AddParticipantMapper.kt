package fr.novu.api.management.adapters.mapper

import fr.novu.api.management.adapters.response.AddParticipantResponse
import fr.novu.api.management.adapters.response.ParticipantResponse
import fr.novu.api.management.usecase.output.JoinGroupOutput
import org.springframework.stereotype.Component

@Component
class AddParticipantMapper {
    fun mapToAddParticipantResponse(result: JoinGroupOutput): AddParticipantResponse = AddParticipantResponse(
        result.groupId,
        result.participants.map { p -> ParticipantResponse(p.userId, p.isAdmin) }
    )
}
