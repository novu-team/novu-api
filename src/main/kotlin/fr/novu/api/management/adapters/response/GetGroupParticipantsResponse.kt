package fr.novu.api.management.adapters.response

data class GetGroupParticipantsResponse(val participants: List<GroupParticipantResponse>)
