package fr.novu.api.management.adapters.request

data class AddTagRequest(val name: String?, val type: String?)