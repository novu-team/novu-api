package fr.novu.api.management.adapters.mapper

import fr.novu.api.management.adapters.response.GetUserGroupsResponse
import fr.novu.api.management.adapters.response.GroupResponse
import fr.novu.api.management.usecase.output.GetUserGroupsOutput
import org.springframework.stereotype.Component

@Component
class GetUserGroupsMapper {

    fun mapToResponse(output: GetUserGroupsOutput): GetUserGroupsResponse {
        return GetUserGroupsResponse(
            output.userId,
            output.groups.map { group ->
                GroupResponse(
                    group.name,
                    group.startDate,
                    group.endDate,
                    group.locationId != null,
                    group.participants.any { p -> p.userId == output.userId && p.participate },
                    group.participants.size
                )
            }
        )
    }
}

