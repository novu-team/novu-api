package fr.novu.api.management.adapters.response

data class ParticipantResponse(
    val userId: String,
    val isAdmin: Boolean
)
