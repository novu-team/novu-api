package fr.novu.api.management.adapters.response

data class GroupParticipantResponse(
    val userId: String,
    val isParticipating: Boolean
)
