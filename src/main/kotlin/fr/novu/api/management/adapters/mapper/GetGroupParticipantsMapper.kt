package fr.novu.api.management.adapters.mapper

import fr.novu.api.management.adapters.response.GetGroupParticipantsResponse
import fr.novu.api.management.adapters.response.GroupParticipantResponse
import fr.novu.api.management.usecase.output.GetGroupInformationsOutput
import org.springframework.stereotype.Component

@Component
class GetGroupParticipantsMapper {

    fun mapToResponse(output: GetGroupInformationsOutput): GetGroupParticipantsResponse {
        return GetGroupParticipantsResponse(
            output.participants.map { p -> GroupParticipantResponse(p.userId, p.participate) }
        )
    }
}
