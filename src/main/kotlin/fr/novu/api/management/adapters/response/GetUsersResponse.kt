package fr.novu.api.management.adapters.response

data class GetUsersResponse (val users: List<UsersResponse>)