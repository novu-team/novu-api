package fr.novu.api.management.adapters.response


data class GetUserGroupsResponse(
    val userId: String,
    val groups: List<GroupResponse>
)
