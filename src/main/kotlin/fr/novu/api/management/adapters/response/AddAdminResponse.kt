package fr.novu.api.management.adapters.response

data class AddAdminResponse(
    val groupId: String,
    val userId: String,
    val isAdmin: Boolean
)
