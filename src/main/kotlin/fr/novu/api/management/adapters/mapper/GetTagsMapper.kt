package fr.novu.api.management.adapters.mapper

import fr.novu.api.management.adapters.response.GetTagsResponse
import fr.novu.api.management.adapters.response.TagResponse
import fr.novu.api.management.domain.entity.Tag
import fr.novu.api.shared.infrastructure.database.model.TagMySQL
import org.springframework.data.domain.Page
import org.springframework.stereotype.Component

@Component
class GetTagsMapper {
    fun mapToDomainEntity(tagMySQL: TagMySQL): Tag? {
        return Tag(
            id = tagMySQL.id,
            name = tagMySQL.name,
            type = tagMySQL.type
        )
    }

    fun mapToTagSQL(tag: Tag): TagMySQL {
        return TagMySQL(
            id = tag.id,
            name = tag.name,
            type = tag.type
        )
    }

    fun mapToResponse(tags: List<Tag>): GetTagsResponse {
        return GetTagsResponse(
            tags = tags.map {
                tag -> TagResponse(tag.name, tag.type)
            }
        )
    }

    fun mapMySQLToEntity(tagsMySQL: Page<TagMySQL>): List<Tag> {
        val tags = mutableListOf<Tag>()
        tagsMySQL.forEach { tagMySQL ->
            val tag = mapToDomainEntity(tagMySQL)

            if (tag != null) {
                tags.add(tag)
            }
        }

        return tags
    }
}