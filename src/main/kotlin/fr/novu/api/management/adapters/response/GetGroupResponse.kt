package fr.novu.api.management.adapters.response

import java.time.LocalDate

data class GetGroupResponse(
    val name: String,
    val startDate: LocalDate? = null,
    val endDate: LocalDate? = null,
    val locationSelected: Boolean,
    val isParticipating: Boolean,
    val participants: List<GroupParticipantResponse>
)
