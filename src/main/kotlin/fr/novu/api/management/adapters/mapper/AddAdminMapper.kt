package fr.novu.api.management.adapters.mapper

import fr.novu.api.management.adapters.response.AddAdminResponse
import fr.novu.api.management.usecase.output.AddGroupAdminOutput
import org.springframework.stereotype.Component

@Component
class AddAdminMapper {
    fun mapToAddAdminResponse(result: AddGroupAdminOutput): AddAdminResponse = AddAdminResponse(
        result.groupId,
        result.userId,
        result.isAdmin
    )
}
