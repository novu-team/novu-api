package fr.novu.api.management.adapters.controller

import fr.novu.api.management.adapters.mapper.*
import fr.novu.api.management.adapters.request.AddGroupRequest
import fr.novu.api.management.adapters.response.*
import fr.novu.api.management.usecase.*
import fr.novu.api.management.usecase.output.CreateGroupOutput
import fr.novu.api.management.usecase.output.GetCountOutput
import fr.novu.api.management.usecase.output.GetUsersOutput
import fr.novu.api.shared.application.security.AuthUser
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI


@Controller
@RequestMapping("/api")
class ManagementController(
//    private val createTag: CreateTag,
//    private val getTags: GetTags,
    private val deleteUser: DeleteUser,
    private val getUsersCount: GetUsersCount,
    private val getUsers: GetUsers,
    private val createGroup: CreateGroup,
    private val joinGroup: JoinGroup,
    private val quitGroup: QuitGroup,
    private val deleteGroupAdmin: DeleteGroupAdmin,
    private val addGroupAdmin: AddGroupAdmin,
    private val getPastUserGroups: GetPastUserGroups,
    private val getGroupInformations: GetGroupInformations,
    private val getCurrentUserGroups: GetCurrentUserGroups,
    private val addParticipantMapper: AddParticipantMapper,
    private val addAdminMapper: AddAdminMapper,
    private val getUserGroupsMapper: GetUserGroupsMapper,
    private val getGroupInformationMapper: GetGroupInformationMapper,
    private val getGroupParticipantsMapper: GetGroupParticipantsMapper
) {
    @PostMapping("/groups")
    fun createGroup(
        @RequestBody request: AddGroupRequest, authentication: Authentication
    ): ResponseEntity<CreateGroupOutput> {
        val authUser: AuthUser = authentication.principal as AuthUser
        val result = createGroup.execute(authUser.getId(), request.name)
        val path = "/api/groups/${result.groupId}"
        val uri = URI.create(
            ServletUriComponentsBuilder.fromCurrentContextPath().path(path).toUriString()
        )
        return ResponseEntity.created(uri).body(result)
    }

    @PostMapping("/groups/{joinCode}/participants")
    fun addParticipant(
        @PathVariable joinCode: String?, authentication: Authentication
    ): ResponseEntity<AddParticipantResponse> {
        val authUser: AuthUser = authentication.principal as AuthUser
        val userId = authUser.getId()
        val result = joinGroup.execute(userId, joinCode)
        val path = "/api/groups/${result.groupId}/participants/$userId"
        val uri = URI.create(
            ServletUriComponentsBuilder.fromCurrentContextPath().path(path).toUriString()
        )
        val response = addParticipantMapper.mapToAddParticipantResponse(result)
        return ResponseEntity.created(uri).body(response)
    }

    @PostMapping("/groups/{groupId}/admins/{adminId}")
    fun addAdmin(
        @PathVariable groupId: String?, @PathVariable adminId: String?, authentication: Authentication
    ): ResponseEntity<AddAdminResponse> {
        val authUser: AuthUser = authentication.principal as AuthUser
        val userId = authUser.getId()
        val result = addGroupAdmin.execute(userId, groupId, adminId)
        val path = "/api/groups/${result.groupId}/participants/$adminId"
        val uri = URI.create(
            ServletUriComponentsBuilder.fromCurrentContextPath().path(path).toUriString()
        )
        val response = addAdminMapper.mapToAddAdminResponse(result)
        return ResponseEntity.created(uri).body(response)
    }

    @DeleteMapping("/groups/{groupId}/quit")
    fun quitGroup(
        @PathVariable groupId: String?, authentication: Authentication
    ): ResponseEntity<Any> {
        val authUser: AuthUser = authentication.principal as AuthUser
        val userId = authUser.getId()
        quitGroup.execute(userId, groupId)
        return ResponseEntity.ok().build()
    }

    @DeleteMapping("/groups/{groupId}/admins/{adminId}")
    fun removeAdmin(
        @PathVariable groupId: String?, @PathVariable adminId: String?, authentication: Authentication
    ): ResponseEntity<Any> {
        val authUser: AuthUser = authentication.principal as AuthUser
        val userId = authUser.getId()
        deleteGroupAdmin.execute(userId, groupId, adminId)
        return ResponseEntity.ok().build()
    }

    @GetMapping("/users/groups")
    fun getUserGroups(
        authentication: Authentication
    ): ResponseEntity<List<GroupResponse>> {
        val authUser: AuthUser = authentication.principal as AuthUser
        val userId = authUser.getId()
        val result = getCurrentUserGroups.execute(userId)
        val response = getUserGroupsMapper.mapToResponse(result)
        return ResponseEntity.ok().body(response.groups)
    }

    @GetMapping("/users/groups-past")
    fun getPastUserGroups(
        authentication: Authentication
    ): ResponseEntity<List<GroupResponse>> {
        val authUser: AuthUser = authentication.principal as AuthUser
        val userId = authUser.getId()
        val result = getPastUserGroups.execute(userId)
        val response = getUserGroupsMapper.mapToResponse(result)
        return ResponseEntity.ok().body(response.groups)
    }

    @GetMapping("/groups/{groupId}")
    fun getGroup(
        @PathVariable groupId: String?,
        authentication: Authentication
    ): ResponseEntity<GetGroupResponse> {
        val authUser: AuthUser = authentication.principal as AuthUser
        val userId = authUser.getId()
        val result = getGroupInformations.execute(groupId)
        val response = getGroupInformationMapper.mapToResponse(result, userId)
        return ResponseEntity.ok().body(response)
    }

    @GetMapping("/groups/{groupId}/users")
    fun getGroupParticipants(
        @PathVariable groupId: String?
    ): ResponseEntity<GetGroupParticipantsResponse> {
        val result = getGroupInformations.execute(groupId)
        val response = getGroupParticipantsMapper.mapToResponse(result)
        return ResponseEntity.ok().body(response)
    }

    @GetMapping("/users")
    fun getUsers(
        @RequestParam("page", required = false, defaultValue = "1") page: String
    ): ResponseEntity<GetUsersOutput> {
        val response = getUsers.execute(page)
        return ResponseEntity.ok().body(response)
    }

    @GetMapping("/users/count")
    fun getUsersCount(): ResponseEntity<GetCountOutput> {
        val response = getUsersCount.execute()
        return ResponseEntity.ok().body(response)
    }

    @DeleteMapping("/users/{id}")
    fun deleteUser(
        @PathVariable id: String
    ): ResponseEntity<Any> {
        deleteUser.execute(id)
        return ResponseEntity.ok().build()
    }

//    @PostMapping("/tags")
//    fun createTags(
//        @RequestBody request: AddTagRequest
//    ): ResponseEntity<CreateTagOutput> {
//        val result = createTag.execute(request.name, request.type)
//        val path = "/api/tags/${result.tagId}"
//        val uri = URI.create(
//            ServletUriComponentsBuilder.fromCurrentContextPath().path(path).toUriString()
//        )
//        return ResponseEntity.created(uri).body(result)
//    }

//    @GetMapping("/tags")
//    fun getTags(
//        @RequestParam("page", required = false, defaultValue = "1") page: String
//    ): ResponseEntity<GetTagsResponse>{
//        val response = getTags.execute(page)
//        return ResponseEntity.ok().body(response)
//    }
}