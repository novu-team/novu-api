package fr.novu.api.management.adapters.response

data class TagResponse (
    val name: String,
    val type: String
)