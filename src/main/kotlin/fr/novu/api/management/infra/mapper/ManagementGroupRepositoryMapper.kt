package fr.novu.api.management.infra.mapper

import fr.novu.api.management.domain.entity.*
import fr.novu.api.shared.infrastructure.database.model.GroupMySQL
import fr.novu.api.shared.infrastructure.database.model.ParticipantId
import fr.novu.api.shared.infrastructure.database.model.ParticipantMySQL
import org.springframework.stereotype.Component

@Component
class ManagementGroupRepositoryMapper {

    fun mapParticipantsToMySQL(participants: List<Participant>): List<ParticipantMySQL> =
        participants.map { participant -> mapParticipantToMySQL(participant) }

    fun mapParticipantToMySQL(participant: Participant): ParticipantMySQL = ParticipantMySQL(
        ParticipantId(
            participant.id.userId.value,
            participant.id.groupId.value,
        ), participant.isAdmin
    )


    fun mapGroupToMySQL(group: Group) = GroupMySQL(group.groupId.value, group.name, group.codeToJoin)

    fun mapMySQLToGroup(groupMySQL: GroupMySQL, participantsMySQL: List<ParticipantMySQL>): Group {
        val location = groupMySQL.locationId?.let { LocationId(it) }?.let { Location(it) }
        return Group(GroupId(groupMySQL.id), groupMySQL.name, participantsMySQL.map { participantMySQL ->
            Participant(
                ParticipantId(
                    UserId(participantMySQL.participantId.userId), GroupId(participantMySQL.participantId.groupId)
                ), participantMySQL.isAdmin
            )
        }.toMutableList(), groupMySQL.codeToJoin, groupMySQL.startDate, groupMySQL.endDate, location)
    }
}