package fr.novu.api.management.infra.repository


import fr.novu.api.management.domain.entity.User
import fr.novu.api.management.domain.entity.UserId
import fr.novu.api.management.domain.repository.UserRepository
import fr.novu.api.shared.infrastructure.database.repository.UserJpaRepository
import org.springframework.stereotype.Repository


@Repository
class UserRepositoryImpl(
    private val userJpaRepository: UserJpaRepository
) : UserRepository {
    override fun findUser(userId: UserId): User? {
        val userMySQL = userJpaRepository.findById(userId.value)
        if (userMySQL.isEmpty) return null
        return User(UserId(userMySQL.get().id))
    }

    override fun deleteById(id: String) {
        return userJpaRepository.deleteById(id)
    }
}