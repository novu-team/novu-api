//package fr.novu.api.management.infra.repository
//
//import fr.novu.api.management.adapters.mapper.AddTagMapper
//import fr.novu.api.management.adapters.mapper.GetTagsMapper
//import fr.novu.api.management.domain.entity.Tag
//import fr.novu.api.management.domain.entity.TagId
//import fr.novu.api.management.domain.repository.TagRepository
//import fr.novu.api.shared.infrastructure.database.model.TagMySQL
//import fr.novu.api.shared.infrastructure.database.repository.TagJpaRepository
//import org.springframework.stereotype.Repository
//
//@Repository
//class TagRepositoryImpl(
//    private val tagJpaRepository: TagJpaRepository,
//    private val getTagsMapper: GetTagsMapper
//): TagRepository {
//    override fun saveTag(tag: Tag) {
//        val tagMySQL = getTagsMapper.mapToTagSQL(tag)
//        tagJpaRepository.save(tagMySQL)
//    }
//
//    override fun nextTagIdentity(): TagId {
//        TODO("Not yet implemented")
//    }
//
//}