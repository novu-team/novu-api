package fr.novu.api.management.infra.repository

import fr.novu.api.management.domain.entity.Group
import fr.novu.api.management.domain.entity.GroupId
import fr.novu.api.management.domain.entity.UserId
import fr.novu.api.management.domain.repository.GroupRepository
import fr.novu.api.management.infra.mapper.ManagementGroupRepositoryMapper
import fr.novu.api.shared.infrastructure.database.model.ParticipantMySQL
import fr.novu.api.shared.infrastructure.database.repository.GroupJpaRepository
import fr.novu.api.shared.infrastructure.database.repository.ParticipantJpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class GroupRepositoryImpl(
    private val groupJpaRepository: GroupJpaRepository,
    private val participantJpaRepository: ParticipantJpaRepository,
    private val mapper: ManagementGroupRepositoryMapper
) : GroupRepository {
    override fun saveGroup(group: Group) {

        val groupMySQL = mapper.mapGroupToMySQL(group)

        val participantsMySQL = mapper.mapParticipantsToMySQL(group.participants)

        groupJpaRepository.save(groupMySQL)
        participantJpaRepository.saveAll(participantsMySQL)
    }


    override fun nextGroupIdentity(): GroupId = GroupId(UUID.randomUUID().toString())
    override fun findGroup(groupId: GroupId): Group? {
        val groupSQL = groupJpaRepository.findById(groupId.value)
        if (groupSQL.isEmpty) return null
        val participantsSQL = participantJpaRepository.findAllByGroup(groupId.value)
        return mapper.mapMySQLToGroup(groupSQL.get(), participantsSQL)
    }

    override fun findGroupToJoin(joinCode: String): Group? {
        val groupSQL = groupJpaRepository.findByCodeToJoin(joinCode) ?: return null
        val participantsSQL = participantJpaRepository.findAllByGroup(groupSQL.id)
        return mapper.mapMySQLToGroup(groupSQL, participantsSQL)
    }

    override fun updateGroup(group: Group) {
        groupJpaRepository.findById(group.groupId.value).ifPresent {
            it.name = group.name
            groupJpaRepository.save(it)
        }

        val allParticipantsMySQL = participantJpaRepository.findAllByGroup(group.groupId.value)
        val participantsToDelete = getParticipantsToDelete(allParticipantsMySQL, group)
        val participantsToAdd = getParticipantsToAdd(group, allParticipantsMySQL)

        val participantsToUpdate = updateParticipants(group, allParticipantsMySQL)

        participantJpaRepository.deleteAll(participantsToDelete)
        participantJpaRepository.saveAll(participantsToAdd)
        participantJpaRepository.saveAll(participantsToUpdate)
    }

    override fun findAllByUser(userId: UserId): List<Group> {
        val groups = mutableListOf<Group>()
        val groupsMySQL = groupJpaRepository.findAllByUser(userId.value)

        groupsMySQL.forEach { group ->
            val participantsMySQL = participantJpaRepository.findAllByGroup(group.id)
            val newGroup = mapper.mapMySQLToGroup(group, participantsMySQL)
            groups.add(newGroup)
        }

        return groups
    }

    override fun generateJoinCode(): String = UUID.randomUUID().toString()


    private fun updateParticipants(group: Group, allParticipantsMySQL: List<ParticipantMySQL>): List<ParticipantMySQL> {
        val participantsToUpdate = mutableListOf<ParticipantMySQL>()
        allParticipantsMySQL.forEach { participantMySQL ->
            val participant =
                group.participants.find { p -> p.id.userId.value == participantMySQL.participantId.userId }
            if (participant != null) {
                participantMySQL.isAdmin = participant.isAdmin
                participantsToUpdate.add(participantMySQL)
            }
        }
        return participantsToUpdate
    }

    private fun getParticipantsToDelete(
        allParticipantsMySQL: List<ParticipantMySQL>, group: Group
    ) = allParticipantsMySQL.filter { participant ->
        group.getParticipant(participant.participantId.userId) == null
    }

    private fun getParticipantsToAdd(
        group: Group, allParticipantsMySQL: List<ParticipantMySQL>
    ) = group.participants.filter { participant ->
        allParticipantsMySQL.none { pMySQL -> pMySQL.participantId.userId == participant.id.userId.value }
    }.map { participant -> mapper.mapParticipantToMySQL(participant) }

}