package fr.novu.api.shared.infrastructure.database.repository

import fr.novu.api.shared.infrastructure.database.model.GroupMySQL
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface GroupJpaRepository : JpaRepository<GroupMySQL, String> {

    fun findByCodeToJoin(codeToJoin: String): GroupMySQL?

    @Query(
        "SELECT g FROM ParticipantMySQL p " + "INNER JOIN UserMySQL u ON p.participantId.userId = u.id " + "INNER JOIN GroupMySQL g ON p.participantId.groupId = g.id " + "WHERE u.id = ?1"
    )
    fun findAllByUser(userId: String): List<GroupMySQL>
}