package fr.novu.api.shared.infrastructure.database.repository

import fr.novu.api.authentication.domain.entity.User
import fr.novu.api.shared.infrastructure.database.model.UserMySQL
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepositoryPaging : PagingAndSortingRepository<UserMySQL, Long> {

}