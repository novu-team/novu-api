package fr.novu.api.shared.infrastructure.database.repository

import fr.novu.api.shared.infrastructure.database.model.AvailabilityId
import fr.novu.api.shared.infrastructure.database.model.AvailabilityMySQL
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface AvailabilityJpaRepository : JpaRepository<AvailabilityMySQL, AvailabilityId> {

    @Query("SELECT a FROM AvailabilityMySQL a WHERE a.userId = ?1 AND a.groupId = ?2")
    fun findAllByParticipant(userId: String, groupId: String): List<AvailabilityMySQL>

    @Modifying
    @Query("DELETE FROM AvailabilityMySQL WHERE userId = ?1 AND groupId = ?2")
    fun deleteAllByParticipant(userId: String, groupId: String)

}