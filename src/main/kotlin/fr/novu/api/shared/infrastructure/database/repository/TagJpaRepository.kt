//package fr.novu.api.shared.infrastructure.database.repository
//
//import fr.novu.api.authentication.domain.entity.UserId
//import fr.novu.api.management.domain.entity.TagId
//import fr.novu.api.shared.infrastructure.database.model.TagMySQL
//import org.springframework.data.jpa.repository.JpaRepository
//import org.springframework.stereotype.Repository
//
//@Repository
//interface TagJpaRepository : JpaRepository<TagMySQL, String>{
//    fun nextIdentity(): TagId
//}