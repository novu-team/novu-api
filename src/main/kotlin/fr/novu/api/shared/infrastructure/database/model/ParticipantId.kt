package fr.novu.api.shared.infrastructure.database.model

import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
class ParticipantId(
    @Column(name = "user_id", nullable = false)
    val userId: String = "",

    @Column(name = "groupe_id", nullable = false)
    val groupId: String = ""
) : java.io.Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ParticipantId

        if (userId != other.userId) return false
        if (groupId != other.groupId) return false

        return true
    }

    override fun hashCode(): Int {
        var result = userId.hashCode()
        result = 31 * result + groupId.hashCode()
        return result
    }
}