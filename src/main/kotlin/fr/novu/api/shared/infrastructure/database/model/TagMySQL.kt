package fr.novu.api.shared.infrastructure.database.model

import javax.persistence.Column
import javax.persistence.Id
import javax.persistence.Table

@Table(name = "tags")
class TagMySQL (
    @Id
    @Column(name = "id", nullable = false)
    val id: String,

    @Column(name = "name", nullable = false)
    var name: String,

    @Column(name = "type", nullable = false)
    var type: String,
)