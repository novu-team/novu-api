package fr.novu.api.shared.infrastructure.database.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "housing")
class HousingMySQL(

    @Id
    @Column(name = "id", nullable = false)
    val id: String,

    @Column(name = "user_id", nullable = false)
    val creatorId: String,

    @Column(name = "groupe_id", nullable = false)
    val groupId: String,

    @Column(name = "name", nullable = false)
    var name: String,

    @Column
    var url: String? = null,

    @Column
    var description: String? = null,

    @Column(nullable = false)
    var available: Boolean = true,

    )