package fr.novu.api.shared.infrastructure.database.repository

import fr.novu.api.shared.infrastructure.database.model.HousingMySQL
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface HousingJpaRepository : JpaRepository<HousingMySQL, String> {
    fun findByCreatorIdAndName(creatorId: String, name: String): HousingMySQL?

    fun findAllByGroupId(groupIdValue: String): List<HousingMySQL>
}