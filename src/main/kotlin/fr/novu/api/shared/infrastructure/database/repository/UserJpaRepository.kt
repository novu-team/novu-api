package fr.novu.api.shared.infrastructure.database.repository

import fr.novu.api.shared.infrastructure.database.model.UserMySQL
import org.apache.catalina.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserJpaRepository : JpaRepository<UserMySQL, String> {
    fun findByEmail(email: String): UserMySQL?
    fun findByPhoneNumber(phoneNumber: String): UserMySQL?

    override fun deleteById(id: String)
}