package fr.novu.api.shared.infrastructure.database.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "location")
class LocationMySQL(

    @Id
    @Column(name = "id", nullable = false)
    val id: String
)