package fr.novu.api.shared.infrastructure.database.model

import java.time.LocalDate
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "groupe")
class GroupMySQL(

    @Id
    @Column(name = "id", nullable = false)
    val id: String,

    @Column(name = "name", nullable = false)
    var name: String,

    @Column(name = "join_code", nullable = true)
    var codeToJoin: String? = null,

    @Column(name = "start_date", nullable = true)
    var startDate: LocalDate? = null,

    @Column(name = "end_date", nullable = true)
    var endDate: LocalDate? = null,

    @Column(name = "location_id", nullable = true)
    var locationId: String? = null,
)