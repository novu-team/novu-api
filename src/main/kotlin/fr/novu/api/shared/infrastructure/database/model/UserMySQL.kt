package fr.novu.api.shared.infrastructure.database.model

import java.time.LocalDate
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Table(name = "users")
@Entity
class UserMySQL(

    @Id
    @Column(name = "id", nullable = false)
    val id: String,

    @Column(name = "name", nullable = false)
    var name: String,

    @Column(name = "first_name", nullable = false)
    var firstName: String,

    @Column(name = "email", nullable = false)
    var email: String,

    @Column(name = "password", nullable = false)
    var password: String,

    @Column(name = "admin", nullable = false)
    var isAdmin: Boolean = false,

    @Column(name = "colour", nullable = true)
    var colour: String?,

    @Column(name = "birthday", nullable = true)
    var birthday: LocalDate?,

    @Column(name = "sex", nullable = true)
    var sex: Boolean?,

    @Column(name = "trigram", nullable = true)
    var trigram: String?,

    @Column(name = "phone_number", nullable = true)
    var phoneNumber: String?,


    )
