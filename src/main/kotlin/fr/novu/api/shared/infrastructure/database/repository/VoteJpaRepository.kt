package fr.novu.api.shared.infrastructure.database.repository

import fr.novu.api.shared.infrastructure.database.model.VoteIdMySQL
import fr.novu.api.shared.infrastructure.database.model.VoteMySQL
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface VoteJpaRepository : JpaRepository<VoteMySQL, VoteIdMySQL> {
    @Query(
        "SELECT v FROM VoteMySQL v INNER JOIN HousingMySQL h ON h.groupId = ?1" +
                " WHERE v.voteIdMySQL.housingId = h.id"
    )
    fun findAllByGroupId(groupId: String): List<VoteMySQL>

    @Query("SELECT v FROM VoteMySQL v WHERE v.voteIdMySQL.housingId = ?1")
    fun findAllByHousingId(housingId: String): List<VoteMySQL>

}