package fr.novu.api.shared.infrastructure.database.model


import java.time.LocalDate
import java.util.*
import javax.persistence.*

@Table(name = "availability")
@Entity
class AvailabilityMySQL(

    @Column(name = "user_id", nullable = false, unique = false)
    val userId: String,

    @Column(name = "groupe_id", nullable = false, unique = false)
    val groupId: String,

    @Column(name = "start_date", nullable = false, unique = false)
    val startDate: LocalDate,

    @Column(name = "end_date", nullable = false, unique = false)
    val endDate: LocalDate,

    @Id
    @GeneratedValue
    val id: UUID? = null

) {


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AvailabilityMySQL

        if (userId != other.userId) return false
        if (groupId != other.groupId) return false
        if (startDate != other.startDate) return false
        if (endDate != other.endDate) return false

        return true
    }

    override fun hashCode(): Int {
        var result = userId.hashCode()
        result = 31 * result + groupId.hashCode()
        result = 31 * result + startDate.hashCode()
        result = 31 * result + endDate.hashCode()
        return result
    }

    override fun toString(): String {
        return "AvailabilityMySQL(id=$id, userId='$userId', groupId='$groupId', startDate=$startDate, endDate=$endDate)"
    }


}
