package fr.novu.api.shared.infrastructure.database.model


import javax.persistence.Column
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table

@Table(name = "participant")
@Entity
class ParticipantMySQL(
    @EmbeddedId
    val participantId: ParticipantId,

    @Column(name = "is_admin", nullable = false)
    var isAdmin: Boolean,

    @Column(name = "participate", nullable = false)
    var participate: Boolean? = false,
)
