package fr.novu.api.shared.infrastructure.database.model

import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "vote")
class VoteMySQL(

    @EmbeddedId
    val voteIdMySQL: VoteIdMySQL,

    )