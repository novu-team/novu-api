package fr.novu.api.shared.infrastructure.database.repository

import fr.novu.api.shared.infrastructure.database.model.ParticipantId
import fr.novu.api.shared.infrastructure.database.model.ParticipantMySQL
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface ParticipantJpaRepository : JpaRepository<ParticipantMySQL, ParticipantId> {

    @Query("SELECT p FROM ParticipantMySQL p WHERE p.participantId.groupId = ?1")
    fun findAllByGroup(groupId: String): List<ParticipantMySQL>
}