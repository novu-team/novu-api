package fr.novu.api.shared.infrastructure.database.model

import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
class VoteIdMySQL(
    @Column(name = "user_id", nullable = false)
    val userId: String,

    @Column(name = "housing_id", nullable = false)
    val housingId: String
) : java.io.Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as VoteIdMySQL

        if (userId != other.userId) return false
        if (housingId != other.housingId) return false

        return true
    }

    override fun hashCode(): Int {
        var result = userId.hashCode()
        result = 31 * result + housingId.hashCode()
        return result
    }
}