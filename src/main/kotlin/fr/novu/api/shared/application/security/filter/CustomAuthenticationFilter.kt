package fr.novu.api.shared.application.security.filter

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.fasterxml.jackson.databind.ObjectMapper
import fr.novu.api.shared.application.security.AuthUser
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromStream
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class CustomAuthenticationFilter(
    private val authManager: AuthenticationManager, private val secretKey: String
) : UsernamePasswordAuthenticationFilter() {


    override fun attemptAuthentication(request: HttpServletRequest?, response: HttpServletResponse?): Authentication {
        val loginDto = request?.let { Json.decodeFromStream<LoginDto>(it.inputStream) }
        val email = loginDto?.email ?: throw UsernameNotFoundException("Wrong email")
        val password = loginDto.password
        val authToken = UsernamePasswordAuthenticationToken(email, password)
        return authManager.authenticate(authToken)
    }

    override fun successfulAuthentication(
        request: HttpServletRequest?, response: HttpServletResponse?, chain: FilterChain?, authResult: Authentication?
    ) {
        val user: AuthUser = authResult?.principal as AuthUser
        val algorithm: Algorithm = Algorithm.HMAC256(secretKey)
        val hours = 10
        val accessToken: String = JWT.create().withSubject(user.username)
            .withExpiresAt(Date(System.currentTimeMillis() + hours * 60 * 60 * 1000))
            .withIssuer(request?.requestURL.toString()).withClaim("roles", user.authorities.map { it.authority })
            .sign(algorithm)

        val refreshToken: String =
            JWT.create().withSubject(user.username).withExpiresAt(Date(System.currentTimeMillis() + 30 * 60 * 1000))
                .withIssuer(request?.requestURL.toString()).sign(algorithm)

        val tokens = hashMapOf<String, String>()
        tokens["access_token"] = accessToken
        tokens["refresh_token"] = refreshToken
        response?.contentType = APPLICATION_JSON_VALUE
        ObjectMapper().writeValue(response?.outputStream, tokens)
    }

}