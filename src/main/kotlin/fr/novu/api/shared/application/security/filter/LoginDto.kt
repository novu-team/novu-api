package fr.novu.api.shared.application.security.filter

import kotlinx.serialization.Serializable

@Serializable
data class LoginDto(val email: String, val password: String)