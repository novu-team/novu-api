package fr.novu.api.shared.application.security

import fr.novu.api.shared.infrastructure.database.model.UserMySQL
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails


class AuthUser(
    private val user: UserMySQL,
    private val authorities: MutableCollection<out GrantedAuthority>
) : UserDetails {

    fun getId(): String = user.id

    fun getEmail(): String = user.email

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return authorities
    }

    override fun getPassword(): String {
        return user.password
    }

    override fun getUsername(): String {
        return user.email
    }

    override fun isAccountNonExpired(): Boolean = true

    override fun isAccountNonLocked(): Boolean = true

    override fun isCredentialsNonExpired(): Boolean = true

    override fun isEnabled(): Boolean = true
}