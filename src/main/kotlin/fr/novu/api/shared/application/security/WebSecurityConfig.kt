package fr.novu.api.shared.application.security

import fr.novu.api.shared.application.security.filter.CustomAuthenticationFilter
import fr.novu.api.shared.application.security.filter.CustomAuthorizationFilter
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
@Configuration
@EnableWebSecurity
class WebSecurityConfig(
    private val userDetailsService: UserDetailsService
) : WebSecurityConfigurerAdapter() {

    @Value("\${algorithm.secret}")
    private val secretKey: String? = null

    override fun configure(http: HttpSecurity?) {
        val customAuthenticationFilter = secretKey?.let { CustomAuthenticationFilter(authenticationManagerBean(), it) }
        customAuthenticationFilter?.setFilterProcessesUrl("/api/login")
        http?.csrf()?.disable()
        http?.cors()
        http?.sessionManagement()?.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        http?.authorizeRequests()?.antMatchers("/api/users")?.permitAll()
        http?.authorizeRequests()?.antMatchers("/api/login")?.permitAll()
        http?.authorizeRequests()?.anyRequest()?.authenticated()
        http?.addFilterBefore(
            CustomAuthorizationFilter(secretKey, userDetailsService),
            UsernamePasswordAuthenticationFilter::class.java
        )
        http?.addFilter(customAuthenticationFilter)
    }


    override fun configure(auth: AuthenticationManagerBuilder?) {
        auth?.userDetailsService(userDetailsService)?.passwordEncoder(passwordEncoder())
    }

    @Bean
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

    @Bean
    fun passwordEncoder(): PasswordEncoder = BCryptPasswordEncoder()

}