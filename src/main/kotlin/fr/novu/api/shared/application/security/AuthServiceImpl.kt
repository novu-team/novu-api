package fr.novu.api.shared.application.security

import fr.novu.api.shared.infrastructure.database.repository.UserJpaRepository
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional
class AuthServiceImpl(
    private val userJpaRepository: UserJpaRepository
) : UserDetailsService {


    override fun loadUserByUsername(email: String?): UserDetails {
        val user = email?.let { userJpaRepository.findByEmail(it) }
            ?: throw UsernameNotFoundException("User not found with email $email")
        val authorities = mutableSetOf<SimpleGrantedAuthority>()
        if (user.isAdmin) {
            authorities.add(SimpleGrantedAuthority("ADMIN"))
        }
        return AuthUser(
            user, authorities
        )
//        return org.springframework.security.core.userdetails.User(
//            user.email,
//            user.password,
//            authorities
//        )
    }
}