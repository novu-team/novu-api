package fr.novu.api.rental.infra.repository

import fr.novu.api.rental.domain.entity.group.GroupId
import fr.novu.api.rental.domain.entity.group.ParticipantId
import fr.novu.api.rental.domain.entity.housing.HousingId
import fr.novu.api.rental.domain.entity.vote.Vote
import fr.novu.api.rental.domain.entity.vote.VoteId
import fr.novu.api.rental.domain.repository.VoteRepository
import fr.novu.api.shared.infrastructure.database.model.VoteIdMySQL
import fr.novu.api.shared.infrastructure.database.model.VoteMySQL
import fr.novu.api.shared.infrastructure.database.repository.VoteJpaRepository
import org.springframework.stereotype.Repository

@Repository
class VoteRepositoryImpl(private val voteJpaRepository: VoteJpaRepository) : VoteRepository {


    override fun findHousingVotesInGroup(groupId: GroupId): List<Vote> {
        val votes = voteJpaRepository.findAllByGroupId(groupId.value)
        return votes.map { vote ->
            Vote(
                VoteId(
                    vote.voteIdMySQL.userId,
                    HousingId(vote.voteIdMySQL.housingId)
                )
            )
        }
    }

    override fun findAllVoteByHousingId(housingId: HousingId): List<Vote> {
        val votes = voteJpaRepository.findAllByHousingId(housingId = housingId.value)
        return votes.map { vote -> Vote(VoteId(vote.voteIdMySQL.userId, HousingId(vote.voteIdMySQL.housingId))) }
    }

    override fun save(vote: Vote) {
        val newVote = VoteMySQL(VoteIdMySQL(vote.getParticipantId(), vote.id.housingId.value))
        voteJpaRepository.save(newVote)
    }

    override fun findVote(participantId: ParticipantId, housingId: HousingId): Vote? {
        val vote = voteJpaRepository.findById(VoteIdMySQL(participantId.value, housingId.value))
        if (vote.isPresent) {
            val v = vote.get()
            return Vote(VoteId(v.voteIdMySQL.userId, HousingId(v.voteIdMySQL.housingId)))
        }
        return null
    }

    override fun delete(vote: Vote) {
        voteJpaRepository.deleteById(VoteIdMySQL(vote.getParticipantId(), vote.id.housingId.value))
    }


}