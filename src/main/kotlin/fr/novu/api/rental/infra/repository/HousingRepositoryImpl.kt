package fr.novu.api.rental.infra.repository

import fr.novu.api.rental.domain.entity.group.GroupId
import fr.novu.api.rental.domain.entity.group.ParticipantId
import fr.novu.api.rental.domain.entity.housing.Housing
import fr.novu.api.rental.domain.entity.housing.HousingId
import fr.novu.api.rental.domain.repository.HousingRepository
import fr.novu.api.shared.infrastructure.database.model.HousingMySQL
import fr.novu.api.shared.infrastructure.database.repository.HousingJpaRepository
import fr.novu.api.shared.infrastructure.database.repository.VoteJpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class HousingRepositoryImpl(
    private val housingJpaRepository: HousingJpaRepository,
    private val voteJpaRepository: VoteJpaRepository
) : HousingRepository {
    override fun findByHousingIdAndGroupId(housingId: HousingId, groupId: GroupId): Housing? {
        TODO("Not yet implemented")
    }

    override fun findHousing(housingId: HousingId): Housing? {
        val housing = housingJpaRepository.findById(housingId.value)
        if (housing.isPresent) {
            val housingFound = housing.get()
            return Housing(
                HousingId(housingFound.id),

                GroupId(housingFound.groupId), ParticipantId(housingFound.creatorId),
                housingFound.name, housingFound.url, housingFound.description, housingFound.available
            )
        }
        return null
    }

    override fun addHousing(housing: Housing) {
        val newHousing = HousingMySQL(
            housing.id.value,
            housing.creatorId.value, housing.groupId.value, housing.name,
            housing.url, housing.description, housing.available
        )
        housingJpaRepository.save(newHousing)
    }

    override fun nextIdentity(): HousingId {
        return HousingId(UUID.randomUUID().toString())
    }

    override fun findByCreatorAndName(participantId: ParticipantId, name: String): Housing? {
        val housing = housingJpaRepository.findByCreatorIdAndName(participantId.value, name) ?: return null
        return Housing(
            HousingId(housing.id),
            GroupId(housing.groupId), ParticipantId(housing.creatorId),
            housing.name, housing.url, housing.description, housing.available
        )
    }

    override fun updateHousing(housing: Housing) {
        val existingHousing = housingJpaRepository.findById(housing.id.value)
        if (existingHousing.isPresent) {
            val housingToUpdate = existingHousing.get()
            housingToUpdate.url = housing.url
            housingToUpdate.description = housing.description
            housingToUpdate.available = housing.available
            housingJpaRepository.save(housingToUpdate)
        }
    }

    override fun deleteHousing(housing: Housing) {
        val housingVotes = voteJpaRepository.findAllByHousingId(housing.id.value)
        voteJpaRepository.deleteAll(housingVotes)
        housingJpaRepository.deleteById(housing.id.value)
    }


}