package fr.novu.api.rental.infra.repository

import fr.novu.api.rental.domain.entity.group.Group
import fr.novu.api.rental.domain.entity.group.GroupId
import fr.novu.api.rental.domain.entity.group.Participant
import fr.novu.api.rental.domain.entity.group.ParticipantId
import fr.novu.api.rental.domain.entity.housing.Housing
import fr.novu.api.rental.domain.entity.housing.HousingId
import fr.novu.api.rental.domain.entity.vote.Vote
import fr.novu.api.rental.domain.entity.vote.VoteId
import fr.novu.api.rental.domain.repository.GroupRepository
import fr.novu.api.shared.infrastructure.database.model.HousingMySQL
import fr.novu.api.shared.infrastructure.database.model.ParticipantMySQL
import fr.novu.api.shared.infrastructure.database.model.UserMySQL
import fr.novu.api.shared.infrastructure.database.repository.*
import org.springframework.stereotype.Repository

@Repository
class RentalGroupRepositoryImpl(
    private val groupJpaRepository: GroupJpaRepository,
    private val participantJpaRepository: ParticipantJpaRepository,
    private val housingJpaRepository: HousingJpaRepository,
    private val userJpaRepository: UserJpaRepository,
    private val voteJpaRepository: VoteJpaRepository
) : GroupRepository {

    private fun mapParticipants(
        participants: List<ParticipantMySQL>, participantInfos: List<UserMySQL>
    ): List<Participant> {
        val participantsWithInfos = mutableListOf<Participant>()

        participants.forEach { p ->
            val infos = participantInfos.find { u -> u.id == p.participantId.userId }
            val newParticipant = Participant(
                ParticipantId(p.participantId.userId), infos?.name, infos?.firstName, infos?.trigram, infos?.colour
            )
            participantsWithInfos.add(newParticipant)
        }
        return participantsWithInfos
    }

    override fun findGroupById(groupId: GroupId): Group? {
        val groupIdValue = groupId.value
        val group = groupJpaRepository.findById(groupIdValue)
        val participants = participantJpaRepository.findAllByGroup(groupIdValue)
        val participantInfos = userJpaRepository.findAllById(participants.map { p -> p.participantId.userId })
        val housings = housingJpaRepository.findAllByGroupId(groupIdValue)


        if (group.isPresent) {
            return Group(
                groupId, mapParticipants(participants, participantInfos).toMutableList(),
                group.get().locationId?.let { HousingId(it) },
                mapHousings(housings)
            )
        }
        return null
    }

    private fun mapHousings(housings: List<HousingMySQL>): MutableList<Housing> {
        val housingsWithVotes = mutableListOf<Housing>()
        housings.forEach { housingMySQL ->
            val housingVotes = voteJpaRepository.findAllByHousingId(housingMySQL.id)
            val housingId = HousingId(housingMySQL.id)
            val housing = Housing(
                housingId,
                GroupId(housingMySQL.groupId), ParticipantId(housingMySQL.creatorId),
                housingMySQL.name, housingMySQL.url, housingMySQL.description, housingMySQL.available,
                housingVotes.map { vote -> Vote(VoteId(vote.voteIdMySQL.userId, housingId)) }.toMutableList()
            )
            housingsWithVotes.add(housing)
        }
        return housingsWithVotes
    }

    override fun update(group: Group) {
        val groupDB = groupJpaRepository.findById(group.groupId.value)
        if (groupDB.isPresent) {
            val presentGroup = groupDB.get()
            presentGroup.locationId = group.assignedHousingId?.value
            groupJpaRepository.save(presentGroup)
        }
    }
}