package fr.novu.api.rental.adapters.controllers


import fr.novu.api.rental.adapters.mapper.*
import fr.novu.api.rental.adapters.request.AddHousingRequest
import fr.novu.api.rental.adapters.request.ChangeHousingRequest
import fr.novu.api.rental.adapters.response.*
import fr.novu.api.rental.usecase.housing.*
import fr.novu.api.rental.usecase.housing.input.DeleteHousingInput
import fr.novu.api.rental.usecase.vote.CancelVoteForOneHousing
import fr.novu.api.rental.usecase.vote.VoteForOneHousing
import fr.novu.api.shared.application.security.AuthUser
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI

@Controller
@RequestMapping("/api")
class RentalController(
    private val addHousing: AddHousing,
    private val deleteHousing: DeleteHousing,
    private val getGroupHousings: GetGroupHousings,
    private val cancelVoteForOneHousing: CancelVoteForOneHousing,
    private val voteForOneHousing: VoteForOneHousing,
    private val lockHousingChoice: LockHousingChoice,
    private val voteForOneHousingMapper: VoteForOneHousingMapper,
    private val getGroupHousingsMapper: GetGroupHousingsMapper,
    private val getParticipantWithoutVoteMapper: GetParticipantWithoutVoteMapper,
    private val addHousingMapper: AddHousingMapper,
    private val changeHousingInfo: ChangeHousingInfo,
    private val changeHousingMapper: ChangeHousingMapper
) {

    @PostMapping("/housings")
    fun createHousing(
        @RequestBody request: AddHousingRequest,
        authentication: Authentication
    ): ResponseEntity<AddHousingResponse> {
        val authUser: AuthUser = authentication.principal as AuthUser
        val input = addHousingMapper.mapToInput(request, authUser.getId())
        val result = addHousing.execute(input)
        val response = addHousingMapper.mapToResponse(result)
        val path = "/api/housings/${result.housingId}"
        val uri = URI.create(
            ServletUriComponentsBuilder.fromCurrentContextPath().path(path).toUriString()
        )
        return ResponseEntity.created(uri).body(response)
    }


    @PostMapping("/housings/{housingId}/votes")
    fun vote(
        @PathVariable housingId: String?,
        authentication: Authentication
    ): ResponseEntity<VoteResponse> {
        val authUser: AuthUser = authentication.principal as AuthUser
        val result = voteForOneHousing.vote(authUser.getId(), housingId)
        val path = "/api/housings/${result.housingId}/votes"
        val uri = URI.create(
            ServletUriComponentsBuilder.fromCurrentContextPath().path(path).toUriString()
        )
        return ResponseEntity.created(uri).body(voteForOneHousingMapper.mapToResponse(result))
    }


    @PutMapping("/housings/{housingId}")
    fun changeHousing(
        @RequestBody request: ChangeHousingRequest,
        @PathVariable housingId: String?,
        authentication: Authentication
    ): ResponseEntity<ChangeHousingResponse> {
        val authUser: AuthUser = authentication.principal as AuthUser
        val input = changeHousingMapper.mapToInput(request, authUser.getId(), housingId)
        val result = changeHousingInfo.execute(input)
        val response = changeHousingMapper.mapToResponse(result)
        return ResponseEntity.ok().body(response)
    }

    @PutMapping("/housings/{housingId}/lock")
    fun lockHousing(
        @PathVariable housingId: String?,
        authentication: Authentication
    ): ResponseEntity<LockHousingChoiceResponse> {
        val authUser: AuthUser = authentication.principal as AuthUser
        val result = lockHousingChoice.lock(housingId, authUser.getId())
        val response = LockHousingChoiceResponse(result.assignedHousingId, result.groupId)
        return ResponseEntity.ok().body(response)
    }

    @DeleteMapping("/housings/{housingId}")
    fun deleteHousing(
        @PathVariable housingId: String?,
        authentication: Authentication
    ): ResponseEntity<Any> {
        val authUser: AuthUser = authentication.principal as AuthUser
        val input = DeleteHousingInput(housingId, authUser.getId())
        deleteHousing.execute(input)
        return ResponseEntity.ok().build()
    }

    @DeleteMapping("/housings/{housingId}/votes")
    fun cancelVote(
        @PathVariable housingId: String?,
        authentication: Authentication
    ): ResponseEntity<Any> {
        val authUser: AuthUser = authentication.principal as AuthUser
        cancelVoteForOneHousing.execute(authUser.getId(), housingId)
        return ResponseEntity.ok().build()
    }

    @GetMapping("/groups/{groupId}/housings")
    fun getGroupHousings(
        @PathVariable groupId: String?,
        authentication: Authentication
    ): ResponseEntity<List<HousingResponse>> {
        val authUser: AuthUser = authentication.principal as AuthUser
        val result = getGroupHousings.execute(groupId)
        val response = getGroupHousingsMapper.mapToResponse(result, authUser.getId())
        return ResponseEntity.ok().body(response)
    }

    @GetMapping("/groups/{groupId}/housings/not-voted")
    fun getParticipantsWithoutHousingVote(
        @PathVariable groupId: String?
    ): ResponseEntity<List<ParticipantResponse>> {
        val result = getGroupHousings.execute(groupId)
        val response = getParticipantWithoutVoteMapper.mapToResponse(result)
        return ResponseEntity.ok().body(response)
    }


}