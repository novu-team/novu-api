package fr.novu.api.rental.adapters.mapper


import fr.novu.api.rental.adapters.request.ChangeHousingRequest
import fr.novu.api.rental.adapters.response.ChangeHousingResponse

import fr.novu.api.rental.usecase.housing.input.ChangeHousingInfoInput
import fr.novu.api.rental.usecase.housing.output.ChangeHousingOutput
import org.springframework.stereotype.Component

@Component
class ChangeHousingMapper {

    fun mapToInput(request: ChangeHousingRequest, userId: String?, housingId: String?): ChangeHousingInfoInput =
        ChangeHousingInfoInput(housingId, userId, request.url, request.description)

    fun mapToResponse(result: ChangeHousingOutput): ChangeHousingResponse =
        ChangeHousingResponse(result.housingId, result.url, result.description)
}