package fr.novu.api.rental.adapters.request

data class ChangeHousingRequest(
    val url: String?,
    val description: String?
)
