package fr.novu.api.rental.adapters.mapper


import fr.novu.api.rental.adapters.response.ParticipantResponse
import fr.novu.api.rental.usecase.housing.output.GetGroupHousingsOutput
import org.springframework.stereotype.Component

@Component
class GetParticipantWithoutVoteMapper {
    fun mapToResponse(result: GetGroupHousingsOutput): List<ParticipantResponse> {

        return result.participants
            .filter { participant ->
                result.housings.none { housing ->
                    housing.participantsVotedIds.contains(
                        participant.participantId
                    )
                }
            }.map { participant ->
                ParticipantResponse(
                    participant.firstname,
                    participant.name, participant.trigram, participant.color
                )
            }


    }


}


