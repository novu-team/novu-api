package fr.novu.api.rental.adapters.mapper

import fr.novu.api.rental.adapters.request.AddHousingRequest
import fr.novu.api.rental.adapters.response.AddHousingResponse
import fr.novu.api.rental.usecase.housing.input.AddHousingInput
import fr.novu.api.rental.usecase.housing.output.AddHousingOutput
import org.springframework.stereotype.Component

@Component
class AddHousingMapper {

    fun mapToInput(request: AddHousingRequest, userId: String): AddHousingInput =
        AddHousingInput(userId, request.groupId, request.name, request.url, request.description)

    fun mapToResponse(result: AddHousingOutput): AddHousingResponse =
        AddHousingResponse(result.housingId)
}