package fr.novu.api.rental.adapters.response

data class HousingResponse(
    val housingId: String,
    val locationUrl: String?,
    val name: String,
    val description: String?,
    val isParticipating: Boolean,
    val isSelected: Boolean,
    val participants: List<ParticipantResponse>
)
