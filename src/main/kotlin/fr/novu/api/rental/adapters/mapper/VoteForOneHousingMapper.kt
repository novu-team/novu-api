package fr.novu.api.rental.adapters.mapper

import fr.novu.api.rental.adapters.response.VoteResponse
import fr.novu.api.rental.usecase.vote.output.VoteOutput
import org.springframework.stereotype.Component

@Component
class VoteForOneHousingMapper {

    fun mapToResponse(result: VoteOutput) =
        VoteResponse(result.participantId, result.housingId)
}


