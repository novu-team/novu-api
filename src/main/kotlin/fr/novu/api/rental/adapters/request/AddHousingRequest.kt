package fr.novu.api.rental.adapters.request

data class AddHousingRequest(
    val groupId: String?,
    val name: String?,
    val url: String?,
    val description: String?
)
