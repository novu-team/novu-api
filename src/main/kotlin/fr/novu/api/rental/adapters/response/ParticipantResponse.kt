package fr.novu.api.rental.adapters.response

data class ParticipantResponse(
    val firstname: String?,
    val name: String?,
    val trigram: String?,
    val color: String?
)
