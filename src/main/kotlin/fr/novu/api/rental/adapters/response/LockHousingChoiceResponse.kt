package fr.novu.api.rental.adapters.response

data class LockHousingChoiceResponse(
    val housingId: String, val groupId: String
)