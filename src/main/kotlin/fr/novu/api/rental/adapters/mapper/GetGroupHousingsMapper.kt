package fr.novu.api.rental.adapters.mapper

import fr.novu.api.rental.adapters.response.HousingResponse
import fr.novu.api.rental.adapters.response.ParticipantResponse
import fr.novu.api.rental.usecase.housing.output.GetGroupHousingsOutput
import fr.novu.api.rental.usecase.housing.output.ParticipantOutput
import org.springframework.stereotype.Component

@Component
class GetGroupHousingsMapper {

    private fun mapParticipants(
        participantsVotedIds: List<String>,
        participants: List<ParticipantOutput>
    ): List<ParticipantResponse> {
        val participantResponses = mutableListOf<ParticipantResponse>()
        participantsVotedIds.forEach { participantId ->
            val user = participants.find { p -> p.participantId == participantId }
            val participant = ParticipantResponse(user?.firstname, user?.name, user?.trigram, user?.color)
            participantResponses.add(participant)
        }
        return participantResponses
    }

    fun mapToResponse(result: GetGroupHousingsOutput, userId: String): List<HousingResponse> {
        val response = mutableListOf<HousingResponse>()

        result.housings.forEach { housing ->
            val housingId = housing.housingId
            val housingResponse = HousingResponse(
                housingId, housing.url,
                housing.name, housing.description,
                housing.participantsVotedIds.contains(userId),
                result.assignedHousingId == housingId,
                mapParticipants(housing.participantsVotedIds, result.participants)
            )
            response.add(housingResponse)
        }
        return response
    }
}


