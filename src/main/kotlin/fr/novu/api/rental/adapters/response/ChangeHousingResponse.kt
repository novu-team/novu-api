package fr.novu.api.rental.adapters.response


data class ChangeHousingResponse(val housingId: String, val url: String?, val description: String?)
