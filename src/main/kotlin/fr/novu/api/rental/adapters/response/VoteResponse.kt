package fr.novu.api.rental.adapters.response

data class VoteResponse(
    val participantId: String,
    val housingId: String
)
