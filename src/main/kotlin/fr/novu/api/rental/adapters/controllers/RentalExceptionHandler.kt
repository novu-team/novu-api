package fr.novu.api.rental.adapters.controllers

import fr.novu.api.rental.domain.exceptions.ConflictException
import fr.novu.api.rental.domain.exceptions.InvalidException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@RestControllerAdvice
class RentalExceptionHandler : ResponseEntityExceptionHandler() {


    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    fun handleInvalidRequest(e: InvalidException) = e.message

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.CONFLICT)
    fun handleConflictRequest(e: ConflictException) = e.message

}