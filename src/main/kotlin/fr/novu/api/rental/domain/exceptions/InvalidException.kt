package fr.novu.api.rental.domain.exceptions

abstract class InvalidException(message: String) : Exception(message)