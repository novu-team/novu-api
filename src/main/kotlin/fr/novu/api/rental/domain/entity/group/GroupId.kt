package fr.novu.api.rental.domain.entity.group

data class GroupId(val value: String)