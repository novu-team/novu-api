package fr.novu.api.rental.domain.entity.group

import fr.novu.api.rental.domain.entity.housing.Housing
import fr.novu.api.rental.domain.entity.vote.Vote
import fr.novu.api.rental.domain.entity.vote.exceptions.ParticipantAlreadyVotedException

class Participant(
    val id: ParticipantId,
    var name: String? = null,
    var firstname: String? = null,
    var trigram: String? = null,
    var color: String? = null
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Participant

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    fun canVote(housing: Housing, group: Group, votes: List<Vote>) {
        housing.isInGroup(group.groupId)
        group.containParticipant(id)
        group.checkIfAlreadyAssignedToHousing()
        housing.checkHousingAvailability()
        votes.stream().forEach { vote -> hasVoted(vote) }
    }

    private fun hasVoted(vote: Vote) {
        if (vote.getParticipantId() == id.value) {
            throw ParticipantAlreadyVotedException()
        }
    }

}