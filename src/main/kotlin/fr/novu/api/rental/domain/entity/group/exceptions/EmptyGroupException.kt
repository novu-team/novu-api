package fr.novu.api.rental.domain.entity.group.exceptions

class EmptyGroupException(message: String) : Exception(message)