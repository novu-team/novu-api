package fr.novu.api.rental.domain.repository

import fr.novu.api.rental.domain.entity.group.GroupId
import fr.novu.api.rental.domain.entity.group.ParticipantId
import fr.novu.api.rental.domain.entity.housing.HousingId
import fr.novu.api.rental.domain.entity.vote.Vote

interface VoteRepository {
    fun findHousingVotesInGroup(groupId: GroupId): List<Vote>
    fun findAllVoteByHousingId(housingId: HousingId): List<Vote>
    fun save(vote: Vote)
    fun findVote(participantId: ParticipantId, housingId: HousingId): Vote?
    fun delete(vote: Vote)
}