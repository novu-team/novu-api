package fr.novu.api.rental.domain.repository

import fr.novu.api.rental.domain.entity.group.Group
import fr.novu.api.rental.domain.entity.group.GroupId

interface GroupRepository {
    fun findGroupById(groupId: GroupId): Group?
    fun update(group: Group)
}