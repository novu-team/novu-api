package fr.novu.api.rental.domain.entity.group

data class ParticipantId(val value: String)