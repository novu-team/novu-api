package fr.novu.api.rental.domain.entity.housing.exceptions

import fr.novu.api.rental.domain.exceptions.InvalidException

class InvalidHousingNameException : InvalidException("Housing name should be provided")