package fr.novu.api.rental.domain.entity.housing.exceptions

import fr.novu.api.rental.domain.exceptions.InvalidException

class HousingNotInGroupException(message: String) : InvalidException(message)