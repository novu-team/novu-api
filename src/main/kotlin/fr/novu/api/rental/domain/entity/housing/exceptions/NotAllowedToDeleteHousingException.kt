package fr.novu.api.rental.domain.entity.housing.exceptions

import fr.novu.api.rental.domain.exceptions.InvalidException

class NotAllowedToDeleteHousingException : InvalidException("You are not allowed to delete this housing")