package fr.novu.api.rental.domain.entity.housing.exceptions

import fr.novu.api.rental.domain.exceptions.InvalidException

class ParticipantNotInGroupException(message: String) : InvalidException(message)