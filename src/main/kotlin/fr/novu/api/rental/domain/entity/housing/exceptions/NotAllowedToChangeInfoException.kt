package fr.novu.api.rental.domain.entity.housing.exceptions

import fr.novu.api.rental.domain.exceptions.InvalidException

class NotAllowedToChangeInfoException() : InvalidException("You are not allowed to change this housing")