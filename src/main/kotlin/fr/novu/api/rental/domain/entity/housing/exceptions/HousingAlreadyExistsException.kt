package fr.novu.api.rental.domain.entity.housing.exceptions

import fr.novu.api.rental.domain.exceptions.InvalidException

class HousingAlreadyExistsException : InvalidException("Housing with same creator and name already exists")