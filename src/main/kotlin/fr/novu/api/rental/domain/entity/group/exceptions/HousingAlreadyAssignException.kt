package fr.novu.api.rental.domain.entity.group.exceptions

class HousingAlreadyAssignException(message: String) : Exception(message)