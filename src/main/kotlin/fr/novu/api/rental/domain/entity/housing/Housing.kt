package fr.novu.api.rental.domain.entity.housing

import fr.novu.api.rental.domain.entity.group.GroupId
import fr.novu.api.rental.domain.entity.group.ParticipantId
import fr.novu.api.rental.domain.entity.housing.exceptions.HousingNotInGroupException
import fr.novu.api.rental.domain.entity.housing.exceptions.HousingUnavailableException
import fr.novu.api.rental.domain.entity.vote.Vote

class Housing(
    val id: HousingId,
    val groupId: GroupId,
    val creatorId: ParticipantId,
    val name: String,
    var url: String? = null,
    var description: String? = null,
    var available: Boolean = true,
    var votes: MutableList<Vote> = mutableListOf()
) {

    fun isInGroup(
        groupId: GroupId
    ) {
        if (groupId != this.groupId) {
            throw HousingNotInGroupException("Housing not in group")
        }
    }

    fun isAvailable(): Boolean {
        return available
    }

    fun checkHousingAvailability() {
        if (isAvailable().not()) {
            throw HousingUnavailableException()
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Housing

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}