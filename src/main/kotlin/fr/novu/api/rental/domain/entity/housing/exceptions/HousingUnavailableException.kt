package fr.novu.api.rental.domain.entity.housing.exceptions

class HousingUnavailableException() : Exception() {
}