package fr.novu.api.rental.domain.entity.vote

import fr.novu.api.rental.domain.entity.housing.HousingId

data class VoteId(val participantId: String, val housingId: HousingId)