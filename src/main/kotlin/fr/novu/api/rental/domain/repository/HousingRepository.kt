package fr.novu.api.rental.domain.repository

import fr.novu.api.rental.domain.entity.group.GroupId
import fr.novu.api.rental.domain.entity.group.ParticipantId
import fr.novu.api.rental.domain.entity.housing.Housing
import fr.novu.api.rental.domain.entity.housing.HousingId

interface HousingRepository {
    fun findByHousingIdAndGroupId(housingId: HousingId, groupId: GroupId): Housing?

    fun findHousing(housingId: HousingId): Housing?
    fun addHousing(housing: Housing)
    fun nextIdentity(): HousingId
    fun findByCreatorAndName(participantId: ParticipantId, name: String): Housing?
    fun updateHousing(housing: Housing)
    fun deleteHousing(housing: Housing)
}