package fr.novu.api.rental.domain.exceptions

abstract class ConflictException(message: String) : Exception(message)