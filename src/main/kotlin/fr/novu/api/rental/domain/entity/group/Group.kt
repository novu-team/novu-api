package fr.novu.api.rental.domain.entity.group

import fr.novu.api.rental.domain.entity.group.exceptions.EmptyGroupException
import fr.novu.api.rental.domain.entity.group.exceptions.HousingAlreadyAssignException
import fr.novu.api.rental.domain.entity.group.exceptions.NotEnoughVoteException
import fr.novu.api.rental.domain.entity.housing.Housing
import fr.novu.api.rental.domain.entity.housing.HousingId
import fr.novu.api.rental.domain.entity.housing.exceptions.ParticipantNotInGroupException
import fr.novu.api.rental.domain.entity.vote.Vote

class Group(
    val groupId: GroupId,
    var participants: MutableList<Participant>,
    var assignedHousingId: HousingId? = null,
    var housingProposals: MutableList<Housing> = mutableListOf(),
) {

    private fun isAssignToAHousing(): Boolean {
        return assignedHousingId != null
    }

    fun checkIfAlreadyAssignedToHousing() {
        if (isAssignToAHousing()) {
            throw HousingAlreadyAssignException("Housing already assigned for group ${groupId}")
        }
    }

    fun assignHousing(
        participantId: ParticipantId,
        votes: List<Vote>, housing: Housing
    ): Group {
        checkIfAlreadyAssignedToHousing()
        hasAtLeastOneParticipant()
        containParticipant(participantId)
        //hasEnoughParticipantVoted(votes)
        return Group(groupId, participants, housing.id)
    }

    fun containParticipant(participantId: ParticipantId) {
        val participantToFind = participants.any { it.id == participantId }
        if (participantToFind.not()) {
            throw ParticipantNotInGroupException("Participant not in group")
        }
    }

    private fun hasEnoughParticipantVoted(
        votes: List<Vote>
    ) {
        val numberParticipantsWhoVoted = votes.stream().map { it.getParticipantId() }.distinct().count()

        if (isLessThanFiftyPercent(numberParticipantsWhoVoted)) {
            throw NotEnoughVoteException("Under 50% of participants have voted")
        }
    }

    private fun isLessThanFiftyPercent(
        numberParticipantsWhoVoted: Long
    ) = numberParticipantsWhoVoted <= (participants.size / 2)

    private fun hasAtLeastOneParticipant() {
        if (participants.isEmpty()) {
            throw EmptyGroupException("Empty group")
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Group

        if (groupId != other.groupId) return false

        return true
    }

    override fun hashCode(): Int {
        return groupId.hashCode()
    }


}