package fr.novu.api.rental.domain.entity.housing

data class HousingId(val value: String)
