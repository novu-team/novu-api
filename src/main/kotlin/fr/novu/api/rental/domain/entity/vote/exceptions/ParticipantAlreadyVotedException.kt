package fr.novu.api.rental.domain.entity.vote.exceptions

import fr.novu.api.rental.domain.exceptions.InvalidException

class ParticipantAlreadyVotedException : InvalidException("") {
}