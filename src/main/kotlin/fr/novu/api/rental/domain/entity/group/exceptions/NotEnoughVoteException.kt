package fr.novu.api.rental.domain.entity.group.exceptions

import fr.novu.api.rental.domain.exceptions.InvalidException

class NotEnoughVoteException(message: String) : InvalidException(message)