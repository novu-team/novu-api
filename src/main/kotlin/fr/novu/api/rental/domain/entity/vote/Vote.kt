package fr.novu.api.rental.domain.entity.vote

import fr.novu.api.rental.domain.entity.group.Group
import fr.novu.api.rental.domain.entity.group.Participant
import fr.novu.api.rental.domain.entity.housing.Housing

class Vote(val id: VoteId) {
    companion object Factory {
        fun createVote(
            housing: Housing,
            participant: Participant,
            group: Group,
            votes: List<Vote>
        ): Vote {
            participant.canVote(housing, group, votes)
            return Vote(VoteId(participant.id.value, housing.id))
        }
    }

    fun getParticipantId(): String {
        return id.participantId
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Vote

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun toString(): String {
        return "Vote(id=$id)"
    }


}