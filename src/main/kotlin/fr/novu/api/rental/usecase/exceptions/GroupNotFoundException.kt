package fr.novu.api.rental.usecase.exceptions

import fr.novu.api.rental.domain.exceptions.InvalidException

class GroupNotFoundException(message: String) : InvalidException(message)