package fr.novu.api.rental.usecase.vote.exceptions

import fr.novu.api.rental.domain.exceptions.InvalidException

class HousingNotFoundException(message: String) : InvalidException(message)