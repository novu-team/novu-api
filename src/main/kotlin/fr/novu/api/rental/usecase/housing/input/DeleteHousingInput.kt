package fr.novu.api.rental.usecase.housing.input

data class DeleteHousingInput(
    val housingId: String?, val suppressorId: String?
)
