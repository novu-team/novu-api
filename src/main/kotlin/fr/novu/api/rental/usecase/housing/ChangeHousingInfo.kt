package fr.novu.api.rental.usecase.housing

import fr.novu.api.rental.domain.entity.housing.Housing
import fr.novu.api.rental.domain.entity.housing.HousingId
import fr.novu.api.rental.domain.entity.housing.exceptions.NotAllowedToChangeInfoException
import fr.novu.api.rental.domain.repository.HousingRepository
import fr.novu.api.rental.usecase.housing.input.ChangeHousingInfoInput
import fr.novu.api.rental.usecase.housing.output.ChangeHousingOutput
import fr.novu.api.rental.usecase.vote.exceptions.HousingNotFoundException
import org.springframework.stereotype.Service

@Service
class ChangeHousingInfo(
    private val housingRepository: HousingRepository
) {
    fun execute(input: ChangeHousingInfoInput): ChangeHousingOutput {

        val housing = housingRepository.findHousing(HousingId(input.housingId ?: ""))
            ?: throw HousingNotFoundException("Housing not found")

        if (housing.creatorId.value != input.changerId) {
            throw NotAllowedToChangeInfoException()
        }

        housing.url = input.url
        housing.description = input.description
        housingRepository.updateHousing(housing)

        return mapToOutput(housing)
    }

    private fun mapToOutput(newHousing: Housing) = ChangeHousingOutput(
        newHousing.id.value,
        newHousing.groupId.value,
        newHousing.creatorId.value,
        newHousing.name,
        newHousing.url,
        newHousing.description,
        newHousing.isAvailable()
    )
}