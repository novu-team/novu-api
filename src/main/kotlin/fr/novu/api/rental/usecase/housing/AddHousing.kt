package fr.novu.api.rental.usecase.housing

import fr.novu.api.rental.domain.entity.group.GroupId
import fr.novu.api.rental.domain.entity.group.ParticipantId
import fr.novu.api.rental.domain.entity.housing.Housing
import fr.novu.api.rental.domain.entity.housing.exceptions.HousingAlreadyExistsException
import fr.novu.api.rental.domain.entity.housing.exceptions.InvalidHousingNameException
import fr.novu.api.rental.domain.repository.GroupRepository
import fr.novu.api.rental.domain.repository.HousingRepository
import fr.novu.api.rental.usecase.exceptions.GroupNotFoundException
import fr.novu.api.rental.usecase.housing.input.AddHousingInput
import fr.novu.api.rental.usecase.housing.output.AddHousingOutput
import org.springframework.stereotype.Service

@Service
class AddHousing(
    private val groupRepository: GroupRepository,
    private val housingRepository: HousingRepository
) {
    fun execute(input: AddHousingInput): AddHousingOutput {
        if (input.name.isNullOrBlank()) throw InvalidHousingNameException()

        val group = groupRepository.findGroupById(GroupId(input.groupId ?: ""))
            ?: throw GroupNotFoundException("Group not found")

        val creatorId = ParticipantId(input.creatorId ?: "")
        group.containParticipant(creatorId)

        val alreadyExists = housingRepository.findByCreatorAndName(creatorId, input.name)
        if (alreadyExists != null) throw HousingAlreadyExistsException()

        val newId = housingRepository.nextIdentity()
        val newHousing = Housing(newId, group.groupId, creatorId, input.name, input.url, input.description)
        housingRepository.addHousing(newHousing)

        return mapToOutput(newHousing)
    }

    private fun mapToOutput(newHousing: Housing) = AddHousingOutput(
        newHousing.id.value,
        newHousing.groupId.value,
        newHousing.creatorId.value,
        newHousing.name,
        newHousing.url,
        newHousing.description,
        newHousing.isAvailable()
    )
}