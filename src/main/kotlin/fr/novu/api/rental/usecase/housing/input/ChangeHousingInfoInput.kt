package fr.novu.api.rental.usecase.housing.input

data class ChangeHousingInfoInput(
    val housingId: String?, val changerId: String?, val url: String?, val description: String?
)
