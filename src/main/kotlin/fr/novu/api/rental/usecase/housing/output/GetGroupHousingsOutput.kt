package fr.novu.api.rental.usecase.housing.output


data class GetGroupHousingsOutput(
    val groupId: String,
    val assignedHousingId: String?,
    val participants: List<ParticipantOutput>,
    val housings: List<HousingOutput>
)
