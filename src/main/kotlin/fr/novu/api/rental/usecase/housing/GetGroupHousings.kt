package fr.novu.api.rental.usecase.housing

import fr.novu.api.rental.domain.entity.group.GroupId
import fr.novu.api.rental.domain.repository.GroupRepository
import fr.novu.api.rental.usecase.exceptions.GroupNotFoundException
import fr.novu.api.rental.usecase.housing.output.GetGroupHousingsOutput
import fr.novu.api.rental.usecase.housing.output.HousingOutput
import fr.novu.api.rental.usecase.housing.output.ParticipantOutput
import org.springframework.stereotype.Service

@Service
class GetGroupHousings(
    private val groupRepository: GroupRepository
) {
    fun execute(groupId: String?): GetGroupHousingsOutput {

        val group = groupRepository.findGroupById(GroupId(groupId ?: ""))
            ?: throw GroupNotFoundException("Group not found")


        return GetGroupHousingsOutput(
            group.groupId.value,
            group.assignedHousingId?.value,
            group.participants.map { participant ->
                ParticipantOutput(
                    participant.id.value,
                    participant.name,
                    participant.firstname,
                    participant.trigram,
                    participant.color
                )
            },
            group.housingProposals.map { housing ->
                HousingOutput(housing.id.value, housing.creatorId.value, housing.name, housing.url,
                    housing.description, housing.available,
                    housing.votes.map { vote -> vote.getParticipantId() })
            }
        )
    }


}