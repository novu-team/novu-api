package fr.novu.api.rental.usecase.housing.output


data class HousingOutput(
    val housingId: String,
    val creatorId: String,
    val name: String,
    val url: String?,
    val description: String?,
    val available: Boolean,
    val participantsVotedIds: List<String>
)
