package fr.novu.api.rental.usecase.housing

import fr.novu.api.rental.domain.entity.housing.HousingId
import fr.novu.api.rental.domain.entity.housing.exceptions.NotAllowedToDeleteHousingException
import fr.novu.api.rental.domain.repository.HousingRepository
import fr.novu.api.rental.usecase.housing.input.DeleteHousingInput
import fr.novu.api.rental.usecase.vote.exceptions.HousingNotFoundException
import org.springframework.stereotype.Service

@Service
class DeleteHousing(private val housingRepository: HousingRepository) {
    fun execute(input: DeleteHousingInput) {

        val housing = housingRepository.findHousing(HousingId(input.housingId ?: ""))
            ?: throw HousingNotFoundException("Housing not found")

        if (input.suppressorId != housing.creatorId.value) {
            throw NotAllowedToDeleteHousingException()
        }

        housingRepository.deleteHousing(housing)
    }
}