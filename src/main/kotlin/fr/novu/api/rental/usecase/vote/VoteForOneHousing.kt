package fr.novu.api.rental.usecase.vote

import fr.novu.api.rental.domain.entity.group.Participant
import fr.novu.api.rental.domain.entity.group.ParticipantId
import fr.novu.api.rental.domain.entity.housing.HousingId
import fr.novu.api.rental.domain.entity.vote.Vote
import fr.novu.api.rental.domain.repository.GroupRepository
import fr.novu.api.rental.domain.repository.HousingRepository
import fr.novu.api.rental.domain.repository.VoteRepository
import fr.novu.api.rental.usecase.exceptions.GroupNotFoundException
import fr.novu.api.rental.usecase.vote.exceptions.HousingNotFoundException
import fr.novu.api.rental.usecase.vote.output.VoteOutput
import org.springframework.stereotype.Service

@Service
class VoteForOneHousing(
    private val groupRepository: GroupRepository,
    private val housingRepository: HousingRepository,
    private val voteRepository: VoteRepository
) {

    fun vote(participantId: String?, housingId: String?): VoteOutput {

        val nonNullHousingId = HousingId(housingId ?: "")
        val housing = housingRepository.findHousing(nonNullHousingId)
            ?: throw HousingNotFoundException("Housing not found")

        val group = groupRepository.findGroupById(housing.groupId) ?: throw GroupNotFoundException("Group not exist")

        val votes = voteRepository.findAllVoteByHousingId(nonNullHousingId)
        val newVote = Vote.createVote(
            housing,
            Participant(ParticipantId(participantId ?: "")),
            group,
            votes
        )
        voteRepository.save(newVote)
        return VoteOutput(newVote.getParticipantId(), newVote.id.housingId.value)
    }
}