package fr.novu.api.rental.usecase.housing.input

data class AddHousingInput(
    val creatorId: String?,
    val groupId: String?,
    val name: String?,
    val url: String?,
    val description: String?
)
