package fr.novu.api.rental.usecase.vote.output

data class VoteOutput(
    val participantId: String,
    val housingId: String
)