package fr.novu.api.rental.usecase.housing.output

data class AddHousingOutput(
    val housingId: String,
    val groupId: String,
    val creatorId: String,
    val name: String,
    val url: String?,
    val description: String?,
    val available: Boolean
)
