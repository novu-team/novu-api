package fr.novu.api.rental.usecase.housing

import fr.novu.api.rental.domain.entity.group.ParticipantId
import fr.novu.api.rental.domain.entity.housing.HousingId
import fr.novu.api.rental.domain.repository.GroupRepository
import fr.novu.api.rental.domain.repository.HousingRepository
import fr.novu.api.rental.domain.repository.VoteRepository
import fr.novu.api.rental.usecase.exceptions.GroupNotFoundException
import fr.novu.api.rental.usecase.housing.output.LockHousingChoiceOutput
import fr.novu.api.rental.usecase.vote.exceptions.HousingNotFoundException
import org.springframework.stereotype.Service

@Service
class LockHousingChoice(
    private val groupRepository: GroupRepository,
    private val housingRepository: HousingRepository,
    private val voteRepository: VoteRepository,
) {
    fun lock(
        housingId: String?,
        participantId: String?
    ): LockHousingChoiceOutput {

        val housing = housingRepository.findHousing(HousingId(housingId ?: ""))
            ?: throw HousingNotFoundException("Housing not found")

        val group = groupRepository.findGroupById(housing.groupId) ?: throw GroupNotFoundException("Group not exist")
        val votes = voteRepository.findHousingVotesInGroup(housing.groupId)
        val newGroup = group.assignHousing(ParticipantId(participantId ?: ""), votes, housing)

        groupRepository.update(newGroup)
        return LockHousingChoiceOutput(newGroup.groupId.value, housing.groupId.value)
    }
}