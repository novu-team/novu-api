package fr.novu.api.rental.usecase.housing.output

data class LockHousingChoiceOutput
    (val groupId: String, val assignedHousingId: String)
