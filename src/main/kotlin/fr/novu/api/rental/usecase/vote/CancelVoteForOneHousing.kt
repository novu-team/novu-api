package fr.novu.api.rental.usecase.vote

import fr.novu.api.rental.domain.entity.group.ParticipantId
import fr.novu.api.rental.domain.entity.housing.HousingId
import fr.novu.api.rental.domain.repository.VoteRepository
import fr.novu.api.rental.usecase.vote.exceptions.VoteNotFoundException
import org.springframework.stereotype.Service

@Service
class CancelVoteForOneHousing(private val voteRepository: VoteRepository) {

    fun execute(participantId: String?, housingId: String?) {
        val nonNullHousingId = HousingId(housingId ?: "")
        val nonNullParticipantId = ParticipantId(participantId ?: "")
        val vote = voteRepository.findVote(nonNullParticipantId, nonNullHousingId) ?: throw VoteNotFoundException()
        voteRepository.delete(vote)
    }
}