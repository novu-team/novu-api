package fr.novu.api.rental.usecase.housing.output

data class ParticipantOutput(
    val participantId: String, val name: String?, val firstname: String?, val trigram: String?, val color: String?
)
