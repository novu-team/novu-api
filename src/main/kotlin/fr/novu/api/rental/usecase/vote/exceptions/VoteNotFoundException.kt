package fr.novu.api.rental.usecase.vote.exceptions

import fr.novu.api.rental.domain.exceptions.InvalidException

class VoteNotFoundException : InvalidException("Vote not found")