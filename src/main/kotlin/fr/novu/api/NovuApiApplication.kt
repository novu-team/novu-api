package fr.novu.api

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class NovuApiApplication

fun main(args: Array<String>) {
    runApplication<NovuApiApplication>(*args)
}
