package fr.novu.api.authentication.domain.exception

class MissingPropertyException(message: String) : InvalidException(message)