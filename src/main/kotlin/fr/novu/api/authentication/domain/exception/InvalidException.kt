package fr.novu.api.authentication.domain.exception

abstract class InvalidException(message: String) : Exception(message)