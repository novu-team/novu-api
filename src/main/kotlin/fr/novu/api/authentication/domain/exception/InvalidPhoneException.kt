package fr.novu.api.authentication.domain.exception

class InvalidPhoneException(message: String) : InvalidException(message)