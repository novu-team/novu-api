package fr.novu.api.authentication.domain.entity

data class Tag(
    val name: String,
    val type: String
)