package fr.novu.api.authentication.domain.repository


import fr.novu.api.authentication.domain.entity.User
import fr.novu.api.authentication.domain.entity.UserId

interface UserRepository {
    fun findUserByEmail(email: String): User?
    fun findUserByPhone(phoneNumber: String): User?
    fun save(user: User): User?
    fun nextIdentity(): UserId

    fun count(): Number
}