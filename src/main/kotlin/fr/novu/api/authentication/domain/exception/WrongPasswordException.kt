package fr.novu.api.authentication.domain.exception

class WrongPasswordException(message: String) : InvalidException(message)