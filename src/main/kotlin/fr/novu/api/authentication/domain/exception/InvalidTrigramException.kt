package fr.novu.api.authentication.domain.exception

class InvalidTrigramException(message: String) : InvalidException(message)