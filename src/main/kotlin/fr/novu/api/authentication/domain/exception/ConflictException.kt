package fr.novu.api.authentication.domain.exception

abstract class ConflictException(message: String) : Exception(message)