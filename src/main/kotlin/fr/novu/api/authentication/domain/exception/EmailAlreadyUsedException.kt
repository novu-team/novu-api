package fr.novu.api.authentication.domain.exception

class EmailAlreadyUsedException(message: String) : ConflictException(message)