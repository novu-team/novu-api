package fr.novu.api.authentication.domain.exception

class WrongEmailException(message: String) : InvalidException(message)