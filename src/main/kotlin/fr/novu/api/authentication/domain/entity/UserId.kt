package fr.novu.api.authentication.domain.entity


data class UserId(val uuid: String)