package fr.novu.api.authentication.domain.exception

class InvalidBirthdayException(message: String) : InvalidException(message)