package fr.novu.api.authentication.domain.exception

class PhoneAlreadyUsedException(message: String) : ConflictException(message)