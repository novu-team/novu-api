package fr.novu.api.authentication.domain.entity

import fr.novu.api.authentication.domain.exception.*
import java.time.LocalDate
import java.util.regex.Pattern

class User(
    val id: UserId,
    var email: String,
    var password: String? = null,
    var firstname: String,
    var name: String,
    var trigram: String? = null,
    var trigramColor: String? = null,
    var birthday: LocalDate? = null,
    var phoneNumber: String? = null,
    var sex: Boolean? = null,
    var isAdmin: Boolean = false,
    var tags: List<Tag>? = emptyList()
) {

    fun validateData() {
        validateName()
        validateFirstname()
        validateEmail()
        validatePassword()
        validateTrigram()
        validateTrigramColor()
        validateBirthday()
        validatePhone()
    }

    private fun validateEmail() {
        val regexPattern = "^(.+)@(\\S+)$"
        if (!Pattern.compile(regexPattern).matcher(email).matches()) {
            throw WrongEmailException("Wrong email")
        }
    }

    private fun validatePassword() {
        val nonNullPassword = password ?: ""
        if (nonNullPassword.trim().length < 8) {
            throw WrongPasswordException("Password should contain at least 8 characters")
        }

        if (nonNullPassword.firstOrNull { letter -> letter.isUpperCase() } == null) {
            throw WrongPasswordException("Password should contain at least 8 characters")
        }
    }

    private fun validateName() {
        if (name.isBlank()) {
            throw MissingPropertyException("Missing name")
        }
    }

    private fun validateFirstname() {
        if (firstname.isBlank()) {
            throw MissingPropertyException("Missing firstname")
        }
    }

    private fun validatePhone() {
        val nonNullPhoneNumber = phoneNumber ?: ""

        if (nonNullPhoneNumber.trim().length != 12) {
            throw InvalidPhoneException("Invalid phone")
        }
    }

    private fun validateBirthday() {
        if (birthday?.isAfter(LocalDate.now()) == true) {
            throw InvalidBirthdayException("Birthday should be in the past")
        }
    }

    private fun validateTrigramColor() {
        if (trigramColor?.isBlank() == true) {
            throw MissingPropertyException("Missing trigram Color")
        }
    }

    private fun validateTrigram() {
        if ((trigram?.trim()?.length ?: "") != 3) {
            throw InvalidTrigramException("Invalid trigram")
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as User

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }


}
