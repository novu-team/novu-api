package fr.novu.api.authentication.usecase.ports

interface PasswordEncrypter {

    fun encryptPassword(password: String): String
}