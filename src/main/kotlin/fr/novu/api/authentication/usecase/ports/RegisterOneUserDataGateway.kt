package fr.novu.api.authentication.usecase.ports

import fr.novu.api.authentication.domain.entity.User
import fr.novu.api.authentication.domain.entity.UserId

interface RegisterOneUserDataGateway {
    fun nextIdentity(): UserId
    fun save(user: User)
    fun findUserByEmail(email: String): User?
    fun findUserByPhone(phoneNumber: String): User?
}