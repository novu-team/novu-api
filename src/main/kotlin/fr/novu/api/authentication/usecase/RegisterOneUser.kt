package fr.novu.api.authentication.usecase

import fr.novu.api.authentication.domain.entity.User
import fr.novu.api.authentication.domain.exception.EmailAlreadyUsedException
import fr.novu.api.authentication.domain.exception.PhoneAlreadyUsedException
import fr.novu.api.authentication.usecase.ports.PasswordEncrypter
import fr.novu.api.authentication.usecase.ports.RegisterOneUserDataGateway
import fr.novu.api.authentication.usecase.ports.RegisterOneUserInput
import fr.novu.api.authentication.usecase.request.RegisterOneUserRequest
import fr.novu.api.authentication.usecase.response.RegisterOneUserResponse
import org.springframework.stereotype.Service

@Service
class RegisterOneUser(
    private val dataGateway: RegisterOneUserDataGateway,
    private val passwordEncrypter: PasswordEncrypter
) : RegisterOneUserInput {

    override fun execute(registerOneUserRequest: RegisterOneUserRequest): RegisterOneUserResponse {
        val nextUserId = dataGateway.nextIdentity()
        val user = User(
            id = nextUserId,
            email = registerOneUserRequest.email,
            password = registerOneUserRequest.password,
            firstname = registerOneUserRequest.firstname,
            name = registerOneUserRequest.name,
            trigram = registerOneUserRequest.trigram,
            trigramColor = registerOneUserRequest.trigramColor,
            birthday = registerOneUserRequest.birthday,
            phoneNumber = registerOneUserRequest.phoneNumber,
            sex = registerOneUserRequest.sex,
            //tags = registerOneUserRequest.tagIds
        )
        user.validateData()

        checkIfEmailIsAlreadyUsed(user)
        checkIfPhoneIsAlreadyUsed(user)
        user.password = user.password?.let { passwordEncrypter.encryptPassword(it) }
        dataGateway.save(user)
        return RegisterOneUserResponse(
            user.id, user.email, user.isAdmin
        )
    }

    private fun checkIfPhoneIsAlreadyUsed(user: User) {
        val userWithSamePhone = user.phoneNumber?.let { dataGateway.findUserByPhone(it) }
        if (userWithSamePhone != null) {
            throw PhoneAlreadyUsedException("Phone already used")
        }
    }

    private fun checkIfEmailIsAlreadyUsed(user: User) {
        val userWithSameEmail = dataGateway.findUserByEmail(user.email)
        if (userWithSameEmail != null) {
            throw EmailAlreadyUsedException("Email already used")
        }
    }


}