package fr.novu.api.authentication.usecase.request

import java.time.LocalDate

data class RegisterOneUserRequest(
    val email: String,
    var password: String,
    val firstname: String,
    val name: String,
    val trigram: String?,
    val trigramColor: String?,
    val birthday: LocalDate?,
    val phoneNumber: String?,
    val sex: Boolean?,
    val tagIds: List<Int>?
)
