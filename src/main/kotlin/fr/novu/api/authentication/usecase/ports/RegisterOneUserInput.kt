package fr.novu.api.authentication.usecase.ports

import fr.novu.api.authentication.usecase.request.RegisterOneUserRequest
import fr.novu.api.authentication.usecase.response.RegisterOneUserResponse

interface RegisterOneUserInput {
    fun execute(registerOneUserRequest: RegisterOneUserRequest): RegisterOneUserResponse
}