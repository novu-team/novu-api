package fr.novu.api.authentication.usecase.response

import fr.novu.api.authentication.domain.entity.UserId

data class RegisterOneUserResponse(
    val userId: UserId,
    val email: String,
    val isAdmin: Boolean
)