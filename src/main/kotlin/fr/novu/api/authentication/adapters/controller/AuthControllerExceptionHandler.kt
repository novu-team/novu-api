package fr.novu.api.authentication.adapters.controller

import fr.novu.api.authentication.domain.exception.ConflictException
import fr.novu.api.authentication.domain.exception.InvalidException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@RestControllerAdvice
class AuthControllerExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.CONFLICT)
    fun handleEmailAlreadyUseException(e: ConflictException) = e.message

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    fun handleInvalidRequest(e: InvalidException) = e.message

}