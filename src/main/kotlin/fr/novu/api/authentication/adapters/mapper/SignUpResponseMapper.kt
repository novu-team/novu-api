package fr.novu.api.authentication.adapters.mapper

import fr.novu.api.authentication.usecase.response.RegisterOneUserResponse
import org.springframework.stereotype.Component

@Component
class SignUpResponseMapper {

    fun mapToSignUpResponse(request: RegisterOneUserResponse): fr.novu.api.authentication.adapters.controller.response.SignUpResponse {
        return fr.novu.api.authentication.adapters.controller.response.SignUpResponse(
            request.userId.uuid,
            request.email,
            boolToInt(request.isAdmin)
        )
    }

    private fun boolToInt(b: Boolean): Int {
        return if (b) {
            1
        } else {
            0
        }
    }

}