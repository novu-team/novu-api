package fr.novu.api.authentication.adapters.mapper

import fr.novu.api.authentication.usecase.request.RegisterOneUserRequest
import org.springframework.stereotype.Component
import java.time.LocalDate

@Component
class SignUpRequestMapper {

    fun mapToRegisterRequest(request: fr.novu.api.authentication.adapters.controller.request.SignUpRequest): RegisterOneUserRequest {
        return RegisterOneUserRequest(
            request.email,
            request.password,
            request.firstname,
            request.name,
            request.trigram,
            request.trigramColor,
            LocalDate.parse(request.birthday),
            request.phoneNumber,
            request.sex?.let { mapSex(it) },
            request.tagIds
        )
    }

    private fun mapSex(sex: String): Boolean = sex != "MALE"

}