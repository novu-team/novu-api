package fr.novu.api.authentication.adapters.controller.response

data class SignUpResponse(
    val userId: String,
    val email: String,
    val isAdmin: Int
)
