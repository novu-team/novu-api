package fr.novu.api.authentication.adapters.controller

import fr.novu.api.authentication.adapters.controller.request.SignUpRequest
import fr.novu.api.authentication.adapters.controller.response.SignUpResponse
import fr.novu.api.authentication.adapters.mapper.SignUpRequestMapper
import fr.novu.api.authentication.adapters.mapper.SignUpResponseMapper
import fr.novu.api.authentication.usecase.ports.RegisterOneUserInput
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI


@Controller
@RequestMapping("/api")
class UserController(
    private val registerOneUserInput: RegisterOneUserInput,
    private val signUpRequestMapper: SignUpRequestMapper,
    private val signUpResponseMapper: SignUpResponseMapper
) {

    @PostMapping("/users")
    fun signUp(@RequestBody request: SignUpRequest): ResponseEntity<SignUpResponse> {
        val register = signUpRequestMapper.mapToRegisterRequest(request)
        val result = registerOneUserInput.execute(register)
        val path = "/api/users/${result.userId}"
        val uri = URI.create(
            ServletUriComponentsBuilder.fromCurrentContextPath().path(path).toUriString()
        )
        val response = signUpResponseMapper.mapToSignUpResponse(result)
        return ResponseEntity.created(uri).body(response)
    }
}