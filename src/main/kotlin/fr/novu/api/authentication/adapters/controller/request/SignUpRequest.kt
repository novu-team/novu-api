package fr.novu.api.authentication.adapters.controller.request

data class SignUpRequest(
    val email: String, val password: String,
    val firstname: String, val name: String,
    val trigram: String?, val trigramColor: String?,
    val birthday: String?, val phoneNumber: String?,
    val sex: String?, val tagIds: List<Int>?
)
