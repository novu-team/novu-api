package fr.novu.api.authentication.infra.repository

import fr.novu.api.authentication.domain.entity.User
import fr.novu.api.authentication.domain.entity.UserId
import fr.novu.api.authentication.domain.repository.UserRepository
import fr.novu.api.authentication.usecase.ports.RegisterOneUserDataGateway
import org.springframework.stereotype.Component

@Component
class RegisterOneUserData(
    private val userRepository: UserRepository
) : RegisterOneUserDataGateway {
    override fun nextIdentity(): UserId {
        return userRepository.nextIdentity()
    }

    override fun save(user: User) {
        userRepository.save(user)
    }

    override fun findUserByEmail(email: String): User? {
        return userRepository.findUserByEmail(email)
    }

    override fun findUserByPhone(phoneNumber: String): User? {
        return userRepository.findUserByPhone(phoneNumber)
    }
}