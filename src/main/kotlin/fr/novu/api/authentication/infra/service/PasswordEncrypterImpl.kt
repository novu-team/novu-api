package fr.novu.api.authentication.infra.service

import fr.novu.api.authentication.usecase.ports.PasswordEncrypter
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component

@Component
class PasswordEncrypterImpl : PasswordEncrypter {
    override fun encryptPassword(password: String): String {
        val encoder = BCryptPasswordEncoder()
        return encoder.encode(password)
    }
}