package fr.novu.api.authentication.infra.db.mapper

import fr.novu.api.authentication.domain.entity.User
import fr.novu.api.authentication.domain.entity.UserId
import fr.novu.api.shared.infrastructure.database.model.UserMySQL
import org.springframework.data.domain.Page
import org.springframework.stereotype.Component

@Component
class UserMapper {

    fun mapToDomainEntity(user: UserMySQL): User? {
        return User(
            UserId(user.id),
            user.email,
            user.password,
            user.firstName,
            user.name,
            user.trigram,
            user.colour,
            user.birthday,
            user.phoneNumber,
            user.sex,
            user.isAdmin
        )
    }

    fun mapToSql(user: User): UserMySQL {
        return UserMySQL(
            user.id.uuid,
            user.name,
            user.firstname,
            user.email,
            user.password ?: "",
            user.isAdmin,
            user.trigramColor,
            user.birthday,
            user.sex,
            user.trigram,
            user.phoneNumber
        )
    }

    fun mapToUsersEntity(usersMySQL: Page<UserMySQL>): MutableList<User> {
        val users =  mutableListOf<User>()
        usersMySQL.forEach { userMySQL ->
            val user = mapToDomainEntity(userMySQL)

            if (user != null) {
                users.add(user)
            }
        }

        return users
    }
}