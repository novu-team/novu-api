package fr.novu.api.authentication.infra.repository

import fr.novu.api.authentication.domain.entity.User
import fr.novu.api.authentication.domain.entity.UserId
import fr.novu.api.authentication.domain.repository.UserRepository
import fr.novu.api.authentication.infra.db.mapper.UserMapper
import fr.novu.api.shared.infrastructure.database.repository.UserJpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class UserRepoImpl(
    private val userJpaRepository: UserJpaRepository,
    private val userMapper: UserMapper
) : UserRepository {
    override fun findUserByEmail(email: String): User? {
        val user = userJpaRepository.findByEmail(email) ?: return null
        return userMapper.mapToDomainEntity(user)
    }

    override fun findUserByPhone(phoneNumber: String): User? {
        val user = userJpaRepository.findByPhoneNumber(phoneNumber) ?: return null
        return userMapper.mapToDomainEntity(user)
    }

    override fun save(user: User): User? {
        val userEntity = userMapper.mapToSql(user)
        val newUser = userJpaRepository.save(userEntity)
        return userMapper.mapToDomainEntity(newUser)
    }

    override fun nextIdentity(): UserId {
        return UserId(UUID.randomUUID().toString())
    }

    override fun count(): Number {
        return userJpaRepository.count()
    }
}