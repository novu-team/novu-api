package fr.novu.api.availability.domain.repository

import fr.novu.api.availability.domain.entity.Group
import fr.novu.api.availability.domain.entity.GroupId

interface GroupRepository {
    fun updateGroup(group: Group)
    fun findGroup(groupId: GroupId): Group?
}