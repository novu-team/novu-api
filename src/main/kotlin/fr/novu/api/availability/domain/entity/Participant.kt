package fr.novu.api.availability.domain.entity

import fr.novu.api.availability.usecase.exceptions.AvailabilityAlreadyExistsException
import fr.novu.api.availability.usecase.exceptions.AvailabilityNotFoundException
import java.time.LocalDate

class Participant(
    val id: ParticipantId,
    var isAdmin: Boolean,
    var availabilities: MutableList<Availability> = mutableListOf(),
    var name: String? = null,
    var firstname: String? = null,
    var trigram: String? = null,
    var color: String? = null,

    ) {

    fun isNotAnAdmin(): Boolean = !isAdmin

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Participant

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    override fun toString(): String {
        return "Member(id=$id, isAdmin=$isAdmin)"
    }

    fun addAvailability(startDate: LocalDate, endDate: LocalDate) {
        if (availabilities
                .find { availability ->
                    availability.startDate == startDate
                            && availability.endDate == endDate
                } != null
        ) {
            throw AvailabilityAlreadyExistsException()
        }
        availabilities.add(Availability(id, startDate, endDate))
    }

    fun hasIndicateAvailability(): Boolean =
        availabilities.size > 0

    fun removeAvailability(availability: Availability) =
        availabilities.remove(availability)

    fun findAvailability(startDate: LocalDate, endDate: LocalDate) =
        availabilities
            .find { availability ->
                availability.startDate == startDate
                        && availability.endDate == endDate
            } ?: throw AvailabilityNotFoundException()


}