package fr.novu.api.availability.domain.exceptions

abstract class InvalidException(message: String) : Exception(message)