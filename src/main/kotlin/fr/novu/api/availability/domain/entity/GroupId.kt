package fr.novu.api.availability.domain.entity

data class GroupId(val value: String)
