package fr.novu.api.availability.domain.entity

import java.time.LocalDate

class Group(
    val groupId: GroupId,
    var name: String,
    var participants: MutableList<Participant>,
    var startDate: LocalDate? = null,
    var endDate: LocalDate? = null
) {

    fun setDates(startDate: LocalDate, endDate: LocalDate) {
        this.startDate = startDate
        this.endDate = endDate
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Group

        if (groupId != other.groupId) return false

        return true
    }

    override fun hashCode(): Int {
        return groupId.hashCode()
    }

    override fun toString(): String {
        return "Group(groupId=$groupId, participants=$participants)"
    }


}