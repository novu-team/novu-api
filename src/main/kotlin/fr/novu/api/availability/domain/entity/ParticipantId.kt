package fr.novu.api.availability.domain.entity

data class ParticipantId(val userId: UserId, val groupId: GroupId)
