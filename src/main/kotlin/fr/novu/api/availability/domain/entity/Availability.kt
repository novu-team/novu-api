package fr.novu.api.availability.domain.entity

import java.time.LocalDate

data class Availability(
    val participantId: ParticipantId, val startDate: LocalDate, val endDate: LocalDate
)