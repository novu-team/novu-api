package fr.novu.api.availability.domain.repository

import fr.novu.api.availability.domain.entity.GroupId
import fr.novu.api.availability.domain.entity.Participant
import fr.novu.api.availability.domain.entity.UserId

interface ParticipantRepository {
    fun updateParticipant(participant: Participant)
    fun findParticipant(userId: UserId, groupId: GroupId): Participant?
    fun findParticipantsOfGroup(groupId: GroupId): List<Participant>
}