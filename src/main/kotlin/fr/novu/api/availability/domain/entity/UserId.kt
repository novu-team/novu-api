package fr.novu.api.availability.domain.entity

data class UserId(val value: String)
