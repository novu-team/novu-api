package fr.novu.api.availability.domain.exceptions

abstract class ConflictException(message: String) : Exception(message)