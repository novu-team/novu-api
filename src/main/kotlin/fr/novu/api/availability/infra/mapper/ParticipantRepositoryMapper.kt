package fr.novu.api.availability.infra.mapper

import fr.novu.api.availability.domain.entity.*
import fr.novu.api.shared.infrastructure.database.model.AvailabilityMySQL
import fr.novu.api.shared.infrastructure.database.model.ParticipantMySQL
import fr.novu.api.shared.infrastructure.database.model.UserMySQL
import org.springframework.stereotype.Component

@Component
class ParticipantRepositoryMapper {

    fun mapToParticipantEntity(
        participant: ParticipantMySQL,
        user: UserMySQL,
        availabilities: List<AvailabilityMySQL>
    ): Participant {
        val participantId = ParticipantId(
            UserId(participant.participantId.userId),
            GroupId(participant.participantId.groupId)
        )
        return Participant(
            participantId, participant.isAdmin,
            availabilities.map { a ->
                Availability(
                    participantId,
                    a.startDate,
                    a.endDate
                )
            }.toMutableList(),
            user.name,
            user.firstName,
            user.trigram,
            user.colour
        )
    }

}