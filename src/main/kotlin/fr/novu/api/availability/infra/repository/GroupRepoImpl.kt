package fr.novu.api.availability.infra.repository

import fr.novu.api.availability.domain.entity.Group
import fr.novu.api.availability.domain.entity.GroupId
import fr.novu.api.availability.domain.repository.GroupRepository
import fr.novu.api.availability.infra.mapper.AvailabilityGroupRepositoryMapper
import fr.novu.api.shared.infrastructure.database.repository.GroupJpaRepository
import fr.novu.api.shared.infrastructure.database.repository.ParticipantJpaRepository
import org.springframework.stereotype.Repository

@Repository
class GroupRepoImpl(
    private val groupJpaRepository: GroupJpaRepository,
    private val participantJpaRepository: ParticipantJpaRepository,
    private val mapper: AvailabilityGroupRepositoryMapper
) : GroupRepository {
    override fun updateGroup(group: Group) {
        val groupMySQL = mapper.mapToGroupMySQL(group)
        groupJpaRepository.save(groupMySQL)
    }

    override fun findGroup(groupId: GroupId): Group? {
        val group = groupJpaRepository.findById(groupId.value)
        if (group.isPresent) {
            val participants = participantJpaRepository.findAllByGroup(groupId.value)
            return mapper.mapToGroupEntity(group.get(), participants)
        }
        return null
    }
}