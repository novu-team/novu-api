package fr.novu.api.availability.infra.repository

import fr.novu.api.availability.domain.entity.GroupId
import fr.novu.api.availability.domain.entity.Participant
import fr.novu.api.availability.domain.entity.UserId
import fr.novu.api.availability.domain.repository.ParticipantRepository
import fr.novu.api.availability.infra.mapper.ParticipantRepositoryMapper
import fr.novu.api.shared.infrastructure.database.model.AvailabilityMySQL
import fr.novu.api.shared.infrastructure.database.model.ParticipantId
import fr.novu.api.shared.infrastructure.database.model.ParticipantMySQL
import fr.novu.api.shared.infrastructure.database.model.UserMySQL
import fr.novu.api.shared.infrastructure.database.repository.AvailabilityJpaRepository
import fr.novu.api.shared.infrastructure.database.repository.ParticipantJpaRepository
import fr.novu.api.shared.infrastructure.database.repository.UserJpaRepository
import org.springframework.stereotype.Repository
import java.util.*
import javax.transaction.Transactional

@Repository
class ParticipantRepoImpl(
    private val participantJpaRepository: ParticipantJpaRepository,
    private val availabilityJpaRepository: AvailabilityJpaRepository,
    private val userJpaRepository: UserJpaRepository,
    private val mapper: ParticipantRepositoryMapper
) : ParticipantRepository {

    @Transactional
    override fun updateParticipant(participant: Participant) {
        val userId = participant.id.userId.value
        val groupId = participant.id.groupId.value

        val participantMySQL = participantJpaRepository.findById(
            ParticipantId(
                userId, groupId
            )
        )

        participantMySQL.ifPresent { p ->
            p.isAdmin = participant.isAdmin
            participantJpaRepository.save(p)
            updateAvailabilities(userId, groupId, participant)
        }
    }


    private fun updateAvailabilities(
        userId: String,
        groupId: String,
        participant: Participant
    ) {
        availabilityJpaRepository.deleteAllByParticipant(userId, groupId)
        val availabilitiesMySQL = participant.availabilities.map { a ->
            AvailabilityMySQL(
                userId,
                groupId,
                a.startDate, a.endDate
            )
        }

        availabilityJpaRepository.saveAll(availabilitiesMySQL)
    }

    override fun findParticipant(userId: UserId, groupId: GroupId): Participant? {
        val userMySQL = userJpaRepository.findById(userId.value)
        val participantMySQL = participantJpaRepository.findById(ParticipantId(userId.value, groupId.value))
        val availabilityMySQL =
            availabilityJpaRepository.findAllByParticipant(userId.value, groupId.value)
        if (participantMySQL.isPresent && userMySQL.isPresent) {
            return mapper.mapToParticipantEntity(participantMySQL.get(), userMySQL.get(), availabilityMySQL)
        }
        return null

    }

    override fun findParticipantsOfGroup(groupId: GroupId): List<Participant> {
        val participantsMySQL = participantJpaRepository.findAllByGroup(groupId.value)
        val participants = mutableListOf<Participant>()
        participantsMySQL.forEach { p ->
            val user = userJpaRepository.findById(p.participantId.userId)
            if (user.isPresent) {
                val participant = mapToParticipant(p, user)
                participants.add(participant)
            }
        }

        return participants
    }

    private fun mapToParticipant(
        p: ParticipantMySQL,
        user: Optional<UserMySQL>
    ) = mapper.mapToParticipantEntity(
        p,
        user.get(),
        availabilityJpaRepository.findAllByParticipant(
            p.participantId.userId,
            p.participantId.groupId
        )
    )

}