package fr.novu.api.availability.infra.mapper

import fr.novu.api.availability.domain.entity.*
import fr.novu.api.shared.infrastructure.database.model.GroupMySQL
import fr.novu.api.shared.infrastructure.database.model.ParticipantMySQL
import org.springframework.stereotype.Component

@Component
class AvailabilityGroupRepositoryMapper {

    fun mapToGroupMySQL(group: Group): GroupMySQL =
        GroupMySQL(group.groupId.value, group.name, startDate = group.startDate, endDate = group.endDate)


    fun mapToGroupEntity(groupMySQL: GroupMySQL, participantsMySQL: List<ParticipantMySQL>): Group {
        return Group(
            GroupId(groupMySQL.id),
            groupMySQL.name,
            participantsMySQL.map { participantMySQL ->
                Participant(
                    ParticipantId(
                        UserId(participantMySQL.participantId.userId),
                        GroupId(participantMySQL.participantId.groupId)
                    ),
                    participantMySQL.isAdmin,

                    )
            }.toMutableList(), groupMySQL.startDate, groupMySQL.endDate
        )
    }
}