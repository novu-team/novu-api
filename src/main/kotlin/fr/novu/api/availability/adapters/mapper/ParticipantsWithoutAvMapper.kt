package fr.novu.api.availability.adapters.mapper

import fr.novu.api.availability.adapters.response.ParticipantWithoutVoteResponse
import fr.novu.api.availability.adapters.response.ParticipantsWithoutAvailabilityResponse
import fr.novu.api.availability.usecase.output.ParticipantsAvailabilitiesOutput
import org.springframework.stereotype.Component

@Component
class ParticipantsWithoutAvMapper {
    fun mapToResponse(output: ParticipantsAvailabilitiesOutput): ParticipantsWithoutAvailabilityResponse {

        return ParticipantsWithoutAvailabilityResponse(
            output.participants
                .filter { participant -> participant.availabilities.isEmpty() }
                .map { participant ->
                    ParticipantWithoutVoteResponse(
                        participant.name,
                        participant.firstname,
                        participant.trigram,
                        participant.color
                    )
                }
        )
    }

}