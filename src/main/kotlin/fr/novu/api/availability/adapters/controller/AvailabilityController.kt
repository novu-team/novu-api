package fr.novu.api.availability.adapters.controller

import fr.novu.api.availability.adapters.mapper.IndicateAvailabilityMapper
import fr.novu.api.availability.adapters.mapper.LockGroupDatesMapper
import fr.novu.api.availability.adapters.mapper.ParticipantsWithoutAvMapper
import fr.novu.api.availability.adapters.mapper.RemoveAvailabilityMapper
import fr.novu.api.availability.adapters.request.IndicateAvailabilityRequest
import fr.novu.api.availability.adapters.request.LockGroupDatesRequest
import fr.novu.api.availability.adapters.request.RemoveAvailabilityRequest
import fr.novu.api.availability.adapters.response.IndicateAvailabilityResponse
import fr.novu.api.availability.adapters.response.LockGroupDatesResponse
import fr.novu.api.availability.adapters.response.ParticipantsWithoutAvailabilityResponse
import fr.novu.api.availability.usecase.GetParticipantsAvailabilities
import fr.novu.api.availability.usecase.IndicateAvailability
import fr.novu.api.availability.usecase.LockGroupDates
import fr.novu.api.availability.usecase.RemoveAvailability
import fr.novu.api.shared.application.security.AuthUser
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/api")
class AvailabilityController(
    private val indicateAvailability: IndicateAvailability,
    private val lockGroupDates: LockGroupDates,
    private val removeAvailability: RemoveAvailability,
    private val indicateAvailabilityMapper: IndicateAvailabilityMapper,
    private val getParticipantsAvailabilities: GetParticipantsAvailabilities,
    private val participantsWithoutAvMapper: ParticipantsWithoutAvMapper,
    private val lockGroupDatesMapper: LockGroupDatesMapper,
    private val removeAvailabilityMapper: RemoveAvailabilityMapper
) {

    @PostMapping("/availability")
    fun createGroup(
        @RequestBody request: IndicateAvailabilityRequest,
        authentication: Authentication
    ): ResponseEntity<IndicateAvailabilityResponse> {
        val authUser: AuthUser = authentication.principal as AuthUser
        val input = indicateAvailabilityMapper.mapToInput(authUser, request)
        val result = indicateAvailability.execute(input)
        val response = indicateAvailabilityMapper.mapToResponse(result)
        return ResponseEntity.ok().body(response)
    }

    @DeleteMapping("/availability")
    fun createGroup(
        @RequestBody request: RemoveAvailabilityRequest,
        authentication: Authentication
    ): ResponseEntity<IndicateAvailabilityResponse> {
        val authUser: AuthUser = authentication.principal as AuthUser
        val input = removeAvailabilityMapper.mapToInput(authUser, request)
        removeAvailability.execute(input)
        return ResponseEntity.ok().build()
    }

    @PutMapping("/groups/{groupId}/lock-dates")
    fun lockGroupDates(
        @RequestBody request: LockGroupDatesRequest,
        @PathVariable groupId: String?,
        authentication: Authentication
    ): ResponseEntity<LockGroupDatesResponse> {
        val authUser: AuthUser = authentication.principal as AuthUser
        val input = lockGroupDatesMapper.mapToInput(authUser, groupId, request)
        val result = lockGroupDates.execute(input)
        val response = lockGroupDatesMapper.mapToResponse(result)
        return ResponseEntity.ok().body(response)
    }

    @GetMapping("/groups/{groupId}/participants-not-voted")
    fun retrieveParticipantsThatHaveNotVoted(
        @PathVariable groupId: String?
    ): ResponseEntity<ParticipantsWithoutAvailabilityResponse> {
        val result = getParticipantsAvailabilities.execute(groupId)
        val response = participantsWithoutAvMapper.mapToResponse(result)
        return ResponseEntity.ok().body(response)
    }


}