package fr.novu.api.availability.adapters.mapper

import fr.novu.api.availability.adapters.request.IndicateAvailabilityRequest
import fr.novu.api.availability.adapters.response.AvailabilityResponse
import fr.novu.api.availability.adapters.response.IndicateAvailabilityResponse
import fr.novu.api.availability.usecase.input.IndicateAvailabilityInput
import fr.novu.api.availability.usecase.output.IndicateAvailabilityOutput
import fr.novu.api.shared.application.security.AuthUser
import org.springframework.stereotype.Component

@Component
class IndicateAvailabilityMapper {

    fun mapToResponse(result: IndicateAvailabilityOutput) =
        IndicateAvailabilityResponse(
            result.userId, result.groupId, result.isAdmin,
            result.availabilities.map { availabilityOutput ->
                AvailabilityResponse(
                    availabilityOutput.startDate,
                    availabilityOutput.endDate
                )
            }
        )

    fun mapToInput(
        authUser: AuthUser,
        request: IndicateAvailabilityRequest
    ) = IndicateAvailabilityInput(
        authUser.getId(), request.groupId, request.startDate,
        request.endDate
    )
}