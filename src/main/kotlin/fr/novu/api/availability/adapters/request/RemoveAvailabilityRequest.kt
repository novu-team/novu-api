package fr.novu.api.availability.adapters.request

import java.time.LocalDate

data class RemoveAvailabilityRequest(
    val groupId: String?,
    val startDate: LocalDate?,
    val endDate: LocalDate?
)
