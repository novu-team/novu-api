package fr.novu.api.availability.adapters.response

data class ParticipantWithoutVoteResponse(
    val name: String?,
    val firstname: String?,
    val trigram: String?,
    val color: String?
)
