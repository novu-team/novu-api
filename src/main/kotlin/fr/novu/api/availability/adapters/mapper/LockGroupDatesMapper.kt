package fr.novu.api.availability.adapters.mapper

import fr.novu.api.availability.adapters.request.LockGroupDatesRequest
import fr.novu.api.availability.adapters.response.LockGroupDatesResponse
import fr.novu.api.availability.usecase.input.LockGroupDatesInput
import fr.novu.api.availability.usecase.output.LockGroupDatesOutput
import fr.novu.api.shared.application.security.AuthUser
import org.springframework.stereotype.Component

@Component
class LockGroupDatesMapper {

    fun mapToResponse(result: LockGroupDatesOutput) =
        LockGroupDatesResponse(
            result.groupId,
            result.startDate, result.endDate
        )

    fun mapToInput(
        authUser: AuthUser,
        groupId: String?,
        request: LockGroupDatesRequest
    ) = LockGroupDatesInput(
        authUser.getId(), groupId, request.startDate,
        request.endDate
    )
}