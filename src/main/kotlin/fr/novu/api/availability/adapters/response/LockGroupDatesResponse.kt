package fr.novu.api.availability.adapters.response

import java.time.LocalDate

data class LockGroupDatesResponse(
    val groupId: String,
    val startDate: LocalDate?,
    val endDate: LocalDate?
)
