package fr.novu.api.availability.adapters.request

import java.time.LocalDate

data class LockGroupDatesRequest(
    val startDate: LocalDate?,
    val endDate: LocalDate?
)
