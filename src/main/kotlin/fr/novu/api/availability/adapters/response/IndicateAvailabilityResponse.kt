package fr.novu.api.availability.adapters.response

data class IndicateAvailabilityResponse(
    val userId: String, val groupId: String,
    val isAdmin: Boolean, val availabilities: List<AvailabilityResponse>
)
