package fr.novu.api.availability.adapters.response

import java.time.LocalDate

data class AvailabilityResponse(
    val startDate: LocalDate,
    val endDate: LocalDate
)
