package fr.novu.api.availability.adapters.mapper


import fr.novu.api.availability.adapters.request.RemoveAvailabilityRequest
import fr.novu.api.availability.usecase.input.RemoveAvailabilityInput
import fr.novu.api.shared.application.security.AuthUser
import org.springframework.stereotype.Component

@Component
class RemoveAvailabilityMapper {


    fun mapToInput(
        authUser: AuthUser,
        request: RemoveAvailabilityRequest
    ) = RemoveAvailabilityInput(
        authUser.getId(),
        authUser.getId(),
        request.groupId,
        request.startDate,
        request.endDate
    )
}