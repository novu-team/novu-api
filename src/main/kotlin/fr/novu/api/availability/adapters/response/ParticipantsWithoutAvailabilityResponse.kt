package fr.novu.api.availability.adapters.response

data class ParticipantsWithoutAvailabilityResponse(val participants: List<ParticipantWithoutVoteResponse>)
