package fr.novu.api.availability.usecase.output

import java.time.LocalDate

data class AvailabilityOutput(
    val startDate: LocalDate,
    val endDate: LocalDate
)
