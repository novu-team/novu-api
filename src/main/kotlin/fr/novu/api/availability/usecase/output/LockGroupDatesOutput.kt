package fr.novu.api.availability.usecase.output

import java.time.LocalDate

data class LockGroupDatesOutput(
    val groupId: String,
    val startDate: LocalDate?,
    val endDate: LocalDate?
)
