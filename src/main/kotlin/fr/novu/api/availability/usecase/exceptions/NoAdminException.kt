package fr.novu.api.availability.usecase.exceptions

import fr.novu.api.availability.domain.exceptions.InvalidException

class NoAdminException : InvalidException("Group does not have an admin")