package fr.novu.api.availability.usecase.exceptions

import fr.novu.api.availability.domain.exceptions.InvalidException

class UnknownGroupException : InvalidException("Participant not found")