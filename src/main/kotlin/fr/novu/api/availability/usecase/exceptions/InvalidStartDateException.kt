package fr.novu.api.availability.usecase.exceptions

import fr.novu.api.availability.domain.exceptions.InvalidException

class InvalidStartDateException : InvalidException("Start date can not be in the past")