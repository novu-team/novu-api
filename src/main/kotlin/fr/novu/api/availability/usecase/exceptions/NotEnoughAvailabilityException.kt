package fr.novu.api.availability.usecase.exceptions

import fr.novu.api.availability.domain.exceptions.InvalidException

class NotEnoughAvailabilityException :
    InvalidException("Half members of the group should indicate their availabilities")