package fr.novu.api.availability.usecase


import fr.novu.api.availability.domain.entity.GroupId
import fr.novu.api.availability.domain.entity.UserId
import fr.novu.api.availability.domain.repository.ParticipantRepository
import fr.novu.api.availability.usecase.exceptions.AvailabilityNotFoundException
import fr.novu.api.availability.usecase.exceptions.UnableToRemoveException
import fr.novu.api.availability.usecase.exceptions.UnknownParticipantException
import fr.novu.api.availability.usecase.input.RemoveAvailabilityInput
import org.springframework.stereotype.Service

@Service
class RemoveAvailability(private val participantRepository: ParticipantRepository) {
    fun execute(
        input: RemoveAvailabilityInput
    ) {
        if (input.userId != input.suppressorId) throw UnableToRemoveException()

        val startDate = input.startDate ?: throw AvailabilityNotFoundException()
        val endDate = input.endDate ?: throw AvailabilityNotFoundException()

        val participant = findParticipant(input.userId, input.groupId)
        val availabilityToRemove = participant.findAvailability(startDate, endDate)

        participant.removeAvailability(availabilityToRemove)
        participantRepository.updateParticipant(participant)
    }

    private fun findParticipant(
        userId: String?,
        groupId: String?
    ) = participantRepository.findParticipant(
        UserId(userId ?: ""),
        GroupId(groupId ?: "")
    ) ?: throw UnknownParticipantException()

}