package fr.novu.api.availability.usecase

import fr.novu.api.availability.domain.entity.GroupId
import fr.novu.api.availability.domain.entity.UserId
import fr.novu.api.availability.domain.repository.ParticipantRepository
import fr.novu.api.availability.usecase.exceptions.InvalidEndDateException
import fr.novu.api.availability.usecase.exceptions.InvalidStartDateException
import fr.novu.api.availability.usecase.exceptions.NoAdminException
import fr.novu.api.availability.usecase.exceptions.UnknownParticipantException
import fr.novu.api.availability.usecase.input.IndicateAvailabilityInput
import fr.novu.api.availability.usecase.output.AvailabilityOutput
import fr.novu.api.availability.usecase.output.IndicateAvailabilityOutput
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service
class IndicateAvailability(private val participantRepository: ParticipantRepository) {
    fun execute(
        input: IndicateAvailabilityInput
    ): IndicateAvailabilityOutput {

        val participant = findParticipant(input.userId, input.groupId)
        val groupParticipants = participantRepository.findParticipantsOfGroup(participant.id.groupId)
        if (groupParticipants.none { p -> p.isAdmin }) {
            throw NoAdminException()
        }

        val validStartDate = assertStartDateIsNotAnterior(input.startDate)
        val validEndDate = assertEndDateIsAfterStartDate(input.endDate, input.startDate)



        participant.addAvailability(validStartDate, validEndDate)

        participantRepository.updateParticipant(participant)

        return IndicateAvailabilityOutput(
            participant.id.userId.value,
            participant.id.groupId.value,
            participant.isAdmin,
            participant.availabilities.map { availability ->
                AvailabilityOutput(availability.startDate, availability.endDate)
            }
        )
    }

    private fun findParticipant(
        userId: String?,
        groupId: String?
    ) = participantRepository.findParticipant(
        UserId(userId ?: ""),
        GroupId(groupId ?: "")
    ) ?: throw UnknownParticipantException()

    private fun assertEndDateIsAfterStartDate(endDate: LocalDate?, startDate: LocalDate?): LocalDate {
        if (endDate == null || endDate.isBefore(startDate)) {
            throw InvalidEndDateException()
        }
        return endDate
    }

    private fun assertStartDateIsNotAnterior(startDate: LocalDate?): LocalDate {
        if (startDate == null || startDate.isBefore(LocalDate.now())) {
            throw InvalidStartDateException()
        }
        return startDate
    }
}