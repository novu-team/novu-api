package fr.novu.api.availability.usecase.exceptions

import fr.novu.api.availability.domain.exceptions.InvalidException

class AvailabilityAlreadyExistsException :
    InvalidException("Availability already exists")