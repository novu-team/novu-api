package fr.novu.api.availability.usecase

import fr.novu.api.availability.domain.entity.GroupId
import fr.novu.api.availability.domain.entity.Participant
import fr.novu.api.availability.domain.repository.ParticipantRepository
import fr.novu.api.availability.usecase.output.AvailabilityOutput
import fr.novu.api.availability.usecase.output.ParticipantAvailabilityOutput
import fr.novu.api.availability.usecase.output.ParticipantsAvailabilitiesOutput
import org.springframework.stereotype.Service

@Service
class GetParticipantsAvailabilities(
    private val participantRepository: ParticipantRepository
) {
    fun execute(
        groupId: String?
    ): ParticipantsAvailabilitiesOutput {

        val participants = participantRepository.findParticipantsOfGroup(GroupId(groupId ?: ""))
        return mapToOutput(groupId, participants)
    }

    private fun mapToOutput(
        groupId: String?,
        participants: List<Participant>
    ) = ParticipantsAvailabilitiesOutput(
        groupId ?: "",
        participants.map { participant ->
            ParticipantAvailabilityOutput(
                participant.id.userId.value,
                participant.availabilities.map { availability ->
                    AvailabilityOutput(
                        availability.startDate,
                        availability.endDate
                    )
                },
                participant.name,
                participant.firstname,
                participant.trigram,
                participant.color
            )
        })

}