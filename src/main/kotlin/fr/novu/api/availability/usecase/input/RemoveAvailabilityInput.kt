package fr.novu.api.availability.usecase.input

import java.time.LocalDate

data class RemoveAvailabilityInput(
    val suppressorId: String?,
    val userId: String?,
    val groupId: String?,
    val startDate: LocalDate?,
    val endDate: LocalDate?
)
