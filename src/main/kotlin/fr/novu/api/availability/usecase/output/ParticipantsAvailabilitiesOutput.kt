package fr.novu.api.availability.usecase.output

data class ParticipantsAvailabilitiesOutput(
    val groupId: String,
    val participants: List<ParticipantAvailabilityOutput>
)
