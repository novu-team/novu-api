package fr.novu.api.availability.usecase.exceptions

import fr.novu.api.availability.domain.exceptions.InvalidException

class NotAnAdminException : InvalidException("Participant should be an admin")