package fr.novu.api.availability.usecase

import fr.novu.api.availability.domain.entity.GroupId
import fr.novu.api.availability.domain.entity.Participant
import fr.novu.api.availability.domain.entity.UserId
import fr.novu.api.availability.domain.repository.GroupRepository
import fr.novu.api.availability.domain.repository.ParticipantRepository
import fr.novu.api.availability.usecase.exceptions.*
import fr.novu.api.availability.usecase.input.LockGroupDatesInput
import fr.novu.api.availability.usecase.output.LockGroupDatesOutput
import org.springframework.stereotype.Service
import java.time.LocalDate
import kotlin.math.ceil

@Service
class LockGroupDates(
    private val participantRepository: ParticipantRepository,
    private val groupRepository: GroupRepository
) {
    fun execute(
        input: LockGroupDatesInput
    ): LockGroupDatesOutput {

        val validStartDate = assertStartDateIsNotAnterior(input.startDate)
        val validEndDate = assertEndDateIsAfterStartDate(input.endDate, input.startDate)


        val participant = findParticipant(input.userId, input.groupId)
        if (participant.isNotAnAdmin()) throw NotAnAdminException()

        val participants = participantRepository.findParticipantsOfGroup(participant.id.groupId)
        val participantAvailabilities = countAvailabilities(participants)

        if (lessThanHalfHadIndicate(participantAvailabilities, participants)) {
            throw NotEnoughAvailabilityException()
        }

        val group = groupRepository.findGroup(GroupId(input.groupId ?: "")) ?: throw UnknownGroupException()


        group.setDates(validStartDate, validEndDate)
        groupRepository.updateGroup(group)

        return LockGroupDatesOutput(
            group.groupId.value,
            group.startDate, group.endDate
        )

    }

    private fun countAvailabilities(
        participants: List<Participant>
    ): Int {
        var participantAvailabilities = 0
        participants.forEach { p ->
            if (p.hasIndicateAvailability()) {
                participantAvailabilities += 1
            }
        }
        return participantAvailabilities
    }

    private fun lessThanHalfHadIndicate(
        participantAvailabilities: Int,
        participants: List<Participant>
    ) = participantAvailabilities < ceil((participants.size.toDouble() / 2))

    private fun findParticipant(
        userId: String?,
        groupId: String?
    ) = participantRepository.findParticipant(
        UserId(userId ?: ""),
        GroupId(groupId ?: "")
    ) ?: throw UnknownParticipantException()

    private fun assertEndDateIsAfterStartDate(endDate: LocalDate?, startDate: LocalDate?): LocalDate {
        if (endDate == null || endDate.isBefore(startDate)) {
            throw InvalidEndDateException()
        }
        return endDate
    }

    private fun assertStartDateIsNotAnterior(startDate: LocalDate?): LocalDate {
        if (startDate == null || startDate.isBefore(LocalDate.now())) {
            throw InvalidStartDateException()
        }
        return startDate
    }
}