package fr.novu.api.availability.usecase.input

import java.time.LocalDate

data class LockGroupDatesInput(
    val userId: String?,
    val groupId: String?,
    val startDate: LocalDate?,
    val endDate: LocalDate?
)
