package fr.novu.api.availability.usecase.output



data class IndicateAvailabilityOutput(
    val userId: String,
    val groupId: String,
    val isAdmin: Boolean,
    val availabilities: List<AvailabilityOutput>
)
