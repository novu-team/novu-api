package fr.novu.api.availability.usecase.output

data class ParticipantAvailabilityOutput(
    val userId: String,
    val availabilities: List<AvailabilityOutput>,
    val name: String?,
    val firstname: String?,
    val trigram: String?,
    val color: String?
)
