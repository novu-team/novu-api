package fr.novu.api.availability.usecase.exceptions

import fr.novu.api.availability.domain.exceptions.InvalidException

class InvalidEndDateException : InvalidException("End date can not be in the past and should be after start date")