package fr.novu.api.availability.usecase.exceptions

import fr.novu.api.availability.domain.exceptions.InvalidException

class UnableToRemoveException : InvalidException("You are not able to remove other's availabilities")